#!/bin/bash
cd `dirname $0`; pwd
DEPLOY_DIR=`pwd`
for file in $(ls $DEPLOY_DIR)
do
    if [ -d $DEPLOY_DIR"/"$file ] ;then
      if [ -f $DEPLOY_DIR"/"$file"/cmd.sh" ] ; then
        echo $file" module command "$1"..."
        su root $DEPLOY_DIR"/"$file"/cmd.sh" $1
        echo -e " "
      else
        echo $file" will execute command "$1"..."
        su root $DEPLOY_DIR"/"$file"/server.sh" $1
        echo -e "  "
      fi
    fi
done