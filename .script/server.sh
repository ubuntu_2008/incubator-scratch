#!/bin/bash
cd `dirname $0`; pwd

DEPLOY_DIR=`pwd`
for file in $(ls $DEPLOY_DIR)
do
    if [ "${file##*.}"x = "jar"x ] ;then
        JAR_NAME=$file
    fi
done

NAME="@application.name@"
PORT=@application.port@
LOGS="@application.logPath@"
ACTIVE_PROFILE=@spring.profiles.active@

PARAM=@jvm.param@
BIT_32="@java.opts.bit32@"
BIT_64="@java.opts.bit64@"

if [ -z "$SERVER_NAME" ]; then
    SERVER_NAME=`hostname`
fi

#使用说明，用来提示输入参数
usage() {
    echo "Usage: sh 执行脚本.sh [start|stop|restart|status]"
    exit 1
}

start(){
    PIDS=`ps -ef | grep java | grep "$JAR_NAME" |awk '{print $2}'`
    if [ -n "$PIDS" ]; then
        echo "ERROR: The $NAME already started!"
        echo "PID: $PIDS"
        exit 1
    fi

    if [ -n "$PORT" -a -1 -ne "$PORT" ]; then
        SERVER_PORT_COUNT=`netstat -tln | grep $PORT | wc -l`
        if [ $SERVER_PORT_COUNT -gt 0 ]; then
            echo "ERROR: The $NAME port $PORT already used!"
            exit 1
        fi
    fi

    if [ -z "$LOGS" ]; then
        LOGS="$DEPLOY_DIR/logs/$NAME"
    fi
    if [ ! -d "$LOGS" ]; then
        mkdir -p "$LOGS"
    fi

    if [ "$ACTIVE_PROFILE" = "dev" ]; then
      STDOUT_FILE="$LOGS/stdout.log"
    else
      STDOUT_FILE="/dev/null"
    fi

    EXEC_JAVA="$JAVA_HOME/bin/java"
    BITS=`java -version 2>&1 | grep -i 64-bit`
    if [ -n "$BITS" ]; then
        JAVA_OPTS=" $BIT_64 "
    else
        JAVA_OPTS=" $BIT_32 "
    fi

    echo -e "Starting the $NAME ...\c"
    nohup java $JAVA_OPTS -jar $JAR_NAME > $STDOUT_FILE 2>&1 &

    COUNT=0
    while [ $COUNT -lt 1 ]; do
        echo -e ".\c"
        sleep 1
        if [ -n "$PORT" -a -1 -ne "$PORT" ]; then
            COUNT=`netstat -an | grep $PORT | wc -l`
        else
            COUNT=`ps -ef | grep java | grep "$JAR_NAME" | awk '{print $2}' | wc -l`
        fi
        if [ $COUNT -gt 0 ]; then
            break
        fi
    done

    echo -e "OK! \c "
    PIDS=`ps -ef | grep java | grep "$JAR_NAME" | awk '{print $2}'`
    echo -e ">>>>>>> PID: $PIDS \c"
    echo -e ">>>>>>> STDOUT: $STDOUT_FILE"
}

stop(){
    PIDS=`ps -ef | grep java | grep "$JAR_NAME" |awk '{print $2}'`
    if [ -z "$PIDS" ]; then
        echo "ERROR: The $NAME does not started!"
        return
    fi

    echo -e "Stopping the $NAME ...\c"
    for PID in $PIDS ; do
        kill -9 $PID > /dev/null 2>&1
    done

    COUNT=0
    while [ $COUNT -lt 1 ]; do
        echo -e ".\c"
        sleep 1
        COUNT=1
        for PID in $pid ; do
            PID_EXIST=`ps -ef -p $PID | grep java`
            if [ -n "$PID_EXIST" ]; then
                COUNT=0
                break
            fi
        done
    done

    echo -e "OK! \c"
    echo -e ">>>>>>> PID: $PIDS"
}

restart(){
    stop
    sleep 6
    start
}

status(){
    PIDS=`ps -ef | grep java | grep "$JAR_NAME" |awk '{print $2}'`
    if [ -n "$PIDS" ]; then
        echo "The $NAME is running"
    else
        echo "The $NAME is not running"
    fi
}


case "$1" in
    start)
        start;;
    stop)
        stop;;
    restart)
        restart;;
    status)
        status;;
    *)
        usage;;
esac