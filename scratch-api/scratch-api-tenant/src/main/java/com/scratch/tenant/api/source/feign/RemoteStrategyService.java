package com.scratch.tenant.api.source.feign;

import com.scratch.common.core.constant.basic.ServiceConstants;
import com.scratch.common.core.web.feign.RemoteCacheService;
import com.scratch.tenant.api.source.feign.factory.RemoteStrategyFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 租户服务 | 策略模块 | 源策略服务
 *
 * @author scratch
 */
@FeignClient(contextId = "remoteStrategyService", path = "/inner/strategy", value = ServiceConstants.TENANT_SERVICE, fallbackFactory = RemoteStrategyFallbackFactory.class)
public interface RemoteStrategyService extends RemoteCacheService {
}