package com.scratch.tenant.api.source.feign;

import com.scratch.common.core.constant.basic.ServiceConstants;
import com.scratch.common.core.web.feign.RemoteCacheService;
import com.scratch.tenant.api.source.feign.factory.RemoteSourceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 租户服务 | 策略模块 | 数据源服务
 *
 * @author scratch
 */
@FeignClient(contextId = "remoteSourceService", path = "/inner/source", value = ServiceConstants.TENANT_SERVICE, fallbackFactory = RemoteSourceFallbackFactory.class)
public interface RemoteSourceService extends RemoteCacheService {
}