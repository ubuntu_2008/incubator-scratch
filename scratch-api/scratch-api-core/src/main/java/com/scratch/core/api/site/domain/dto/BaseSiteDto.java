package com.scratch.core.api.site.domain.dto;

import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.api.site.domain.po.BaseSitePo;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 网点 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseSiteDto extends BaseSitePo {
    private SysDeptDto dept;

    private BaseCpDto cp;
    @Serial
    private static final long serialVersionUID = 1L;

}