package com.scratch.core.api.site.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.annotation.Excel;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 网点 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_site", excludeProperty = { SORT, REMARK, NAME })
public class BaseSitePo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 网点编号 */
    @Excel(name = "网点编号")
    protected String siteNo;

    /** 区域 */
    @Excel(name = "区域")
    protected Long areaId;

    @Excel(name = "所属渠道")
    protected Long cpId;

    /** 网点名称 */
    @Excel(name = "网点名称")
    protected String siteName;

    /** 身份证号 */
    @Excel(name = "身份证号")
    protected String cardNo;

    /** 联系人 */
    @Excel(name = "联系人")
    protected String contact;

    /** 联系方式 */
    @Excel(name = "联系方式")
    protected String contactUs;

    /** 联系地址 */
    @Excel(name = "联系地址")
    protected String contactAddress;

    /** 已授权业务 */
    @Excel(name = "已授权业务")
    protected String authBus;

    /** 网点状态 */

    /** 网点地址 */
    @Excel(name = "网点地址", type = Excel.Type.EXPORT)
    protected String siteAddress;

    /** 经度 */
    @Excel(name = "经度", type = Excel.Type.EXPORT)
    protected String longitude;

    /** 纬度 */
    @Excel(name = "纬度", type = Excel.Type.EXPORT)
    protected String latitude;

}