package com.scratch.core.enums;

public enum EnumPayStatus {
    UNPAID(0, "未支付"),
    SUCCESS(1, "支付成功"),
    TIMEOUT(2, "支付超时"),
    REFUND(10, "待退款"),
    REFUNDED(11, "退款成功"),
    REFUND_FAIL(12, "退款失败");
    public int code;

    public String message;

    EnumPayStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static EnumPayStatus getByValue(int value) {
        for (EnumPayStatus code : values()) {
            if (code.getCode() == value) {
                return code;
            }
        }
        return null;
    }

    public static String getMsgByValue(int value) {
        for (EnumPayStatus code : values()) {
            if (code.getCode() == value) {
                return code.getMessage();
            }
        }
        return "未知";
    }
}
