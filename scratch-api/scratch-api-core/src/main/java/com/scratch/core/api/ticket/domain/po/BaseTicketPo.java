package com.scratch.core.api.ticket.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.util.Date;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 票信息管理 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_ticket", excludeProperty = { SORT, REMARK, NAME })
public class BaseTicketPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 方案编号 */
    protected String playNo;

    /** 玩法名称 */
    protected String playName;

    /** 票面价值(元) */
    protected Long value;

    /** 票类型 */
    protected String type;

    /** 横/竖票面 */
    protected String xy;

    /** 票面尺寸宽(毫米) */
    protected Integer sizeX;

    /** 票面尺寸高(毫米) */
    protected Integer sizeY;

    /** 售兑区域类型 */
    protected String areaType;

    /** 售兑区域 */
    protected String areaId;

    /** 上市年份 */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date listYear;

    /** 退市年份 */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date delistYear;

    /** 样票图片 */
    protected String img;

    /** 票类说明 */
    @TableField("`desc`")
    protected String desc;

    /** 玩法规则 */
    protected String playDesc;

    /** 奖组规模(万张) */
    protected Integer awardGroupSize;

    /** 奖级箱数(箱) */
    protected Integer awardLevelCaseNum;

    /** 每箱重量(公斤) */
    protected Integer caseWeight;

    /** 每箱盒数(盒) */
    protected Integer caseBoxNum;

    /** 每盒本数(本) */
    protected Integer boxBenNum;

    /** 每箱本数(本) */
    protected Integer caseBenNum;

    /** 每本张数(张) */
    protected Integer benZhangNum;

    /** 最高奖金(元) */
    protected Long maxMoney;

    /** 中奖率(%) */
    protected Integer winRate;

    /** 返奖率(%) */
    protected Integer rewardRate;

    /** 中奖机会 */
    protected String winChance;

    public Date getListYear() {
        return listYear;
    }
    public Date getDelistYear() {
        return delistYear;
    }

}