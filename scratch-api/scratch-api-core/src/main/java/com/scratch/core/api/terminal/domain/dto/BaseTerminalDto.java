package com.scratch.core.api.terminal.domain.dto;

import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.api.terminal.domain.po.BaseTerminalPo;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 终端信息 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTerminalDto extends BaseTerminalPo {

    @Serial
    private static final long serialVersionUID = 1L;

    private BaseSiteDto site;

    private BaseTerminalModelDto model;

    public String getRedisFieldId() {
        return super.devNo;
    }

}