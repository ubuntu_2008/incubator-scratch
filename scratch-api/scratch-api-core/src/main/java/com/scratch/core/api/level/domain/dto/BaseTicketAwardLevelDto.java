package com.scratch.core.api.level.domain.dto;

import com.scratch.core.api.level.domain.po.BaseTicketAwardLevelPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 奖级 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTicketAwardLevelDto extends BaseTicketAwardLevelPo {

    @Serial
    private static final long serialVersionUID = 1L;

}