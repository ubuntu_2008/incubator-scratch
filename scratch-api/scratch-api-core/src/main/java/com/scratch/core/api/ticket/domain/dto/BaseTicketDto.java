package com.scratch.core.api.ticket.domain.dto;

import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.api.ticket.domain.po.BaseTicketPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.util.List;

/**
 * 票信息管理 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTicketDto extends BaseTicketPo {

    @Serial
    private static final long serialVersionUID = 1L;

    protected List<BaseTicketAwardLevelDto> levels;


}