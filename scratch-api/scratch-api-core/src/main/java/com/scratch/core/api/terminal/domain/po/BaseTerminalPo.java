package com.scratch.core.api.terminal.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 终端信息 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_terminal", excludeProperty = {SORT, REMARK, NAME})
public class BaseTerminalPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 品牌名称
     */
    protected Long brandId;

    /**
     * 型号名称
     */
    protected Long modelId;

    /**
     * mac地址
     */
    protected String mac;

    /**
     * 终端ID
     */
    protected String devNo;

    /**
     * 所属网点
     */
    protected Long siteId;

    /**
     * 备注
     */
    protected String remarks;


}