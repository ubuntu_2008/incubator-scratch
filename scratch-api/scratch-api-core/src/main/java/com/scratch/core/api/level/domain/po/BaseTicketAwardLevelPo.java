package com.scratch.core.api.level.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.REMARK;
import static com.scratch.common.core.constant.basic.EntityConstants.STATUS;

/**
 * 奖级 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_ticket_award_level", excludeProperty = { STATUS, REMARK })
public class BaseTicketAwardLevelPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 票信息表id */
    protected Long baseTicketId;

    /** 奖级名称 */
    protected String name;

    /** 中奖金额(元) */
    protected Long winMoney;

    /** 中奖金额(元) */
    protected Long winNum;

}