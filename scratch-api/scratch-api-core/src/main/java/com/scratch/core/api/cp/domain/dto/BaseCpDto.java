package com.scratch.core.api.cp.domain.dto;

import com.scratch.core.api.cp.domain.po.BaseCpPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 信息管理 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseCpDto extends BaseCpPo {

    @Serial
    private static final long serialVersionUID = 1L;

}