package com.scratch.core.enums;

public enum EnumOrderType {

    TYPE_LOTT(1, "即开票");

    public int code;

    public String message;

    EnumOrderType(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
