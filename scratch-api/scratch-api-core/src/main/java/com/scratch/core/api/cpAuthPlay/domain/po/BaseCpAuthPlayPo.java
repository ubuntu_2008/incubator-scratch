package com.scratch.core.api.cpAuthPlay.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.annotation.Excel;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 渠道玩法授权 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_cp_auth_play", excludeProperty = { STATUS, SORT, REMARK, NAME })
public class BaseCpAuthPlayPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 渠道信息表id */
    @Excel(name = "渠道信息表id")
    protected Long baseCpId;

    /** 票信息表id */
    @Excel(name = "票信息表id")
    protected Long baseTicketId;

    /** 销售授权 */
    @Excel(name = "销售授权")
    protected String authSell;

    /** 兑奖授权 */
    @Excel(name = "兑奖授权")
    protected String authCash;

}