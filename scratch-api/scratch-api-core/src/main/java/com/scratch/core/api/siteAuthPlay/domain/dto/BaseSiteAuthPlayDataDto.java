package com.scratch.core.api.siteAuthPlay.domain.dto;


import lombok.Data;

@Data
public class BaseSiteAuthPlayDataDto {
    private String baseTicketId;
    private String img;
    private String playName;
    private String playNo;
    private String authSell;
    private String bkgeSell;
    private String authCash;
    private String bkgeCash;
}
