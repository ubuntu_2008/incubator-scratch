package com.scratch.core.api.cash.domain.po;

import com.scratch.common.core.web.tenant.base.TBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 兑奖 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_cash", excludeProperty = { UPDATE_BY, SORT, DEL_FLAG, UPDATE_TIME, REMARK, NAME })
public class BaseCashPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 设备编号 */
    protected String devNo;

    /** 用户ID */
    protected String userId;

    /** 订单编号 */
    protected String orderNo;

    /** 游戏名称 */
    protected String playName;

    /** 保安码 */
    protected String dataArea;

    /** 票号 */
    protected String ticketCode;

    /** 中奖金额 */
    protected Long winMoney;

    /** 兑奖时间 */
    protected Long cashTime;


    /** 签名 */
    protected String sign;

}