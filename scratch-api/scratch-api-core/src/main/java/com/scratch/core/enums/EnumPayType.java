package com.scratch.core.enums;

public enum EnumPayType {
    E_PAYMENT(1, "电子支付"),
    CASH(2, "现金");
    public int code;

    public String message;

    EnumPayType(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static EnumPayType getByValue(int value) {
        for (EnumPayType code : values()) {
            if (code.getCode() == value) {
                return code;
            }
        }
        return null;
    }

    public static String getMsgByValue(int value) {
        for (EnumPayType code : values()) {
            if (code.getCode() == value) {
                return code.getMessage();
            }
        }
        return "未知";
    }
}
