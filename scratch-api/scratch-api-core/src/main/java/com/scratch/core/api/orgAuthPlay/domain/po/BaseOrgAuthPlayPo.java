package com.scratch.core.api.orgAuthPlay.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 机构玩法授权 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_org_auth_play", excludeProperty = { STATUS, SORT, REMARK, NAME })
public class BaseOrgAuthPlayPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 机构信息表id */
    protected Long baseOrgId;
    /** 票信息表id */
    protected Long baseTicketId;

    /** 销售授权 */
    protected String authSell;

    /** 兑奖授权 */
    protected String authCash;

}