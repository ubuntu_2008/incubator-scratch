package com.scratch.core.api.service.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResOrder {

    /**
     * 机器展示码
     */
    private String orderQrCode;

    /**
     * 订单超时时间
     */
    private Long orderTimeOut;

    /**
     * 终端编号
     */
    private String devNo;

    /**
     * 订单时间
     */
    private Long orderTime;


}
