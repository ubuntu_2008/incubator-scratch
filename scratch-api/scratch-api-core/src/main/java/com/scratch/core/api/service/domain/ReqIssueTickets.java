package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ReqIssueTickets {
    @NotBlank(message = "订单编号不能为空")
    private String orderNo;
//    @NotBlank(message = "票号不能为空")
//    private String ticketNo;
}
