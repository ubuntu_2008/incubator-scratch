package com.scratch.core.api.orgAuthPlay.domain.dto;

import com.scratch.core.api.orgAuthPlay.domain.po.BaseOrgAuthPlayPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 机构玩法授权 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseOrgAuthPlayDto extends BaseOrgAuthPlayPo {

    @Serial
    private static final long serialVersionUID = 1L;
    public String getRedisKeyId() {
        return super.baseOrgId.toString();
    }
}