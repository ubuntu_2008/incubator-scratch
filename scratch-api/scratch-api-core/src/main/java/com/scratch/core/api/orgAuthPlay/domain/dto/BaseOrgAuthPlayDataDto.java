package com.scratch.core.api.orgAuthPlay.domain.dto;


import lombok.Data;

@Data
public class BaseOrgAuthPlayDataDto {
private String baseTicketId;
private String img;
private String playName;
private String playNo;
private String authSell;
private String authCash;
}
