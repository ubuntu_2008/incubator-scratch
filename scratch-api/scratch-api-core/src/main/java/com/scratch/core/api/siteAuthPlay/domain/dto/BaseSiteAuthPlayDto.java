package com.scratch.core.api.siteAuthPlay.domain.dto;

import com.scratch.core.api.siteAuthPlay.domain.po.BaseSiteAuthPlayPo;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 站点玩法授权 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseSiteAuthPlayDto extends BaseSiteAuthPlayPo {

    @Serial
    private static final long serialVersionUID = 1L;

    public String getRedisKeyId() {
        return super.baseSiteId.toString();
    }

}