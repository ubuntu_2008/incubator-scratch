package com.scratch.core.api.service.domain;

import lombok.Data;

@Data
public class ResPlayList {
    private String ticketId;

    /**
     * 方案编号
     */
    private String playNo;

    /**
     * 玩法名称
     */
    private String playName;

    /**
     * 票面价值(元)
     */
    private Long value;

    /**
     * 票类型
     */
    private String type;

    /**
     * 横/竖票面
     */
    private String xy;

    /**
     * 票面尺寸宽(毫米)
     */
    private Integer sizeX;

    /**
     * 票面尺寸高(毫米)
     */
    private Integer sizeY;

    /**
     * 售兑区域类型
     */
    private String areaType;

    /**
     * 样票图片
     */
    private String img;


}
