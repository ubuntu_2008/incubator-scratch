package com.scratch.core.api.siteAuthPlay.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 站点玩法授权 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_site_auth_play", excludeProperty = { STATUS, SORT, REMARK, NAME })
public class BaseSiteAuthPlayPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 站点信息表id */
    protected Long baseSiteId;

    /** 票信息表id */
    protected Long baseTicketId;

    /** 销售授权 */
    protected String authSell;

    /** 销售佣金 */
    protected String bkgeSell;

    /** 兑奖授权 */
    protected String authCash;

    /** 兑奖佣金 */
    protected String bkgeCash;

}