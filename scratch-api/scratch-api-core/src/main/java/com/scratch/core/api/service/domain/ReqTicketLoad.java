package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ReqTicketLoad {
    @NotBlank(message = "终端Id不能为空")
    private String devNo;

    @NotBlank(message = "票Id不能为空")
    private String ticketId;

    /**
     * 仓位序号约定俗称  从左至右，从上到下  依次1、2、3、4、5、6
     */
    @NotNull(message = "仓位编号不能为空")
    @Min(value = 1,message = "仓位编号错误")
    private Integer hpId;

    @NotNull(message = "仓位票总数错误")
    @Min(value = 1,message = "请输入正确的票数量")
    private Integer total;



}
