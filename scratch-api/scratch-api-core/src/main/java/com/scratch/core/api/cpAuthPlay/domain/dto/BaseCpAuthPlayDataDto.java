package com.scratch.core.api.cpAuthPlay.domain.dto;


import lombok.Data;

@Data
public class BaseCpAuthPlayDataDto {
    private String baseTicketId;
    private String img;
    private String playName;
    private String playNo;
    private String authSell;
    private String authCash;
}
