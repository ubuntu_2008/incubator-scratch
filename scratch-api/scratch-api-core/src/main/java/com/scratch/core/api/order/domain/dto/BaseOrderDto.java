package com.scratch.core.api.order.domain.dto;

import com.scratch.core.api.order.domain.po.BaseOrderPo;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import lombok.*;

import java.io.Serial;

/**
 * 订单 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseOrderDto extends BaseOrderPo {
    private SysDeptDto dept;
    @Serial
    private static final long serialVersionUID = 1L;

}