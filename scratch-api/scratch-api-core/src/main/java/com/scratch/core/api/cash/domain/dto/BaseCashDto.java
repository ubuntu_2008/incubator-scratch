package com.scratch.core.api.cash.domain.dto;

import com.scratch.core.api.cash.domain.po.BaseCashPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 兑奖 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseCashDto extends BaseCashPo {

    @Serial
    private static final long serialVersionUID = 1L;

}