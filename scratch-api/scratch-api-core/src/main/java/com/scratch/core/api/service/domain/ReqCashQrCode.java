package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ReqCashQrCode {
    @NotBlank(message = "终端编号不能为空")
    private String devNo;
}
