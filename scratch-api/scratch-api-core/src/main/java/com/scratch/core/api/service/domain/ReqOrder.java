package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ReqOrder {
    @NotBlank(message = "终端编号不能为空")
    private String devNo;
    @NotNull(message = "仓位编号不能为空")
    @Min(value = 1, message = "仓位编号错误")
    private Integer hpId;
    @NotBlank(message = "票Id不能为空")
    private String ticketId;
    @NotNull(message = "总金额不能为空")
    private Integer money;
    @NotNull(message = "票数量不能为空")
    @Min(value = 1, message = "票数量错误")
    private Integer number;

}
