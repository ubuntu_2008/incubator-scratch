package com.scratch.core.api.terminalModel.domain.dto;

import com.scratch.core.api.terminalModel.domain.po.BaseTerminalModelPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 终端型号 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTerminalModelDto extends BaseTerminalModelPo {

    @Serial
    private static final long serialVersionUID = 1L;

}