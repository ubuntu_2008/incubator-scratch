package com.scratch.core.api;

import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 授权后所有对象合集
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthDto {
    private BaseTerminalDto baseTerminalDto;
    private BaseSiteDto baseSiteDto;
    private BaseCpDto baseCpDto;
    private BaseOrgDto baseOrgDto;
    private List<Object> siteAuthSellPlayList;

    private List<Object> siteAuthCashPlayList;

}
