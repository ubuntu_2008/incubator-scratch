package com.scratch.core.enums;

public enum EnumAuthBus {
    SELL(0, "销售"),
    CASH(1, "兑奖");
    private int code;
    private String message;

    EnumAuthBus(
            int code,
            String message) {
        this.code = code;
        this.message = message;
    }

    public static EnumAuthBus getEnum(
            int code) {
        for (EnumAuthBus ele : EnumAuthBus.values()) {
            if (ele.getCode() == code) {
                return ele;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(
            int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(
            String message) {
        this.message = message;
    }
}
