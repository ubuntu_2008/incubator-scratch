package com.scratch.core.api.order.domain.po;

import com.scratch.common.core.web.entity.base.BaseEntity;
import com.scratch.common.core.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 订单 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_order", excludeProperty = {UPDATE_BY, SORT, CREATE_BY, DEL_FLAG, UPDATE_TIME, REMARK, NAME,STATUS})
public class BaseOrderPo extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 区域ID
     */
    protected Long areaId;

    /**
     * 机构编号
     */
    protected String orgNo;

    /**
     * 渠道编号
     */
    protected String cpNo;

    /**
     * 站点编号
     */
    protected String siteNo;

    /**
     * 终端编号
     */
    protected String devNo;

    /**
     * 票id
     */
    protected String ticketId;

    /**
     * 订单编号
     */
    protected String orderNo;


    /**
     * 订单金额 （元）
     */
    protected Integer orderMoney;


    /**
     * 订单时间
     */
    protected Long orderTime;

    /**
     * 订单失效时间
     */
    protected Long orderTimeOut;

    /**
     * 订单描述
     */
    protected String orderDesc;



    /**
     * 支付时间
     */
    protected Long payTime;

    /**
     * 退款时间
     */
    protected Long refundTime;

    /**
     * 出票时间
     */
    protected Long buyTime;

    /**
     * 票号JSON
     */
    protected String orderContent;

    /**
     * 订单类型
     */
    protected Integer orderType;
    protected Integer orderStatus;
    protected Integer payType;
    protected Integer payStatus;

    /**
     * 签名
     */
    protected String sign;

}