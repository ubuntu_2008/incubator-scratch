package com.scratch.core.api.cpAuthPlay.domain.dto;

import com.scratch.core.api.cpAuthPlay.domain.po.BaseCpAuthPlayPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 渠道玩法授权 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseCpAuthPlayDto extends BaseCpAuthPlayPo {

    @Serial
    private static final long serialVersionUID = 1L;

    public String getRedisKeyId() {
        return super.baseCpId.toString();
    }
}