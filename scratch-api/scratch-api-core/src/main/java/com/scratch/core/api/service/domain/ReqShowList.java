package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ReqShowList {
    @NotBlank(message = "终端Id不能为空")
    private String devNo;
}
