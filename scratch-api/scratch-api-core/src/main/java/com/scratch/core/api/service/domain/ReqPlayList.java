package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ReqPlayList {
    @NotBlank(message = "终端Id不能为空")
    private String devNo;

    private Long value;
}
