package com.scratch.core.api.service.domain;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResShowList {
    /**
     * 仓位ID
     */
    private Integer hpId;
    /**
     * 票信息
     */
    private TicketItem ticket;
    /**
     * 票余量
     */
    private Integer total;

}
