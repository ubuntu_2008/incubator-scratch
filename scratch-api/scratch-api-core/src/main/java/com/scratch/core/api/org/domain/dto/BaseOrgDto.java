package com.scratch.core.api.org.domain.dto;

import com.scratch.core.api.org.domain.po.BaseOrgPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 信息管理 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseOrgDto extends BaseOrgPo {

    @Serial
    private static final long serialVersionUID = 1L;

    public String getRedisFieldId() {
        return super.areaId.toString();
    }

}