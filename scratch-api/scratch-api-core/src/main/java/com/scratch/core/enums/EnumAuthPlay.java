package com.scratch.core.enums;

public enum EnumAuthPlay {
    AUTHORIZED(0, "已授权"),
    UNAUTHORIZED(1, "未授权");
    private int code;
    private String message;

    EnumAuthPlay(
            int code,
            String message) {
        this.code = code;
        this.message = message;
    }

    public static EnumAuthPlay getEnum(
            int code) {
        for (EnumAuthPlay ele : EnumAuthPlay.values()) {
            if (ele.getCode() == code) {
                return ele;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(
            int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(
            String message) {
        this.message = message;
    }
}
