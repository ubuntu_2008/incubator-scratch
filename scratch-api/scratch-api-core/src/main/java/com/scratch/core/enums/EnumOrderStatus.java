package com.scratch.core.enums;

public enum EnumOrderStatus {
    PENDING(0, "订单进行中"),
    COMPLETE(1, "订单完成"),
    CLOSE(2, "订单关闭"),
    SUSPEND(3, "订单挂起");


    public int code;

    public String message;

    EnumOrderStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static EnumOrderStatus getByValue(int value) {
        for (EnumOrderStatus code : values()) {
            if (code.getCode() == value) {
                return code;
            }
        }
        return null;
    }

    public static String getMsgByValue(int value) {
        for (EnumOrderStatus code : values()) {
            if (code.getCode() == value) {
                return code.getMessage();
            }
        }
        return "未知";
    }
}
