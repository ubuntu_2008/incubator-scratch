package com.scratch.core.api.terminalModel.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 终端型号 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_terminal_model", excludeProperty = { SORT, REMARK, NAME })
public class BaseTerminalModelPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 型号名称 */
    protected String modelName;

    /** 品牌名称 */
    protected Long brandId;

    /** 终端类型 */
    protected String terminalType;

    /** 屏幕类型 */
    protected String screenType;

    /** 机头/仓位容量(张) */
    protected Integer hpStorage;

    /**
     * 机头仓位配置
     */
    protected String hp;


}