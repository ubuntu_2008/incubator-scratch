package com.scratch.core.api.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResCash {
    /**
     * 1: 中奖
     * 2: 未中奖
     * 3: 重复兑奖
     */
    private Integer status;
    private Long winMoney;
}
