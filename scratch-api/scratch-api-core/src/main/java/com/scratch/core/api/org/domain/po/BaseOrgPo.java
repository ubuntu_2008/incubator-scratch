package com.scratch.core.api.org.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.tenant.base.TBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 信息管理 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "base_org", excludeProperty = { SORT, REMARK, NAME })
public class BaseOrgPo extends TBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 机构编号 */
    protected String orgNo;

    /** 区域 */
    protected Long areaId;

    /** 机构名称 */
    protected String orgName;

    /** 联系人 */
    protected String contact;

    /** 联系方式 */
    protected String contactUs;

    /** 联系地址 */
    protected String contactAddress;

    /** 已授权业务 */
    protected String authBus;



}