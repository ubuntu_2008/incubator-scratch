package com.scratch.common.security.utils;

import com.scratch.common.core.utils.core.SpringUtil;
import com.scratch.common.security.service.TokenUserService;
import com.scratch.system.api.model.DataScope;

/**
 * 管理端 - 权限获取工具类
 *
 * @author scratch
 */
public class SecurityUserUtils extends SecurityUtils {

    /**
     * 获取数据权限信息
     */
    public static DataScope getDataScope() {
        return SpringUtil.getBean(TokenUserService.class).getDataScope();
    }
}
