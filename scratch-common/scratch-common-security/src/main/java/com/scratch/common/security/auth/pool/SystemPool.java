package com.scratch.common.security.auth.pool;

import com.scratch.common.security.auth.pool.system.SysApplicationPool;
import com.scratch.common.security.auth.pool.system.SysAuthorityPool;
import com.scratch.common.security.auth.pool.system.SysDictPool;
import com.scratch.common.security.auth.pool.system.SysFilePool;
import com.scratch.common.security.auth.pool.system.SysMonitorPool;
import com.scratch.common.security.auth.pool.system.SysNoticePool;
import com.scratch.common.security.auth.pool.system.SysOrganizePool;

/**
 * 系统服务 权限标识常量
 *
 * @author scratch
 */
public interface SystemPool extends SysApplicationPool, SysAuthorityPool, SysDictPool, SysFilePool, SysMonitorPool, SysNoticePool, SysOrganizePool {
}
