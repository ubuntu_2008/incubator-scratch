package com.scratch.common.security.auth.pool.system;

/**
 * 系统服务 | 字典模块 权限标识常量
 *
 * @author scratch
 */
public interface SysDictPool {

    /** 系统服务 | 字典模块 | 字典管理 | 列表 */
    String SYS_DICT_LIST = "dict:dict:list";
    /** 系统服务 | 字典模块 | 字典管理 | 详情 */
    String SYS_DICT_SINGLE = "dict:dict:single";
    /** 系统服务 | 字典模块 | 字典管理 | 新增 */
    String SYS_DICT_ADD = "dict:dict:add";
    /** 系统服务 | 字典模块 | 字典管理 | 修改 */
    String SYS_DICT_EDIT = "dict:dict:edit";
    /** 系统服务 | 字典模块 | 字典管理 | 修改状态 */
    String SYS_DICT_ES = "dict:dict:es";
    /** 系统服务 | 字典模块 | 字典管理 | 删除 */
    String SYS_DICT_DEL = "dict:dict:delete";
    /** 系统服务 | 字典模块 | 字典管理 | 字典管理 */
    String SYS_DICT_DICT = "dict:dict:dict";

    /** 系统服务 | 字典模块 | 参数管理 | 列表 */
    String SYS_CONFIG_LIST = "dict:config:list";
    /** 系统服务 | 字典模块 | 参数管理 | 详情 */
    String SYS_CONFIG_SINGLE = "dict:config:single";
    /** 系统服务 | 字典模块 | 参数管理 | 新增 */
    String SYS_CONFIG_ADD = "dict:config:add";
    /** 系统服务 | 字典模块 | 参数管理 | 修改 */
    String SYS_CONFIG_EDIT = "dict:config:edit";
    /** 系统服务 | 字典模块 | 参数管理 | 修改状态 */
    String SYS_CONFIG_ES = "dict:config:es";
    /** 系统服务 | 字典模块 | 参数管理 | 删除 */
    String SYS_CONFIG_DEL = "dict:config:delete";

}
