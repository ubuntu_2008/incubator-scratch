package com.scratch.common.security.auth;

import com.scratch.common.security.auth.pool.GenPool;
import com.scratch.common.security.auth.pool.JobPool;
import com.scratch.common.security.auth.pool.SystemPool;
import com.scratch.common.security.auth.pool.TenantPool;

/**
 * 权限标识常量
 *
 * @author scratch
 */
public class Auth implements SystemPool, JobPool, GenPool, TenantPool {
}
