package com.scratch.common.security.auth.pool;

import com.scratch.common.security.auth.pool.tenant.TeSourcePool;
import com.scratch.common.security.auth.pool.tenant.TeTenantPool;

/**
 * 租户服务 权限标识常量
 *
 * @author scratch
 */
public interface TenantPool extends TeTenantPool, TeSourcePool {
}
