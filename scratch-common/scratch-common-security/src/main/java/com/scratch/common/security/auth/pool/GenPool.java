package com.scratch.common.security.auth.pool;

/**
 * 代码生成服务 权限标识常量
 *
 * @author scratch
 */
public interface GenPool {

    /** 代码生成 - 代码生成管理 - 列表 */
    String GEN_GENERATE_LIST = "generate:gen:list";
    /** 代码生成 - 代码生成管理 - 详情 */
    String GEN_GENERATE_SINGLE = "generate:gen:single";
    /** 代码生成 - 代码生成管理 - 新增 */
    String GEN_GENERATE_ADD = "generate:gen:add";
    /** 代码生成 - 代码生成管理 - 修改 */
    String GEN_GENERATE_EDIT = "generate:gen:edit";
    /** 代码生成 - 代码生成管理 - 删除 */
    String GEN_GENERATE_DEL = "generate:gen:delete";
    /** 代码生成 - 代码生成管理 - 导入 */
    String GEN_GENERATE_IMPORT = "generate:gen:import";
    /** 代码生成 - 代码生成管理 - 生成 */
    String GEN_GENERATE_PREVIEW = "generate:gen:preview";
    /** 代码生成 - 代码生成管理 - 下载 */
    String GEN_GENERATE_CODE = "generate:gen:code";
}
