package com.scratch.common.service.db.domain;

import com.scratch.common.service.db.DbEntity;
import com.scratch.common.service.db.DbTable;
import com.scratch.common.service.db.DbTableColumn;
import com.scratch.common.service.db.DbTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@DbTable(
        name = "base_ticket_load",
        desc = "装票/票余量表",
        updateKey = {"data_version"},
        updateColumns = {"total"})
public class BaseTicketLoadEntity extends DbEntity {

    @DbTableColumn("dev_no")
    private String devNo;

    @DbTableColumn("base_ticket_id")
    private String ticketId;

    @DbTableColumn("hp_id")
    private Integer hpId;

    @DbTableColumn("total")
    private Integer total;
}
