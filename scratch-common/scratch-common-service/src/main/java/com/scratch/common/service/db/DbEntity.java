package com.scratch.common.service.db;


import lombok.Data;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@Data
public class DbEntity {

    private Long dataVersion;
}
