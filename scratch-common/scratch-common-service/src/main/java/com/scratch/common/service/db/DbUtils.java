package com.scratch.common.service.db;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bpf on 2018/1/29.
 */
@Slf4j
public class DbUtils extends org.apache.commons.lang3.StringUtils {


    public final static String KEY_LIST_COLUMN = "KEY_LIST_COLUMN";
    public final static String KEY_LIST_ATTRIBUTE = "KEY_LIST_ATTRIBUTE";
    private static final Pattern linePattern = Pattern.compile("_(\\w)");
    private static final Pattern humpPattern = Pattern.compile("[A-Z]");

    public static Class getClass(String clazzStr) {
        Class clazz = null;
        try {
            //利用forName读取配置文件的名称
            clazz = Class.forName(clazzStr);
        } catch (ClassNotFoundException e) {
            throw new DbException("字符串转类错误", e);
        }
        return clazz;
    }

    public static DbTable getDbTable(Class clazz) {
        return (DbTable) clazz.getDeclaredAnnotation(DbTable.class);
    }

    public static String getTableName(Class clazz) {
        DbTable dbTable = getDbTable(clazz);
        return (dbTable != null) ? dbTable.name().toUpperCase() : humpToLine(lowerFirstChar(clazz.getSimpleName())).toUpperCase();
    }

    public static String[] getTableNameInsterExclude(Class clazz) {
        DbTable dbTable = getDbTable(clazz);
        return (dbTable != null) ? dbTable.insertExcludeColumns() : new String[0];
    }

    public static String[] getTableNameUpdateColumns(Class clazz) {
        DbTable dbTable = getDbTable(clazz);
        return ArrayUtils.addAll(dbTable.updateColumns(), dbTable.updateKey());
    }


    public static String getTableNameUpdateSql(Class clazz) {
        DbTable dbTable = getDbTable(clazz);
        StringBuffer buffer = new StringBuffer();
        buffer.append("UPDATE " + dbTable.name() + " SET ");
        for (int i = 0; i < dbTable.updateColumns().length; i++) {
            if (i == dbTable.updateColumns().length - 1) {
                buffer.append(dbTable.updateColumns()[i] + "=? ");
            } else {
                buffer.append(dbTable.updateColumns()[i] + "=?, ");
            }
        }
        if (dbTable.updateKey().length > 0) {
            for (int j = 0; j < dbTable.updateKey().length; j++) {
                if (j == 0) {
                    buffer.append(" WHERE " + dbTable.updateKey()[j] + "=?");
                } else {
                    buffer.append(" AND " + dbTable.updateKey()[j] + "=?");
                }
            }
        }
        return buffer.toString();
    }

    public static String getTableNameInsertSql(Class clazz, List<String> listColumn) {
        DbTable dbTable = getDbTable(clazz);
        String[] paramArray = new String[listColumn.size()];
        Arrays.fill(paramArray, "?");
        StringBuffer buffer = new StringBuffer();
        buffer.append("INSERT INTO " + dbTable.name() + " (" + StringUtils.join(listColumn.toArray(), ",") + ") VALUES (");
        buffer.append(StringUtils.join(paramArray, ","));
        buffer.append(")");
        return buffer.toString();
    }


    public static String getTableNameInsertBulkSql(Class clazz, List<String> listColumn) {
        DbTable dbTable = getDbTable(clazz);
        String[] paramArray = new String[listColumn.size()];
        Arrays.fill(paramArray, "?");
        StringBuffer buffer = new StringBuffer();
        buffer.append("INSERT INTO " + dbTable.name() + " (" + StringUtils.join(listColumn.toArray(), ",") + ") VALUES (");
        buffer.append(StringUtils.join(paramArray, ","));
        buffer.append(")");
        buffer.append(" ON DUPLICATE KEY UPDATE ");
        String bufferSuffix = "@=IF(VALUES(&)>&,VALUES(@),@),".replaceAll("&", dbTable.updateKey()[0]);
        Arrays.stream(ArrayUtils.addAll(dbTable.updateColumns(), dbTable.updateKey())).forEach(column -> buffer.append(bufferSuffix.replaceAll("@", column)));
        return buffer.deleteCharAt(buffer.length() - 1).toString();
    }

    public static void main(String[] args) {


//      Map<String, List<String>> map = DbUtils.getTableColumn(DbTypeEnum.getByCode("1"), com.westedge.welfare.starter.common.db.domain.amc.AmsCustAccount.class);
//      String str = getTableNameInsertSql(com.westedge.welfare.starter.common.db.domain.amc.AmsCustAccount.class, map.get(DbUtils.KEY_LIST_COLUMN));
        String str[] = getTableNameUpdateColumns(com.scratch.common.service.db.domain.BaseTicketLoadEntity.class);
        System.out.println(str);
//        Map<String, List<String>> map = DbUtils.getTableColumn(DbTypeEnum.UPDATE, TmcBetDetail_U.class);
//        String str = getTableNameUpdateSql(TmcBetDetail_U.class);
//        System.out.println(str);
//        System.out.println(System.currentTimeMillis());
    }


    /**
     * 返回实体对应数据库的列
     *
     * @param clazz
     * @return 返回所有注解属性
     */
    public static Map<String, List<String>> getTableColumn(DbTypeEnum dbTypeEnum, Class clazz) {
        Class clazz_ = clazz;
        Map<String, List<String>> map = new HashMap<>();
        map.put(KEY_LIST_COLUMN, new ArrayList<>());
        map.put(KEY_LIST_ATTRIBUTE, new ArrayList<>());
        log.debug("读取当前类" + clazz.getSimpleName());
        map = columns(clazz_, map);
        while (DbUtils.isParentClass(clazz_)) {
            clazz_ = clazz.getSuperclass();
            log.debug("读取继承类" + clazz_.getSimpleName());
            map = columns(clazz_, map);
        }
        //过滤掉不需要入库的属性
        List<String> listColumn = map.get(KEY_LIST_COLUMN);
        List<String> listAttribute = map.get(KEY_LIST_ATTRIBUTE);
        switch (dbTypeEnum) {
            case INSERT:
                String[] tableNameExclude = getTableNameInsterExclude(clazz);
                for (int i = 0; i < tableNameExclude.length; i++) {
                    int j = listColumn.indexOf(tableNameExclude[i].toUpperCase());
                    if (j > -1) {
                        listColumn.remove(j);
                        listAttribute.remove(j);
                    }
                }
                if (map.get(KEY_LIST_COLUMN).size() != map.get(KEY_LIST_ATTRIBUTE).size()) {
                    throw new DbException("读取实体属性错误");
                }
                break;
            case UPDATE:
                String[] tableNameUpdateColumns = getTableNameUpdateColumns(clazz);
                tableNameUpdateColumns = JSON.parseObject(JSON.toJSONString(tableNameUpdateColumns).toUpperCase(), String[].class);
                String[] listAttribute_ = new String[tableNameUpdateColumns.length];
                for (int i = 0; i < listColumn.size(); i++) {
                    if (Arrays.asList(tableNameUpdateColumns).contains(listColumn.get(i))) {
                        int index = findIndex(tableNameUpdateColumns, listColumn.get(i));
                        if (index > -1) {
                            listAttribute_[index] = listAttribute.get(i);
                        } else {
                            throw new DbException("未找到update注解中Column对应的Attribute");
                        }
                    }
                }
                listColumn.clear();
                listAttribute.clear();
                map.put(KEY_LIST_ATTRIBUTE, Arrays.asList(listAttribute_));
                if (map.get(KEY_LIST_ATTRIBUTE).size() < 1) {
                    throw new DbException("读取实体属性错误");
                }
                break;
            default:
                break;
        }
        return map;
    }


    public static int findIndex(String[] arr1, String value) {
        int index = -1;
        if (null == arr1 || arr1.length == 0) {
            return index;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i].equals(value)) {
                index = i;
                break;
            }
        }
        return index;
    }


    private static Map<String, List<String>> columns(Class clazz, Map<String, List<String>> map) {
        List<String> listColumn = map.get(KEY_LIST_COLUMN);
        List<String> listAttribute = map.get(KEY_LIST_ATTRIBUTE);
        for (Field declaredField : clazz.getDeclaredFields()) {
            char[] charArr = declaredField.getName().toCharArray();
            if (!Character.isUpperCase(charArr[0])) {
                DbTableColumn fieldAnnotation = declaredField.getDeclaredAnnotation(DbTableColumn.class);
                if (fieldAnnotation != null) {
                    listColumn.add(fieldAnnotation.value().toUpperCase());
                } else {
                    listColumn.add(humpToLine(declaredField.getName()).toUpperCase());
                }
                listAttribute.add(declaredField.getName());

            }
        }
        map.put(KEY_LIST_COLUMN, listColumn);
        map.put(KEY_LIST_ATTRIBUTE, listAttribute);
        return map;
    }


    /**
     * 下划线转驼峰
     */
    public static String lineToHump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }


    /**
     * 驼峰转下划线
     */
    public static String humpToLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 根据属性名获取属性值
     */
    public static String getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter);
            Object value = method.invoke(o);
            return value == null ? "\\N" : value.toString();
        } catch (Exception e) {
            throw new DbException("根据属性名获取属性值错误", e);
        }
    }

    public static Map<String, Object> getFieldValueByNameAndType(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter);
            Object value = method.invoke(o);
//            if (value == null) {
//                throw new DbException("根据属性名获取属性值为空");
//            }
            Map<String, Object> map = new HashMap<>();
            map.put(method.getGenericReturnType().getTypeName(), value);
            return map;
        } catch (Exception e) {
            throw new DbException("根据属性名获取属性值错误", e);
        }
    }

    public static InputStream getFieldListByStream(List<String> jsonList, List<String> listAttribute, Type clazz) {
        try {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < jsonList.size(); j++) {
                Object jsonObject = JSON.parse(jsonList.get(j));
                Object object = JSON.parseObject(jsonObject.toString(), clazz);
                for (int i = 0; i < listAttribute.size(); i++) {
                    if (listAttribute.get(i).equalsIgnoreCase("id")) {
                        builder.append(uuid());
                        builder.append("\t");
                    } else {
                        String value = DbUtils.getFieldValueByName(listAttribute.get(i), object);

                        builder.append(value);
                        if (i == listAttribute.size() - 1) {
                            builder.append("\n");
                        } else {
                            builder.append("\t");
                        }
                    }
                }
            }
            byte[] bytes = builder.toString().getBytes();
            InputStream dataStream = new ByteArrayInputStream(bytes);
            return dataStream;
        } catch (Exception e) {
            throw new DbException("生成数据入库字节流失败", e);
        }
    }

    /**
     * 判断是否有父类
     *
     * @param clazz
     * @return
     */
    public static boolean isParentClass(Class clazz) {
        return !clazz.getSuperclass().getName().equals("java.lang.Object");
    }

    /**
     * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    private static String lowerFirstChar(String str) {
        char[] chars = str.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

}
