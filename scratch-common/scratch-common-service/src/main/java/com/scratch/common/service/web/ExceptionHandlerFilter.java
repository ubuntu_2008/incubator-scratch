package com.scratch.common.service.web;

import com.scratch.common.core.web.enums.ErrorCode;
import com.scratch.common.core.web.result.R;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.stream.Collectors;


/**
 * Created by bpf on 2020/12/22.
 */

@ControllerAdvice
@Slf4j
public class ExceptionHandlerFilter {

    @Value("${spring.application.name}")
    private String appName;


    private String getExptionString(ErrorCode codeEnum) {
        return String.format("[%s] %s", appName, codeEnum.getMessage());
    }


    /**
     * 此处错误可详分
     * 包含PulsarClientException转换为RuntimeException异常此处收录
     *
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R exceptionHandler(Exception e) {
        log.error("{}", ErrorCode.SYS_BUSY.getMessage(), e);
        return R.fail(ErrorCode.SYS_BUSY.getCode(),String.format("%s -- %s", getExptionString(ErrorCode.SYS_BUSY), e.getMessage()));

    }



    /**
     * 处理请求参数格式错误 @RequestBody上validate失败后抛出的异常是MethodArgumentNotValidException异常。
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public R handleBindException(MethodArgumentNotValidException ex) {
        FieldError fieldError = ex.getBindingResult().getFieldError();
        return R.fail(ErrorCode.INVALID_PARAM.getCode(),String.format("%s -- %s", getExptionString(ErrorCode.INVALID_PARAM), String.format("%s(%s)", fieldError.getDefaultMessage(), fieldError.getField())));
    }

    /**
     * 处理Get请求中 使用@Valid 验证路径中请求实体校验失败后抛出的异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public R handleBindException(BindException ex) {
        FieldError fieldError = ex.getBindingResult().getFieldError();
        return R.fail(ErrorCode.INVALID_PARAM.getCode(),String.format("%s -- %s", getExptionString(ErrorCode.INVALID_PARAM), String.format("%s(%s)", fieldError.getDefaultMessage(), fieldError.getField())));
    }

    /**
     * 处理请求参数格式错误 @RequestParam上validate失败后抛出的异常是javax.validation.ConstraintViolationException
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public R ConstraintViolationExceptionHandler(ConstraintViolationException e) {
        String message = e.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining());
        return R.fail(ErrorCode.INVALID_PARAM.getCode(),String.format("%s -- %s", getExptionString(ErrorCode.INVALID_PARAM), message));
    }

}