package com.scratch.common.service.db.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.scratch.common.core.web.entity.base.BaseEntity;
import com.scratch.common.service.db.DbEntity;
import com.scratch.common.service.db.DbTable;
import com.scratch.common.service.db.DbTableColumn;
import lombok.*;

import java.io.Serial;

import static com.scratch.common.core.constant.basic.EntityConstants.*;

/**
 * 订单 持久化对象
 *
 * @author scratch
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@DbTable(
        name = "base_order",
        desc = "订单表",
        updateKey = {"data_version"},
        updateColumns = {"pay_status", "pay_time", "order_status", "ticket_no"}
)
public class BaseOrderEntity extends DbEntity {

    @DbTableColumn("area_id")
    protected Long areaId;
    @DbTableColumn("org_no")
    protected String orgNo;
    @DbTableColumn("cp_no")
    protected String cpNo;
    @DbTableColumn("site_no")
    protected String siteNo;
    @DbTableColumn("dev_no")
    protected String devNo;
    @DbTableColumn("ticket_id")
    protected String ticketId;
    @DbTableColumn("ticket_no")
    protected String ticketNo;
    @DbTableColumn("order_no")
    protected String orderNo;
    @DbTableColumn("order_money")
    protected Integer orderMoney;
    @DbTableColumn("order_time")
    protected Long orderTime;
    @DbTableColumn("order_time_out")
    protected Long orderTimeOut;
    @DbTableColumn("order_desc")
    protected String orderDesc;
    @DbTableColumn("pay_time")
    protected Long payTime;
    @DbTableColumn("refund_time")
    protected Long refundTime;
    @DbTableColumn("buy_time")
    protected Long buyTime;
    @DbTableColumn("order_content")
    protected String orderContent;
    @DbTableColumn("order_type")
    protected Integer orderType;
    @DbTableColumn("order_status")
    protected Integer orderStatus;
    @DbTableColumn("pay_type")
    protected Integer payType;
    @DbTableColumn("pay_status")
    protected Integer payStatus;
    @DbTableColumn("sign")
    protected String sign;

    @DbTableColumn("tenant_id")
    protected Long tenantId;

    @DbTableColumn("create_by")
    protected Long createBy;


}