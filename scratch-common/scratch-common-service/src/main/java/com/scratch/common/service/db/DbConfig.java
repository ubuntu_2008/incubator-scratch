package com.scratch.common.service.db;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by bpf on 2021/01/20.
 */
@Data
@Configuration
@ConfigurationProperties("db")
public class DbConfig {
    /**
     * 默认拉取时间 :正整数,单位毫秒
     */
    private String cron;
    /**
     * 最大批量执行数,单次拉取数
     */
    private int maxLimit;

    private int batchTime;
}
