package com.scratch.common.service.db;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.scratch.core.api.service.domain.TicketItem;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by bpf on 2018/1/31.
 */
@Slf4j
public class DbDao {

    public DataSource dataSource;

    public int insert(Class clazz, List<String> jsonList, List<String> listColumn, List<String> listAttribute) {
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            String sql = DbUtils.getTableNameUpdateColumns(clazz).length > 0 ? DbUtils.getTableNameInsertBulkSql(clazz, listColumn) : DbUtils.getTableNameInsertSql(clazz, listColumn);
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(sql);
            for (int i = 0; i < jsonList.size(); i++) {
                Map<String, Object> map = BeanUtil.beanToMap(jsonList.get(i), false, true);
                for (int j = 0; j < listAttribute.size(); j++) {
                   ps.setObject(j + 1, map.get(listAttribute.get(j)));
                }
                ps.addBatch();
            }
            int rows[] = ps.executeBatch();
            conn.commit();
            result = rows.length;
            conn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                conn.rollback();
                conn.setAutoCommit(true);
            } catch (SQLException e1) {
                throw new DbException("rollback error", e1);
            }
            throw new DbException("execute error", e);
        } finally {
            try {
                if (ps != null) {
                    ps.clearBatch();
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sqle) {
                throw new DbException("close Connection error", sqle);
            }
        }
        return result;
    }

    public int update(Class clazz, List<String> jsonList, List<String> listAttribute) {
        String sql = DbUtils.getTableNameUpdateSql(clazz);
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(sql);
            for (int i = 0; i < jsonList.size(); i++) {
                Object object = BeanUtil.copyProperties(jsonList.get(i),clazz);
                for (int j = 0; j < listAttribute.size(); j++) {
                    Map<String, Object> map = DbUtils.getFieldValueByNameAndType(listAttribute.get(j), object);
                    for (String key : map.keySet()) {
                        ps.setObject(j + 1, map.get(key));
                    }
                }
                ps.addBatch();
            }
            int rows[] = ps.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);
            result = rows.length;
        } catch (Exception e) {
            try {
                conn.rollback();
                conn.setAutoCommit(true);
            } catch (SQLException e1) {
                throw new DbException("rollback error", e1);
            }
            throw new DbException("execute error", e);
        } finally {
            try {
                if (ps != null) {
                    ps.clearBatch();
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sqle) {
                throw new DbException("close Connection error", sqle);
            }
        }
        return result;
    }
}
