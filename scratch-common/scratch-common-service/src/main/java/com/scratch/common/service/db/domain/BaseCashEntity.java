package com.scratch.common.service.db.domain;

import com.scratch.common.service.db.DbTable;
import com.scratch.common.service.db.DbTableColumn;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@DbTable(
        name = "base_cash",
        desc = "兑奖表"
)
public class BaseCashEntity {

    @DbTableColumn("dev_no")
    private String devNo;
    @DbTableColumn("user_id")
    private String userId;
    @DbTableColumn("order_no")
    private String orderNo;
    @DbTableColumn("play_name")
    private String playName;
    @DbTableColumn("data_area")
    private String dataArea;
    @DbTableColumn("ticket_code")
    private String ticketCode;
    @DbTableColumn("win_money")
    private Long winMoney;
    @DbTableColumn("cash_time")
    private Long cashTime;
    @DbTableColumn("status")
    private Integer status;
    @DbTableColumn("sign")
    private String sign;
    @DbTableColumn("create_by")
    private Long createBy;
    @DbTableColumn("tenant_id")
    private Long tenantId;
}
