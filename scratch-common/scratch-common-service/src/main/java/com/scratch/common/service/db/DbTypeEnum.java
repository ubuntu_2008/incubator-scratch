package com.scratch.common.service.db;

/**
 * Created by bpf on 2021/01/20.
 */

public enum DbTypeEnum {
    INSERT("1", "插入"),
    UPDATE("2", "修改"),
    INSERT_AND_UPDATE("3", "插入与修改");

    private String code;
    private String desc;

    DbTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public static DbTypeEnum getByCode(String code) {
        for (DbTypeEnum typeEum : values()) {
            if (typeEum.getCode().equals(code)) {
                return typeEum;
            }
        }
        return null;
    }

}
