package com.scratch.common.service.db;

public interface DbDefaultParam {
    public static final int CRON = 3000;
    public static final int MAX_LIMIT = 5000;
    public static final int BATCH_TIME = 500;

    public static final int LOCK_TIME = 20000;

}
