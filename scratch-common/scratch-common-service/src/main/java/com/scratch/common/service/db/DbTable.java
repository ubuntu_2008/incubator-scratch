package com.scratch.common.service.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by bpf on 2021/01/20.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD, ElementType.TYPE})
public @interface DbTable {
    /**
     * 数据库表名
     *
     * @return
     */
    String name();

    /**
     * 备注 日志输出方便阅读
     *
     * @return
     */
    String desc();

    /**
     * 默认启动插入任务
     * 即如只有插入任务无需配置此项
     *
     * @return
     */
    DbTypeEnum type() default DbTypeEnum.INSERT;


    /**
     * 任务间隔时间 默认读取的是配置文件的cron  单位毫秒
     */
    int cron() default DbDefaultParam.CRON;


    /**
     * 单次执行最大数据量  默认读取的是配置文件的maxLimit
     * @return
     */
    int maxLimit() default DbDefaultParam.MAX_LIMIT;


    /**
     * 锁超时时间
     * @return
     */
    int lockTime() default   DbDefaultParam.LOCK_TIME;

    /**
     * @return
     */
    int batchTime() default  DbDefaultParam.BATCH_TIME;

    /**
     * 插入--排除某列,列名与数据库一致
     *
     * @return
     */
    String[] insertExcludeColumns() default {};

    /**
     * 修改--主键
     *
     * @return
     */
    String[] updateKey() default {};


    /**
     * 修改--指定需要修改的列
     *
     * @return
     */
    String[] updateColumns() default {};
}
