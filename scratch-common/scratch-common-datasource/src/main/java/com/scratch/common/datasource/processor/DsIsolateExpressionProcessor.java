package com.scratch.common.datasource.processor;

import com.baomidou.dynamic.datasource.processor.DsProcessor;
import com.scratch.common.datasource.utils.DSUtil;
import com.scratch.common.security.utils.SecurityUtils;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

import static com.scratch.common.core.constant.basic.TenantConstants.ISOLATE;

/**
 * 租户库源策略
 *
 * @author scratch
 */
@Component
public class DsIsolateExpressionProcessor extends DsProcessor {

    @Override
    public boolean matches(String key) {
        return key.startsWith(ISOLATE);
    }

    @Override
    public String doDetermineDatasource(MethodInvocation invocation, String key) {
        return DSUtil.loadDs(SecurityUtils.getSourceName());
    }
}