package com.scratch.common.core.web.enums;

import com.scratch.common.core.web.result.R;

public enum ErrorCode {


    SYS_BUSY(1000, "系统忙"),
    INVALID_PARAM(10001, "无效参数"),
    TERMINAL_GETPLAYLIST_TERMINAL_NOT_FOUND(10001, "终端未发现"),
    TERMINAL_GETPLAYLIST_TERMINAL_UNAUTHORIZED(10002, "站点未授权"),

    TERMINAL_GETPLAYLIST_SITE_UNAUTHORIZED_SELL_OR_CASH_PERMISSION(10003, "站点未授权销售或兑奖权限"),
    TERMINAL_GETPLAYLIST_CP_UNAUTHORIZED_SELL_OR_CASH_PERMISSION(10004, "渠道未授权销售或兑奖权限"),
    TERMINAL_GETPLAYLIST_ORG_UNAUTHORIZED_SELL_OR_CASH_PERMISSION(10005, "机构未授权销售或兑奖权限"),

    TERMINAL_GETPLAYLIST_SITE_UNAUTHORIZED_PLAY(10006, "站点未授权游戏"),
    TERMINAL_GETPLAYLIST_CP_UNAUTHORIZED_PLAY(10007, "渠道未授权游戏"),
    TERMINAL_GETPLAYLIST_ORG_UNAUTHORIZED_PLAY(10008, "机构未授权游戏"),

    TERMINAL_GETPLAYLIST_RETAINALL_UNAUTHORIZED_PLAY(10009, "无有效授权游戏"),

    TERMINAL_LOADTICKETS_TICKETID(10010, "错误票ID"),
    TERMINAL_LOADTICKETS_HP_CONFIG(10011, "机头/仓头未配置"),
    TERMINAL_LOADTICKETS_HPID(10012, "机头/仓头ID错误"),
    TERMINAL_ORDER_TICKET_COUNT(10013, "票余量不足"),
    TERMINAL_ORDER_MONEY(10014, "总金额错误"),

    TERMINAL_PAY_NOTIFY_ORDER_DOES_NOT_EXIST(10015, "订单不存在"),
    TERMINAL_PAY_NOTIFY_ORDER_TIMEOUT(10016, "订单超时"),
    TERMINAL_PAY_RESULT_ORDER_DOES_NOT_EXIST(10017, "订单不存在"),

    TERMINAL_ISSUE_TICKETS_ORDER_DOES_NOT_EXIST(10018, "订单不存在"),
    TERMINAL_ISSUE_TICKETS_ORDER_DOES_NOT_PAY(10019, "订单未支付"),
    TERMINAL_CASH_DATAAREA(10020,"请扫描正确的保安码"),


    ;

    private int code;
    private String message;

    ErrorCode(
            int code,
            String message) {
        this.code = code;
        this.message = message;
    }

    public static ErrorCode getEnum(
            int code) {
        for (ErrorCode ele : ErrorCode.values()) {
            if (ele.getCode() == code) {
                return ele;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(
            int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(
            String message) {
        this.message = message;
    }
}
