package com.scratch.common.core.utils;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import org.springframework.util.DigestUtils;

import java.util.*;
import java.util.stream.Collectors;

public final class SignUtil {
    private static final SignUtil SIGN_UTILS = new SignUtil();

    private SignUtil() {
    }

    public static SignUtil getInstance() {
        return SIGN_UTILS;
    }

    public static String generateSign(String signKey, Map<String, String> params) {
        List<String> storedKeys = (List)Arrays.stream(params.keySet().toArray(new String[0])).sorted(Comparator.naturalOrder()).collect(Collectors.toList());
        String sign = ((String)storedKeys.stream().filter((key) -> {
            return !Objects.equals(key, "sign");
        }).map((key) -> {
            return String.join("", key, (CharSequence)params.get(key));
        }).collect(Collectors.joining())).trim().concat(signKey);
        return DigestUtils.md5DigestAsHex(sign.getBytes()).toUpperCase();
    }

    public boolean isValid(String sign, Map<String, String> params, String signKey) {
        return Objects.equals(sign, generateSign(signKey, params));
    }

    public String generateKey() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }
}
