package com.scratch.common.core.exception;

import java.io.Serial;

public class SignException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 1L;

    public SignException(String message) {
        super(message);
    }

}
