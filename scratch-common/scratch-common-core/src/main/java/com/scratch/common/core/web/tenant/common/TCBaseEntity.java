package com.scratch.common.core.web.tenant.common;

import com.baomidou.mybatisplus.annotation.TableField;
import com.scratch.common.core.web.entity.common.CBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * Base 租户混合基类
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TCBaseEntity extends CBaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 租户Id */
    @TableField(exist = false)
    protected Long enterpriseId;

}
