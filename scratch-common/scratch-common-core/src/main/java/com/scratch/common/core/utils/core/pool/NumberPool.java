package com.scratch.common.core.utils.core.pool;

/**
 * 常用数字常量
 *
 * @author scratch
 */
public interface NumberPool {

    int Ne_One = -1;
    int Zero = 0;
    int One = 1;
    int Two = 2;
    int Three = 3;
    int Four = 4;
    int Five = 5;
    int Six = 6;
    int Seven = 7;
    int Eight = 8;
    int Nine = 9;
    int Ten = 10;
}
