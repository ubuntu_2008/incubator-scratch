package com.scratch.common.core.constant.basic;

/**
 * 服务名称通用常量
 *
 * @author scratch
 */
public class ServiceConstants {

    /** 认证服务的serviceId */
    public static final String AUTH_SERVICE = "scratch-auth";

    /** 文件服务的serviceId */
    public static final String FILE_SERVICE = "scratch-file";

    /** 系统模块的serviceId */
    public static final String SYSTEM_SERVICE = "scratch-system";

    /** 租管模块的serviceId */
    public static final String TENANT_SERVICE = "scratch-tenant";

    /** 定时任务模块的serviceId */
    public static final String JOB_SERVICE = "scratch-job";
}
