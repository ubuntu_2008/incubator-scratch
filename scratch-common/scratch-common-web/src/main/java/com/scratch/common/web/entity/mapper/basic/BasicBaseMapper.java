package com.scratch.common.web.entity.mapper.basic;

import com.scratch.common.core.web.entity.base.BaseEntity;

/**
 * 数据层 基类数据处理
 * 仅用于基类调用 禁止被继承使用
 *
 * @author scratch
 */
public interface BasicBaseMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<BaseEntity> {
}
