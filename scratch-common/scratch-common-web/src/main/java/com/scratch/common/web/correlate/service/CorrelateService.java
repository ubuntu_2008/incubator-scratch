package com.scratch.common.web.correlate.service;

import com.scratch.common.web.correlate.domain.BaseCorrelate;

import java.util.List;

/**
 * 关联映射 枚举映射器
 */
public interface CorrelateService {

    List<? extends BaseCorrelate<?>> getCorrelates();
}
