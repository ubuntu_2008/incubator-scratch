package com.scratch.common.redis.constant;

import com.scratch.common.core.utils.core.EnumUtil;
import com.scratch.common.core.utils.core.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 缓存 通用常量
 *
 * @author scratch
 */
public class RedisConstants {

    /**
     * 缓存键
     */
    @Getter
    @AllArgsConstructor
    public enum CacheKey {

        CLIENT_DETAILS_KEY("client:details", "oauth 客户端信息"),
        CAPTCHA_CODE_KEY("captcha_codes:", "验证码"),
        SYS_CORRELATE_KEY("system:correlate:{}.{}", "数据关联工具"),
        SYS_CORRELATE_IMPL_KEY("system:correlate:service:{}", "数据关联工具"),
        DICT_KEY("system:dict:{}", "字典缓存"),
        CONFIG_KEY("system:config:{}", "参数缓存");

        private final String code;
        private final String info;

        public String getCacheName(Object... cacheNames) {
            return StrUtil.format(getCode(), cacheNames);
        }
    }

    /**
     * 缓存类型
     */
    @Getter
    @AllArgsConstructor
    public enum OperateType {

        REFRESH_ALL("refresh_all", "更新全部"),
        REFRESH("refresh", "更新"),
        REMOVE("remove", "删除");

        private final String code;
        private final String info;

        public static OperateType getByCode(String code) {
            return EnumUtil.getByCode(OperateType.class, code);
        }

    }


    @Getter
    @AllArgsConstructor
    public enum BaseKey {
        BASE_ORG_NO("core:org_no", "自增INCR"),
        BASE_CP_NO("core:cp_no", "自增INCR"),
        BASE_SITE_NO("core:site_no", "自增INCR"),

        DB_LOCK("core:db:lock:", "DB锁"),
        DB_LIST("core:db:list:", "DB队列"),

        BASE_TICKET_LOAD("core:ticket_load:{}", "票余量内存数据"),

        BASE_ORDER("core:order", "订单内存数据"),
        BASE_CASH("core:cash", "兑奖内存数据"),
        BASE_CASH_AUTH("core:cash_auth", "授权登录"),
        DATA_VERSION("core:data:version:{}:{}", "数据:版本:{表}:{唯一ID}");
        private final String code;
        private final String info;

        public static BaseKey getByCode(String code) {
            return EnumUtil.getByCode(BaseKey.class, code);
        }

    }

}
