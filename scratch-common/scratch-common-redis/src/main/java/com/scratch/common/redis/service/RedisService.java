package com.scratch.common.redis.service;

import com.scratch.common.core.exception.UtilException;
import com.scratch.common.core.utils.core.ArrayUtil;
import com.scratch.common.core.utils.core.CollUtil;
import com.scratch.common.core.utils.core.MapUtil;
import com.scratch.common.core.utils.core.NumberUtil;
import com.scratch.common.core.utils.core.ObjectUtil;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.redis.constant.BaseCoreConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * spring redis 工具类
 *
 * @author scratch
 **/
@Component
@SuppressWarnings(value = {"unchecked", "rawtypes"})
public class RedisService {

    @Autowired
    public RedisTemplate redisTemplate;

    @Autowired
    @Qualifier("redisJavaTemplate")
    public RedisTemplate redisJavaTemplate;

    /**
     * 缓存基本的对象，Integer、String、实体类等 | Java序列化
     *
     * @param key   Redis键
     * @param value 缓存的值
     */
    public <T> void setJavaCacheObject(final String key, final T value) {
        setJavaCacheObject(key, value, NumberUtil.Nine, TimeUnit.HOURS);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等 | Java序列化
     *
     * @param key      Redis键
     * @param value    缓存的值
     * @param timeout  时间
     * @param timeUnit 时间颗粒度
     */
    public <T> void setJavaCacheObject(final String key, final T value, final long timeout, final TimeUnit timeUnit) {
        redisJavaTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    /**
     * 获得缓存的基本对象 | Java序列化
     *
     * @param key Redis键
     * @return 缓存键值对应的数据
     */
    public <T> T getJavaCacheObject(final String key) {
        ValueOperations<String, T> operation = redisJavaTemplate.opsForValue();
        return operation.get(key);
    }

    /**
     * 缓存Map | Java序列化
     *
     * @param key     Redis键
     * @param dataMap map
     */
    public <T> void setJavaCacheMap(final String key, final Map<String, T> dataMap) {
        setJavaCacheMap(key, dataMap, NumberUtil.Nine, TimeUnit.HOURS);
    }

    /**
     * 缓存Map | Java序列化
     *
     * @param key     Redis键
     * @param dataMap map
     * @param timeout 超时时间
     * @param unit    时间单位
     */
    public <T> void setJavaCacheMap(final String key, final Map<String, T> dataMap, final long timeout, final TimeUnit unit) {
        if (dataMap != null) {
            redisJavaTemplate.opsForHash().putAll(key, dataMap);
            expireJava(key, timeout, unit);
        }
    }

    /**
     * 获得缓存的Map | Java序列化
     *
     * @param key Redis键
     * @return 哈希
     */
    public <T> Map<String, T> getJavaCacheMap(final String key) {
        return redisJavaTemplate.opsForHash().entries(key);
    }

    /**
     * 往Hash中存入数据 | Java序列化
     *
     * @param key   Redis键
     * @param hKey  Hash键
     * @param value 值
     */
    public <T> void setJavaCacheMapValue(final String key, final String hKey, final T value) {
        redisJavaTemplate.opsForHash().put(key, hKey, value);
    }

    /**
     * 获取Hash中的数据 | Java序列化
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    public <T> T getJavaCacheMapValue(final String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = redisJavaTemplate.opsForHash();
        return opsForHash.get(key, hKey);
    }

    /**
     * 判断 key是否存在 | Java序列化
     *
     * @param key Redis键
     * @return true 存在 false不存在
     */
    public Boolean hasJavaKey(String key) {
        return redisJavaTemplate.hasKey(key);
    }

    /**
     * 设置有效时间 | Java序列化
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @param unit    时间单位
     * @return true=设置成功；false=设置失败
     */
    public Boolean expireJava(final String key, final long timeout, final TimeUnit unit) {
        return redisJavaTemplate.expire(key, timeout, unit);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key   Redis键
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key      Redis键
     * @param value    缓存的值
     * @param timeout  时间
     * @param timeUnit 时间颗粒度
     */
    public <T> void setCacheObject(final String key, final T value, final Long timeout, final TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(final String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @param unit    时间单位
     * @return true=设置成功；false=设置失败
     */
    public Boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    /**
     * 获取有效时间
     *
     * @param key Redis键
     * @return 有效时间
     */
    public Long getExpire(final String key) {
        return redisTemplate.getExpire(key);
    }

    /**
     * 判断 key是否存在
     *
     * @param key Redis键
     * @return true 存在 false不存在
     */
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 获得缓存的基本对象
     *
     * @param key Redis键
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final String key) {
        ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key);
    }

    /**
     * 删除单个对象
     *
     * @param key Redis键
     * @return true=删除成功；false=删除失败
     */
    public Boolean deleteObject(final String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 删除集合对象
     *
     * @param collection 多个对象
     * @return 结果
     */
    public Long deleteObject(final Collection collection) {
        return redisTemplate.delete(collection);
    }

    /**
     * 缓存List数据
     *
     * @param key      Redis键
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    public <T> long setCacheList(final String key, final List<T> dataList) {
        Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
        return count == null ? 0 : count;
    }

    /**
     * 缓存List数据
     *
     * @param key      Redis键
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    public <T> long setCacheList(final String key, final List<T> dataList, final long timeout, final TimeUnit unit) {
        Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
        if (ObjectUtil.isNotNull(count)) {
            expire(key, timeout, unit);
        }
        return count == null ? 0 : count;
    }

    /**
     * 获得缓存的list对象
     *
     * @param key Redis键
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(final String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    /**
     * 缓存Set
     *
     * @param key     Redis键
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> BoundSetOperations<String, T> setCacheSet(final String key, final Set<T> dataSet) {
        BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
        for (T t : dataSet) {
            setOperation.add(t);
        }
        return setOperation;
    }

    /**
     * 获得缓存的set
     *
     * @param key Redis键
     * @return 集合
     */
    public <T> Set<T> getCacheSet(final String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 缓存Map
     *
     * @param key     Redis键
     * @param dataMap map
     */
    public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
        if (dataMap != null) {
            redisTemplate.opsForHash().putAll(key, dataMap);
        }
    }

    /**
     * 缓存Map
     *
     * @param key     Redis键
     * @param dataMap map
     * @param timeout 超时时间
     * @param unit    时间单位
     */
    public <T> void setCacheMap(final String key, final Map<String, T> dataMap, final long timeout, final TimeUnit unit) {
        if (dataMap != null) {
            redisTemplate.opsForHash().putAll(key, dataMap);
            expire(key, timeout, unit);
        }
    }

    /**
     * 获得缓存的Map
     *
     * @param key Redis键
     * @return 哈希
     */
    public <T> Map<String, T> getCacheMap(final String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 往Hash中存入数据
     *
     * @param key   Redis键
     * @param hKey  Hash键
     * @param value 值
     */
    public <T> void setCacheMapValue(final String key, final String hKey, final T value) {
        redisTemplate.opsForHash().put(key, hKey, value);
    }

    /**
     * 获取Hash中的数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    public <T> T getCacheMapValue(final String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
        return opsForHash.get(key, hKey);
    }

    /**
     * 获取多个Hash中的数据
     *
     * @param key   Redis键
     * @param hKeys Hash键集合
     * @return Hash对象集合
     */
    public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hKeys) {
        return redisTemplate.opsForHash().multiGet(key, hKeys);
    }

    /**
     * 删除Hash中的某条数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return 是否成功
     */
    public boolean deleteCacheMapValue(final String key, final String hKey) {
        return redisTemplate.opsForHash().delete(key, hKey) > 0;
    }

    /**
     * 删除Hash中的某条数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return 是否成功
     */
    public boolean deleteCacheMapValue(final String key, final Object[] hKey) {
        return redisTemplate.opsForHash().delete(key, hKey) > 0;
    }

    /**
     * 获得缓存的基本对象列表
     *
     * @param pattern 字符串前缀
     * @return 对象列表
     */
    public Collection<String> keys(final String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 更新LIST缓存字典
     *
     * @param optionKey 集合缓存键值
     * @param dataList  数据集合
     */
    public <T> void refreshListCache(String optionKey, List<T> dataList) {
        if (optionKey.startsWith(BaseCoreConstants.BASE_CORE_REDIS_PREFIX)) {
            setCacheList(optionKey, dataList);
        } else {
            setCacheList(optionKey, dataList, NumberUtil.Nine, TimeUnit.HOURS);
        }
    }

    /**
     * 全量更新MAP缓存字典
     *
     * @param mapKey    缓存键值
     * @param cacheList 缓存数据集合
     * @param keyGet    键名
     * @param valueGet  值名
     */
    public <T, K> void refreshMapCache(String mapKey, Collection<T> cacheList, Function<? super T, String> keyGet, Function<? super T, K> valueGet) {
        Map<String, K> resultMap = new HashMap<>();
        if (CollUtil.isNotEmpty(cacheList)) {
            resultMap = cacheList.stream()
                    .filter(item -> {
                        if (StrUtil.isEmpty(keyGet.apply(item))) {
                            throw new UtilException("cache key cannot be empty");
                        }
                        return ObjectUtil.isNotNull(valueGet.apply(item));
                    }).collect(Collectors.toMap(keyGet, valueGet));
        }
        if (MapUtil.isNotEmpty(resultMap)) {
            if (mapKey.startsWith(BaseCoreConstants.BASE_CORE_REDIS_PREFIX)) {
                setCacheMap(mapKey, resultMap);
            } else {
                setCacheMap(mapKey, resultMap, NumberUtil.Nine, TimeUnit.HOURS);
            }
        } else {
            deleteObject(mapKey);
        }
    }

    /**
     * 更新MAP缓存字典
     *
     * @param mapKey   缓存键值
     * @param keyGet   键名
     * @param valueGet 值名
     */
    public <T> void refreshMapValueCache(String mapKey, Supplier<Serializable> keyGet, Supplier<T> valueGet) {
        if (ObjectUtil.isNotNull(valueGet.get())) {
            setCacheMapValue(mapKey, keyGet.get().toString(), valueGet.get());
        } else {
            deleteCacheMapValue(mapKey, keyGet.get().toString());
        }
        if (hasKey(mapKey) && !mapKey.startsWith(BaseCoreConstants.BASE_CORE_REDIS_PREFIX)) {
            expire(mapKey, NumberUtil.Nine, TimeUnit.HOURS);
        }
    }

    /**
     * 删除MAP缓存字典指定键数据
     *
     * @param mapKey 缓存键值
     * @param keys   键名
     */
    public void removeMapValueCache(String mapKey, Serializable... keys) {
        if (ArrayUtil.isNotEmpty(keys)) {
            deleteCacheMapValue(mapKey, keys);
        }
    }


    //##############################################################################

    public List<String> lrange(final String key, final Long start, final Long end) {
        List<String> result = redisTemplate.opsForList().range(key, start, end);
        return result;
    }
    public Long rpush(final String key, final String value) {
        Long result = (Long) redisTemplate.execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection redisConnection) throws DataAccessException {
                byte keys[] = getRedisSerializer().serialize(key);
                byte values[] = getRedisSerializer().serialize(value);
                return redisConnection.rPush(keys, values);
            }
        });
        return result;
    }

    public Long llen(final String key) {
        Long result = redisTemplate.opsForList().size(key);
        return result;
    }

    public Void ltrim(final String key, final Long start, final Long end) {
        Void result = (Void) redisTemplate.execute(new RedisCallback<Void>() {
            @Override
            public Void doInRedis(RedisConnection redisConnection) throws DataAccessException {
                RedisSerializer<String> serializer = getRedisSerializer();
                byte keys[] = serializer.serialize(key);
                redisConnection.lTrim(keys, start, end);
                return null;
            }
        });
        return result;
    }

    public boolean acquireLock(String lock, long lockTime) {
        // 1. 通过SETNX试图获取一个lock
        boolean success = false;
        long value = System.currentTimeMillis() + lockTime;
        //SETNX成功，则成功获取一个锁
        if (setnx(lock, String.valueOf(value))) {
            success = true;
        }//SETNX失败，说明锁仍然被其他对象保持，检查其是否已经超时
        else {
            String oldValue_key = get(lock);
            if (StringUtils.isNotBlank(oldValue_key)) {
                long oldValue = Long.valueOf(oldValue_key);
                //超时
                if (oldValue < System.currentTimeMillis()) {
                    String getValue = getSet(lock, String.valueOf(value));
                    // 获取锁成功
                    if (Long.valueOf(getValue) == oldValue) {
                        success = true;
                    }// 已被其他进程捷足先登了
                    else {
                        success = false;
                    }
                }
                //未超时，则直接返回失败
                else {
                    success = false;
                }
            }

        }
        return success;
    }


    // 释放锁
    public void releaseLock(String lock) {
        long current = System.currentTimeMillis();
        // 避免删除非自己获取得到的锁
        String lockTime = get(lock);
        if (StringUtils.isNotBlank(lockTime)) {
            if (current < Long.valueOf(lockTime)) {
                delete(lock);
            }
        }
    }

    public String get(final String key) {
        String result = String.valueOf(redisTemplate.opsForValue().get(key));
        return result;
    }

    public String getSet(final String key, final String keyValue) {
        String result = String.valueOf(redisTemplate.opsForValue().getAndSet(key, keyValue));
        return result;
    }

    public Boolean setnx(final String key, final String value) {
        Boolean result = (Boolean) redisTemplate.execute(new RedisCallback<Boolean>() {
            @Override
            public Boolean doInRedis(RedisConnection redisConnection) throws DataAccessException {
                byte keys[] = getRedisSerializer().serialize(key);
                byte values[] = getRedisSerializer().serialize(value);
                return redisConnection.setNX(keys, values);
            }
        });
        return result;
    }

    protected RedisSerializer<String> getRedisSerializer() {
        RedisSerializer<String> redisSerializer = redisTemplate.getStringSerializer();
        return redisSerializer;
    }

    public Boolean delete(String key) {
        Boolean result = redisTemplate.delete(key);
        return result;

    }
}
