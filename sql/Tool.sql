#机构玩法授权
SELECT a.id AS baseTicketId,a.img,a.play_name AS playName ,a.play_no AS playNo,IF(b.auth_sell=1 OR b.auth_sell IS NULL,'',0)  AS authSell,IF(b.auth_cash=1 OR b.auth_cash IS NULL,'',0) AS authCash FROM base_ticket a
LEFT JOIN   `base_org_auth_play` b ON
a.id=b.base_ticket_id  AND
b.base_org_id='1711258181287362562'  
WHERE  a.del_flag=0 ORDER BY a.create_time DESC

#渠道玩法授权
SELECT a.id AS baseTicketId,a.img,a.play_name AS playName ,a.play_no AS playNo,IF(b.auth_sell=1 OR b.auth_sell IS NULL,'',0)  AS authSell,IF(b.auth_cash=1 OR b.auth_cash IS NULL,'',0) AS authCash FROM base_ticket a
LEFT JOIN   `base_cp_auth_play` b ON
a.id=b.base_ticket_id  AND
b.base_cp_id='1712723541601566722'  
WHERE  a.del_flag=0 ORDER BY a.create_time DESC

#网点玩法授权
SELECT a.id AS baseTicketId,a.img,a.play_name AS playName ,a.play_no AS playNo,IF(b.auth_sell=1 OR b.auth_sell IS NULL,'',0)  AS authSell,IF(b.bkge_sell IS NULL,0,b.bkge_sell) AS bkgeSell,IF(b.auth_cash=1 OR b.auth_cash IS NULL,'',0) AS authCash ,IF(b.bkge_cash IS NULL,0,b.bkge_cash) AS bkgeCash	 FROM base_ticket a
LEFT JOIN   `base_site_auth_play` b ON
a.id=b.base_ticket_id  AND
b.base_site_id='1713835743591100418'  
WHERE  a.del_flag=0 ORDER BY a.create_time DESC
#修改图片IP地址
UPDATE `sys_file` SET url=REPLACE(url, '192.168.31.4', '192.168.31.62')

UPDATE `sys_module` SET logo=REPLACE(logo, '127.0.0.1', '192.168.31.62')


UPDATE `base_ticket` SET img=REPLACE(img, '127.0.0.1', '192.168.31.62')


CREATE TABLE `base_order` (
  `id` VARCHAR(32) NOT NULL COMMENT 'id 物理主键 UUID',
  `orderNo` VARCHAR(32) DEFAULT NULL COMMENT '订单编号 ',
  `cpNo` VARCHAR(32) DEFAULT NULL COMMENT '渠道编号 根据渠道表定义长度',
  `userPhone` VARCHAR(20) DEFAULT NULL,
  `custNo` VARCHAR(32) DEFAULT NULL COMMENT '用户编号 普通单索引',
  `acctNo` VARCHAR(32) DEFAULT NULL COMMENT '账号编号 普通单索引',
  `wxCode` VARCHAR(20) DEFAULT NULL,
  `openId` VARCHAR(64) DEFAULT NULL,
  `ticketNo` VARCHAR(32) DEFAULT NULL COMMENT '票号 ',
  `payNo` VARCHAR(32) DEFAULT NULL COMMENT '账单流水 ',
  `thirdNo` VARCHAR(32) DEFAULT NULL,
  `orderMoney` INT(11) DEFAULT NULL COMMENT '订单金额 单位（分）',
  `orderDate` BIGINT(20) DEFAULT NULL COMMENT '订单时间 ',
  `orderType` SMALLINT(6) DEFAULT NULL COMMENT '订单类型 订单类型(1 - 彩票)',
  `orderContent` JSON DEFAULT NULL COMMENT '订单内容 json格式投注内容',
  `orderStatus` SMALLINT(6) DEFAULT NULL COMMENT '订单状态 订单状态(0 - 待支付; 1 - 已支付; 2 -  超时支付; 3  - 待退款; 4 - 退款成功; 5 - 退款失败; 9  - 成功处理)',
  `orderDesc` VARCHAR(100) DEFAULT NULL COMMENT '订单说明 订单说明',
  `devMac` VARCHAR(32) DEFAULT NULL,
  `devNo` VARCHAR(32) DEFAULT NULL,
  `payDate` BIGINT(13) DEFAULT NULL COMMENT '付款时间 ',
  `buyDate` BIGINT(20) DEFAULT NULL COMMENT '出票时间',
  `refundDate` BIGINT(20) DEFAULT NULL COMMENT '退款时间',
  `finalDate` BIGINT(20) DEFAULT NULL COMMENT '订单失效操作时间',
  `insertTime` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间 CURRENT_TIMESTAMP',
  `sign` VARCHAR(32) DEFAULT NULL COMMENT '签名'
) ENGINE=INNODB DEFAULT CHARSET=utf8


SELECT * FROM t_oms_order_1211;
 
 
 
 
 
SELECT * FROM  `base_org`
SELECT * FROM `base_site`

SELECT * FROM `te_source`



 


 
