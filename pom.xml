<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.scratch</groupId>
    <artifactId>scratch</artifactId>
    <version>3.0.0-RC</version>

    <name>scratch</name>
    <description>管理系统</description>

    <properties>
        <log.path>/home/logs</log.path>
        <deploy.path>../.deploy/${project.artifactId}</deploy.path>
        <script.path>../.script/</script.path>
        <java.opts.bit32>
            -server -Xms128m -Xmx128m -XX:PermSize=128m -XX:SurvivorRatio=2 -XX:+UseParallelGC
        </java.opts.bit32>
        <java.opts.bit64>
            -server -Xms128m -Xmn128m -Xmx128m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=128m
        </java.opts.bit64>
        <scratch.version>3.0.0-RC</scratch.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven-compiler-plugin.version>3.11.0</maven-compiler-plugin.version>
        <java.version>17</java.version>
        <spring-boot.version>3.1.3</spring-boot.version>
        <spring-cloud.version>2022.0.4</spring-cloud.version>
        <spring-cloud-alibaba.version>2022.0.0.0</spring-cloud-alibaba.version>
        <spring.authorization.version>1.1.2</spring.authorization.version>
        <spring-boot-admin.version>3.1.5</spring-boot-admin.version>
        <mybatis-plus.version>3.5.3.1</mybatis-plus.version>
        <pagehelper.version>5.3.2</pagehelper.version>
        <spring-doc.version>2.2.0</spring-doc.version>
        <tobato.version>1.27.2</tobato.version>
        <kaptcha.version>2.3.3</kaptcha.version>
        <dynamic-ds.version>4.1.3</dynamic-ds.version>
        <commons.io.version>2.13.0</commons.io.version>
        <velocity.version>2.3</velocity.version>
        <fastjson2.version>2.0.39</fastjson2.version>
        <minio.version>8.5.1</minio.version>
        <poi.version>5.2.3</poi.version>
        <hutool.version>5.8.20</hutool.version>
        <jjwt.version>0.11.5</jjwt.version>
        <lombok.version>1.18.28</lombok.version>
        <mapstruct.version>1.5.5.Final</mapstruct.version>
        <lombok-mapstruct-binding.version>0.2.0</lombok-mapstruct-binding.version>
        <transmittable-thread-local.version>2.14.3</transmittable-thread-local.version>
        <sms4j.version>2.2.0</sms4j.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
    </properties>

    <!-- 依赖声明 -->
    <dependencyManagement>
        <dependencies>

            <!-- SpringCloud 微服务 -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- SpringCloud Alibaba 微服务 -->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring-cloud-alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- SpringBoot 依赖配置 -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- FastDFS 分布式文件系统 -->
            <dependency>
                <groupId>com.github.tobato</groupId>
                <artifactId>fastdfs-client</artifactId>
                <version>${tobato.version}</version>
            </dependency>

            <!-- Mybatis-Plus 依赖配置 -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>

            <!-- Dynamic DataSource -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>dynamic-datasource-spring-boot3-starter</artifactId>
                <version>${dynamic-ds.version}</version>
            </dependency>

            <!-- Oauth2 Authorization Server -->
            <dependency>
                <groupId>org.springframework.security</groupId>
                <artifactId>spring-security-oauth2-authorization-server</artifactId>
                <version>${spring.authorization.version}</version>
            </dependency>

            <!-- pageHelper 分页插件 -->
            <dependency>
                <groupId>com.github.pagehelper</groupId>
                <artifactId>pagehelper</artifactId>
                <version>${pagehelper.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.mybatis</groupId>
                        <artifactId>mybatis</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.mybatis</groupId>
                        <artifactId>mybatis-spring</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>com.github.jsqlparser</groupId>
                        <artifactId>jsqlparser</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <!-- lombok -->
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>

            <!-- mapstruct 转换器 -->
            <dependency>
                <groupId>org.mapstruct</groupId>
                <artifactId>mapstruct</artifactId>
                <version>${mapstruct.version}</version>
            </dependency>

            <!-- Swagger Webmvc 依赖配置 -->
            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
                <version>${spring-doc.version}</version>
            </dependency>

            <!-- Swagger Webflux 依赖配置 -->
            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-webflux-ui</artifactId>
                <version>${spring-doc.version}</version>
            </dependency>

            <!-- Minio -->
            <dependency>
                <groupId>io.minio</groupId>
                <artifactId>minio</artifactId>
                <version>${minio.version}</version>
            </dependency>

            <!-- 验证码 -->
            <dependency>
                <groupId>pro.fessional</groupId>
                <artifactId>kaptcha</artifactId>
                <version>${kaptcha.version}</version>
            </dependency>

            <!-- io常用工具类 -->
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${commons.io.version}</version>
            </dependency>

            <!-- excel工具 -->
            <dependency>
                <groupId>org.apache.poi</groupId>
                <artifactId>poi-ooxml</artifactId>
                <version>${poi.version}</version>
            </dependency>

            <!-- 代码生成使用模板 -->
            <dependency>
                <groupId>org.apache.velocity</groupId>
                <artifactId>velocity-engine-core</artifactId>
                <version>${velocity.version}</version>
            </dependency>

            <!-- JSON 解析器和生成器 -->
            <dependency>
                <groupId>com.alibaba.fastjson2</groupId>
                <artifactId>fastjson2</artifactId>
                <version>${fastjson2.version}</version>
            </dependency>

            <!-- JWT -->
            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt-api</artifactId>
                <version>${jjwt.version}</version>
            </dependency>

            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt-impl</artifactId>
                <version>${jjwt.version}</version>
                <scope>runtime</scope>
            </dependency>

            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt-jackson</artifactId>
                <version>${jjwt.version}</version>
                <scope>runtime</scope>
            </dependency>

            <!-- 线程传递值 -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>transmittable-thread-local</artifactId>
                <version>${transmittable-thread-local.version}</version>
            </dependency>

            <!-- 核心模块 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-core</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 通信模块 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-web</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 接口模块 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-swagger</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 安全模块 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-security</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 权限范围 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-datascope</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 多数据源 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-datasource</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 通用开发依赖 - Data -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-dependency-data</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 通用开发依赖 - Nacos -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-dependency-nacos</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 日志记录 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-log</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 缓存管理 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-cache</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 缓存服务 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-redis</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 分布式事务 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-seata</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 系统接口 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-api-core</artifactId>
                <version>${scratch.version}</version>
            </dependency>
            <!-- 系统接口 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-api-system</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 租管接口 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-api-tenant</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 文件接口 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-api-file</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- 定时任务接口 -->
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-api-job</artifactId>
                <version>${scratch.version}</version>
            </dependency>

            <!-- huTool 工具类库 -->
            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-bom</artifactId>
                <version>${hutool.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!--短信sms4j-->
            <dependency>
                <groupId>org.dromara.sms4j</groupId>
                <artifactId>sms4j-spring-boot-starter</artifactId>
                <version>${sms4j.version}</version>
            </dependency>

            <dependency>
                <groupId>io.github.openfeign</groupId>
                <artifactId>feign-okhttp</artifactId>
                <version>13.0</version>
            </dependency>
            <dependency>
                <groupId>com.scratch</groupId>
                <artifactId>scratch-common-service</artifactId>
                <version>${scratch.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <modules>
        <module>scratch-auth</module>
        <module>scratch-gateway</module>
        <module>scratch-visual</module>
        <module>scratch-modules</module>
        <module>scratch-api</module>
        <module>scratch-common</module>
        <module>scratch-test</module>
        <module>scratch-service</module>
    </modules>
    <packaging>pom</packaging>

    <dependencies>

        <!-- bootstrap 启动器 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>org.mapstruct</groupId>
                            <artifactId>mapstruct-processor</artifactId>
                            <version>${mapstruct.version}</version>
                        </path>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </path>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok-mapstruct-binding</artifactId>
                            <version>${lombok-mapstruct-binding.version}</version>
                        </path>
                    </annotationProcessorPaths>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring-boot.version}</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <repositories>
        <repository>
            <id>public</id>
            <name>aliyun nexus</name>
            <url>https://maven.aliyun.com/repository/public</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>public</id>
            <name>aliyun nexus</name>
            <url>https://maven.aliyun.com/repository/public</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <profiles>
        <profile>
            <id>dev</id>
            <activation>
                <!--默认激活配置,maven打包默认选用的配置-->
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <spring.profiles.active>dev</spring.profiles.active>
                <nacos.addr>192.168.31.62:8848</nacos.addr>
                <nacos.username>nacos</nacos.username>
                <nacos.password>nacos</nacos.password>
                <nacos.namespace>a148d646-72f4-4b7b-a25c-92cfb24e5f15</nacos.namespace>
            </properties>
        </profile>
        <profile>
            <id>prod</id>
            <properties>
                <spring.profiles.active>prod</spring.profiles.active>
                <nacos.addr>192.168.31.62:8848</nacos.addr>
                <nacos.username>nacos</nacos.username>
                <nacos.password>nacos</nacos.password>
                <nacos.namespace></nacos.namespace>
            </properties>
        </profile>
    </profiles>
</project>