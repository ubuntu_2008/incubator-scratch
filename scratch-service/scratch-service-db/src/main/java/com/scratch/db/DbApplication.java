package com.scratch.db;


import com.scratch.common.security.aspect.AuthAspect;
import com.scratch.common.security.config.WebMvcConfig;
import com.scratch.common.security.handler.ClientRegisteredRepositoryHandler;
import com.scratch.common.security.handler.GlobalExceptionHandler;
import com.scratch.common.security.handler.RedisOAuth2AuthorizationConsentHandler;
import com.scratch.common.security.handler.RedisOAuth2AuthorizationHandler;
import com.scratch.common.security.service.TokenUserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by bpf on 2019/4/19.
 */
@EnableScheduling
@SpringBootApplication(exclude = {WebMvcConfig.class, TokenUserService.class, AuthAspect.class, GlobalExceptionHandler.class,
        ClientRegisteredRepositoryHandler.class, RedisOAuth2AuthorizationHandler.class, RedisOAuth2AuthorizationConsentHandler.class})
public class DbApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DbApplication.class);
        application.run();
    }
}
