package com.scratch.terminal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 系统模块
 *
 * @author scratch
 */
@EnableFeignClients
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class},
scanBasePackages = {"com.scratch.common.service","com.scratch.terminal",})
public class ScratchTerminalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScratchTerminalApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  即开票查询模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}