package com.scratch.terminal.service;

import com.scratch.common.core.web.result.R;
import com.scratch.core.api.service.domain.*;
import org.springframework.web.bind.annotation.RequestBody;

public interface ITerminalService {
    /**
     * 装票时获取的游戏列表
     *
     * @param reqPlayList
     * @return
     */
    R playList(ReqPlayList reqPlayList);

    /**
     * 装票
     *
     * @param reqTicketLoad
     * @return
     */
    R loadTickets(ReqTicketLoad reqTicketLoad);


    /**
     * 首页列表展示
     */
    R showList(ReqShowList reqShowList);


    /**
     * 选票下单
     *
     * @param reqOrder
     * @return
     */
    R order(ReqOrder reqOrder);


    /**
     * 支付通知
     *
     * @param encryOrderNo
     * @return
     */
    R payNotify(String encryOrderNo);


    /**
     * 设备查询支付结果
     */
    R payResult(ReqPayResult reqPayResult);

    /**
     * 出票
     */
    R issueTickets(ReqIssueTickets reqIssueTickets);


    /**
     * 获取兑奖二维码
     *
     * @param reqCashQrCode
     * @return
     */
    R cashQrCode(ReqCashQrCode reqCashQrCode);

    /**
     * 兑奖授权 获取唯一ID当userId用
     */
    R cashAuth(String devNo, String userId);

    /**
     * 获取授权结果
     */
    R cashAuthResult(String devNo);


    R cash(ReqCash reqCash);


}
