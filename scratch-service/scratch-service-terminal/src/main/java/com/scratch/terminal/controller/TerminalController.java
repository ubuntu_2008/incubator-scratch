package com.scratch.terminal.controller;


import com.scratch.common.core.web.result.R;
import com.scratch.core.api.service.domain.*;
import com.scratch.terminal.service.ITerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/api/terminal")
public class TerminalController {
    @Autowired
    private ITerminalService terminalService;

    @PostMapping("/getPlayList")
    public R getPlayList(@Validated @RequestBody ReqPlayList reqPlayList) {
        return terminalService.playList(reqPlayList);
    }

    @PostMapping("/loadTickets")
    public R loadTickets(@Validated @RequestBody ReqTicketLoad reqTicketLoad) {
        return terminalService.loadTickets(reqTicketLoad);
    }

    @PostMapping("/showList")
    public R showList(@Validated @RequestBody ReqShowList reqShowList) {
        return terminalService.showList(reqShowList);
    }


    @PostMapping("/order")
    public R order(@Validated @RequestBody ReqOrder reqOrder) {
        return terminalService.order(reqOrder);
    }


    @GetMapping("/payNotify/{orderNo}")
    public String payNotify(@PathVariable("orderNo") String orderNo) {
        try {
            return terminalService.payNotify(orderNo).isOk() ? "1" : "0";
        } catch (Exception e) {
            return "0";
        }
    }

    @PostMapping("/payResult")
    public R payResult(@RequestBody ReqPayResult reqPayResult) {
        return terminalService.payResult(reqPayResult);
    }


    @PostMapping("/issueTickets")
    public R issueTickets(@RequestBody ReqIssueTickets reqIssueTickets) {
        return terminalService.issueTickets(reqIssueTickets);
    }


    @PostMapping("/cashQrCode")
    public R cashQrCode(@RequestBody ReqCashQrCode reqCashQrCode) {
        return terminalService.cashQrCode(reqCashQrCode);
    }


    @GetMapping("/cashAuth/{devNo}/{userId}")
    public String cashAuth(@PathVariable("devNo") String devNo, @PathVariable("userId") String userId) {
        try {
            return terminalService.cashAuth(devNo, userId).isOk() ? "1" : "0";
        } catch (Exception e) {
            return "0";
        }
    }

    @PostMapping("/cashAuthResult")
    public R cashAuthResult(@RequestBody ReqCashAuthResult reqCashAuthResult) {
        return terminalService.cashAuthResult(reqCashAuthResult.getDevNo());
    }

    @PostMapping("/cash")
    public R cash(@RequestBody ReqCash reqCash) {
        return terminalService.cash(reqCash);
    }
}

