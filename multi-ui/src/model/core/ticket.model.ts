import { BasicFetchResult, BasicPageParams, BaseEntity } from '@/model/basic';
import { LevelLM} from "@/model/core/level.model";


/** ticket info model */
export interface TicketIM extends BaseEntity {
  /** 机构id */
  id: string;
  /** 方案编号 */
  playNo: string;
  /** 玩法名称 */
  playName: string;
  /** 票面价值(元) */
  value: string;
  /** 票类型 */
  type: string;
  /** 横/竖票面 */
  xy: string;
  /** 票面尺寸宽(毫米) */
  sizeX: number;
  /** 票面尺寸高(毫米) */
  sizeY: number;
  /** 售兑区域类型 */
  areaType: string;
  /** 售兑区域 */
  areaId: string;
  /** 上市年份 */
  listYear: any;
  /** 退市年份 */
  delistYear: any;
  /** 样票图片 */
  img: string;
  /** 票类说明 */
  desc: string;
  /** 玩法规则 */
  playDesc: string;
  /** 奖组规模(万张) */
  awardGroupSize: number;
  /** 奖级箱数(箱) */
  awardLevelCaseNum: number;
  /** 每箱重量(公斤) */
  caseWeight: number;
  /** 每箱盒数(盒) */
  caseBoxNum: number;
  /** 每盒本数(本) */
  boxBenNum: number;
  /** 每箱本数(本) */
  caseBenNum: number;
  /** 每本张数(张) */
  benZhangNum: number;
  /** 最高奖金(元) */
  maxMoney: string;
  /** 中奖率(%) */
  winRate: number;
  /** 返奖率(%) */
  rewardRate: number;
  /** 中奖机会 */
  winChance: string;
  /** 状态 */
  status: number;
  /** 奖级id */
  levels: LevelLM;
}

/** ticket list model */
export type TicketLM = TicketIM[];

/** ticket param model */
export interface TicketPM extends BaseEntity {
  /** 机构id */
  id?: string;
  /** 方案编号 */
  playNo?: string;
  /** 玩法名称 */
  playName?: string;
  /** 票面价值(元) */
  value?: string;
  /** 票类型 */
  type?: string;
  /** 横/竖票面 */
  xy?: string;
  /** 票面尺寸宽(毫米) */
  sizeX?: number;
  /** 票面尺寸高(毫米) */
  sizeY?: number;
  /** 售兑区域类型 */
  areaType?: string;
  /** 售兑区域 */
  areaId?: string;
  /** 上市年份 */
  listYear?: any;
  /** 退市年份 */
  delistYear?: any;
  /** 样票图片 */
  img?: string;
  /** 票类说明 */
  desc?: string;
  /** 玩法规则 */
  playDesc?: string;
  /** 奖组规模(万张) */
  awardGroupSize?: number;
  /** 奖级箱数(箱) */
  awardLevelCaseNum?: number;
  /** 每箱重量(公斤) */
  caseWeight?: number;
  /** 每箱盒数(盒) */
  caseBoxNum?: number;
  /** 每盒本数(本) */
  boxBenNum?: number;
  /** 每箱本数(本) */
  caseBenNum?: number;
  /** 每本张数(张) */
  benZhangNum?: number;
  /** 最高奖金(元) */
  maxMoney?: string;
  /** 中奖率(%) */
  winRate?: number;
  /** 返奖率(%) */
  rewardRate?: number;
  /** 中奖机会 */
  winChance?: string;
  /** 状态 */
  status?: number;
  /** 奖级id */
  levels: LevelLM;
}

/** ticket page param model */
export type TicketPPM = BasicPageParams & TicketPM;

/** ticket list result model */
export type TicketLRM = BasicFetchResult<TicketIM>;
