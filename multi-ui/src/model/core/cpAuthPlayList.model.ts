import {
  BasicFetchResult,
  BasicPageParams, BaseEntity
} from '@/model/basic';

/** play info model */
export interface CpAuthPlayListIM extends BaseEntity {
  baseTicketId: string;
  img: string;
  playName: string;
  playNo: string;
  authSell: string;
  authCash: string;
}

/** play list model */
export type CpAuthPlayListLM = CpAuthPlayListIM[];

/** play param model */
export interface CpAuthPlayListPM extends BaseEntity {
  baseTicketId?: string;
  img?: string;
  playName?: string;
  playNo?: string;
  authSell?: string;
  authCash?: string;
}

/** play page param model */
export type CpAuthPlayListPPM = BasicPageParams & CpAuthPlayListPM;

/** play list result model */
export type CpAuthPlayListLRM = BasicFetchResult<CpAuthPlayListIM>;
