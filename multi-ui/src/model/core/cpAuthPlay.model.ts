import { BasicFetchResult,
  BasicPageParams, BaseEntity } from '@/model/basic';

/** play info model */
export interface CpAuthPlayIM extends BaseEntity {
  /** 玩法授权Id */
  id: string;
  /** 机构信息表id */
  baseCpId: string;
  /** 票信息表id */
  baseTicketId: string;
  /** 销售授权 */
  authSell: string;
  /** 兑奖授权 */
  authCash: string;
}

/** play list model */
export type CpAuthPlayLM = CpAuthPlayIM[];

/** play param model */
export interface CpAuthPlayPM extends BaseEntity {
  /** 玩法授权Id */
  id?: string;
  /** 机构信息表id */
  baseCpId?: string;
  /** 票信息表id */
  baseTicketId?: string;
  /** 销售授权 */
  authSell?: string;
  /** 兑奖授权 */
  authCash?: string;
}

/** play page param model */
export type CpAuthPlayPPM = BasicPageParams & CpAuthPlayPM;

/** play list result model */
export type CpAuthPlayLRM = BasicFetchResult<CpAuthPlayIM>;
