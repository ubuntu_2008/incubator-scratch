import { BasicFetchResult, BasicPageParams, BaseEntity } from '@/model/basic';

/** site info model */
export interface SiteIM extends BaseEntity {
  /** 网点id */
  id: string;
  /** 网点编号 */
  siteNo: string;
  /** 区域 */
  areaId: string;
  /** 所属渠道 */
  cpId: string;
  /** 网点名称 */
  siteName: string;
  /** 身份证号 */
  cardNo: string;
  /** 联系人 */
  contact: string;
  /** 联系方式 */
  contactUs: string;
  /** 联系地址 */
  contactAddress: string;
  /** 已授权业务 */
  authBus: string;
  /** 网点状态 */
  status: number;
  /** 网点地址 */
  siteAddress: string;
  /** 经度 */
  longitude: string;
  /** 纬度 */
  latitude: string;
}

/** site list model */
export type SiteLM = SiteIM[];

/** site param model */
export interface SitePM extends BaseEntity {
  /** 网点id */
  id?: string;
  /** 网点编号 */
  siteNo?: string;
  /** 区域 */
  areaId?: string;
  /** 所属渠道 */
  cpId: string;
  /** 网点名称 */
  siteName?: string;
  /** 身份证号 */
  cardNo?: string;
  /** 联系人 */
  contact?: string;
  /** 联系方式 */
  contactUs?: string;
  /** 联系地址 */
  contactAddress?: string;
  /** 已授权业务 */
  authBus?: string;
  /** 网点状态 */
  status?: number;
  /** 网点地址 */
  siteAddress?: string;
  /** 经度 */
  longitude?: string;
  /** 纬度 */
  latitude?: string;
}

/** site page param model */
export type SitePPM = BasicPageParams & SitePM;

/** site list result model */
export type SiteLRM = BasicFetchResult<SiteIM>;
