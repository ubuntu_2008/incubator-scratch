import { BasicFetchResult, BasicPageParams, BaseEntity } from '@/model/basic';

/** cp info model */
export interface CpIM extends BaseEntity {
  /** 渠道id */
  id: string;
  /** 渠道编号 */
  cpNo: string;
  /** 区域 */
  areaId: string;
  /** 渠道名称 */
  cpName: string;
  /** 联系人 */
  contact: string;
  /** 联系方式 */
  contactUs: string;
  /** 联系地址 */
  contactAddress: string;
  /** 已授权业务 */
  authBus: string;
  /** 状态 */
  status: number;
}

/** cp list model */
export type CpLM = CpIM[];

/** cp param model */
export interface CpPM extends BaseEntity {
  /** 渠道id */
  id?: string;
  /** 渠道编号 */
  cpNo?: string;
  /** 区域 */
  areaId?: string;
  /** 渠道名称 */
  cpName?: string;
  /** 联系人 */
  contact?: string;
  /** 联系方式 */
  contactUs?: string;
  /** 联系地址 */
  contactAddress?: string;
  /** 已授权业务 */
  authBus?: string;
  /** 状态 */
  status?: number;
}

/** cp page param model */
export type CpPPM = BasicPageParams & CpPM;

/** cp list result model */
export type CpLRM = BasicFetchResult<CpIM>;
