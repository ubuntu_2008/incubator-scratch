import {
  BasicFetchResult,
  BasicPageParams, BaseEntity
} from '@/model/basic';

/** play info model */
export interface OrgAuthPlayListIM extends BaseEntity {
  baseTicketId: string;
  img: string;
  playName: string;
  playNo: string;
  authSell: string;
  authCash: string;
}

/** play list model */
export type OrgAuthPlayListLM = OrgAuthPlayListIM[];

/** play param model */
export interface OrgAuthPlayListPM extends BaseEntity {
  baseTicketId?: string;
  img?: string;
  playName?: string;
  playNo?: string;
  authSell?: string;
  authCash?: string;
}

/** play page param model */
export type OrgAuthPlayListPPM = BasicPageParams & OrgAuthPlayListPM;

/** play list result model */
export type OrgAuthPlayListLRM = BasicFetchResult<OrgAuthPlayListIM>;
