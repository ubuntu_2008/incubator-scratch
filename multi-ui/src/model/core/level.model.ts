import { BasicFetchResult, BasicPageParams,
  BaseEntity } from '@/model/basic';

/** level info model */
export interface LevelIM extends BaseEntity {
  /** 机构id */
  id: string;
  /** 票信息表id */
  baseTicketId: string;
  /** 奖级名称 */
  name: string;
  /** 中奖金额(元) */
  winMoney: string;
  /** 中奖金额(元) */
  winNum: string;
}

/** level list model */
export type LevelLM = LevelIM[];

/** level param model */
export interface LevelPM extends BaseEntity {
  /** 机构id */
  id?: string;
  /** 票信息表id */
  baseTicketId?: string;
  /** 奖级名称 */
  name?: string;
  /** 中奖金额(元) */
  winMoney?: string;
  /** 中奖金额(元) */
  winNum?: string;
}

/** level page param model */
export type LevelPPM = BasicPageParams & LevelPM;

/** level list result model */
export type LevelLRM = BasicFetchResult<LevelIM>;
