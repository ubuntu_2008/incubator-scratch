import {
  BasicFetchResult,
  BasicPageParams, BaseEntity
} from '@/model/basic';

/** play info model */
export interface SiteAuthPlayListIM extends BaseEntity {
  baseTicketId: string;
  img: string;
  playName: string;
  playNo: string;
  authSell: string;
  bkgeSell?: string;
  authCash: string;
  bkgeCash?: string;
}

/** play list model */
export type SiteAuthPlayListLM = SiteAuthPlayListIM[];

/** play param model */
export interface SiteAuthPlayListPM extends BaseEntity {
  baseTicketId?: string;
  img?: string;
  playName?: string;
  playNo?: string;
  authSell?: string;
  bkgeSell?: string;
  authCash?: string;
  bkgeCash?: string;
}

/** play page param model */
export type SiteAuthPlayListPPM = BasicPageParams & SiteAuthPlayListPM;

/** play list result model */
export type SiteAuthPlayListLRM = BasicFetchResult<SiteAuthPlayListIM>;
