import {BasicFetchResult, BasicPageParams, BaseEntity} from '@/model/basic';

/** terminal info model */
export interface TerminalIM extends BaseEntity {
  /** 终端id */
  id: string;
  /** 品牌名称 */
  brandId: string;
  /** 型号名称 */
  modelId: string;
  /** mac地址 */
  mac: string;
  /** 终端ID */
  devNo: string;

  /** 站点ID */
  siteId: string;
  /** 备注 */
  remarks: string;
  /** 状态 */
  status: number;
}

/** terminal list model */
export type TerminalLM = TerminalIM[];

/** terminal param model */
export interface TerminalPM extends BaseEntity {
  /** 终端id */
  id?: string;
  /** 品牌名称 */
  brandId?: string;
  /** 型号名称 */
  modelId?: string;
  /** mac地址 */
  mac?: string;
  /** 终端ID */
  devNo?: string;
  siteId: string;
  /** 备注 */
  desc?: string;
  /** 状态 */
  status?: number;
}

/** terminal page param model */
export type TerminalPPM = BasicPageParams & TerminalPM;

/** terminal list result model */
export type TerminalLRM = BasicFetchResult<TerminalIM>;
