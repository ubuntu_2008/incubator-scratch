import { BasicFetchResult, BasicPageParams, BaseEntity } from '@/model/basic';

/** order info model */
export interface OrderIM extends BaseEntity {
  /** 订单id */
  id: string;
  /** 区域ID  */
  areaId: string;
  /** 机构编号 */
  orgNo: string;
  /** 渠道编号 */
  cpNo: string;
  /** 站点编号 */
  siteNo: string;
  /** 终端编号 */
  devNo: string;
  /** 票ID */
  ticketId: string;
  ticketNo: string;
  /** 订单编号  */
  orderNo: string;
  /** 订单金额 （元） */
  orderMoney: number;
  /** 订单时间  */
  orderTime: string;
  /** 订单失效时间 */
  orderTimeOut: string;
  /** 订单描述 */
  orderDesc: string;
  /** 订单类型 */
  orderType: number;

  orderStatus?:number;

  payType?:number;

  payStatus?:number;

  /** 支付时间  */
  payTime: string;
  /** 退款时间 */
  refundTime: string;
  /** 出票时间 */
  buyTime: string;
  /** 票号JSON */
  orderContent: string;
  /** 签名 */
  sign: string;
}

/** order list model */
export type OrderLM = OrderIM[];

/** order param model */
export interface OrderPM extends BaseEntity {
  /** 订单id */
  id?: string;
  /** 区域ID  */
  areaId?: string;
  /** 机构编号 */
  orgNo?: string;
  /** 渠道编号 */
  cpNo?: string;
  /** 站点编号 */
  siteNo?: string;
  /** 终端编号 */
  devNo?: string;
  /** 票ID */
  ticketId?: string;
  ticketNo?: string;
  /** 订单编号  */
  orderNo?: string;
  /** 订单金额 （元） */
  orderMoney?: number;
  /** 订单时间  */
  orderTime?: string;
  /** 订单失效时间 */
  orderTimeOut?: string;
  /** 订单描述 */
  orderDesc?: string;

  /** 订单类型 */
  orderType?: number;

  orderStatus?:number;

  payType?:number;

  payStatus?:number;


  /** 支付时间  */
  payTime?: string;
  /** 退款时间 */
  refundTime?: string;
  /** 出票时间 */
  buyTime?: string;
  /** 票号JSON */
  orderContent?: string;
  /** 签名 */
  sign?: string;
}

/** order page param model */
export type OrderPPM = BasicPageParams & OrderPM;

/** order list result model */
export type OrderLRM = BasicFetchResult<OrderIM>;
