import { BasicFetchResult, BasicPageParams, BaseEntity } from '@/model/basic';

/** siteAuthPlay info model */
export interface SiteAuthPlayIM extends BaseEntity {
  /** 玩法授权Id */
  id: string;
  /** 站点信息表id */
  baseSiteId: string;
  /** 票信息表id */
  baseTicketId: string;
  /** 销售授权 */
  authSell: string;
  /** 销售佣金 */
  bkgeSell: string;
  /** 兑奖授权 */
  authCash: string;
  /** 兑奖佣金 */
  bkgeCash: string;
}

/** siteAuthPlay list model */
export type SiteAuthPlayLM = SiteAuthPlayIM[];

/** siteAuthPlay param model */
export interface SiteAuthPlayPM extends BaseEntity {
  /** 玩法授权Id */
  id?: string;
  /** 站点信息表id */
  baseSiteId?: string;
  /** 票信息表id */
  baseTicketId?: string;
  /** 销售授权 */
  authSell?: string;
  /** 销售佣金 */
  bkgeSell?: string;
  /** 兑奖授权 */
  authCash?: string;
  /** 兑奖佣金 */
  bkgeCash?: string;
}

/** siteAuthPlay page param model */
export type SiteAuthPlayPPM = BasicPageParams & SiteAuthPlayPM;

/** siteAuthPlay list result model */
export type SiteAuthPlayLRM = BasicFetchResult<SiteAuthPlayIM>;
