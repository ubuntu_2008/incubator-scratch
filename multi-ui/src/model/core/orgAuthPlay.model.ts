import { BasicFetchResult,
  BasicPageParams, BaseEntity } from '@/model/basic';

/** play info model */
export interface OrgAuthPlayIM extends BaseEntity {
  /** 玩法授权Id */
  id: string;
  /** 机构信息表id */
  baseOrgId: string;
  /** 票信息表id */
  baseTicketId: string;
  /** 销售授权 */
  authSell: string;
  /** 兑奖授权 */
  authCash: string;
}

/** play list model */
export type OrgAuthPlayLM = OrgAuthPlayIM[];

/** play param model */
export interface OrgAuthPlayPM extends BaseEntity {
  /** 玩法授权Id */
  id?: string;
  /** 机构信息表id */
  baseOrgId?: string;
  /** 票信息表id */
  baseTicketId?: string;
  /** 销售授权 */
  authSell?: string;
  /** 兑奖授权 */
  authCash?: string;
}

/** play page param model */
export type OrgAuthPlayPPM = BasicPageParams & OrgAuthPlayPM;

/** play list result model */
export type OrgAuthPlayLRM = BasicFetchResult<OrgAuthPlayIM>;
