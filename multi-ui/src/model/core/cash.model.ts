import { BasicFetchResult, BasicPageParams, BaseEntity } from '@/model/basic';

/** cash info model */
export interface CashIM extends BaseEntity {
  /**  */
  id: string;
  /** 设备编号 */
  devNo: string;
  /** 用户ID */
  userId: string;
  /** 订单编号 */
  orderNo: string;
  /** 游戏名称 */
  playName: string;
  /** 保安码 */
  dataArea: string;
  /** 票号 */
  ticketCode: string;
  /** 中奖金额 */
  winMoney: string;
  /** 兑奖时间 */
  cashTime: string;
  /** 兑奖状态 */
  status: number;
  /** 签名 */
  sign: string;
}

/** cash list model */
export type CashLM = CashIM[];

/** cash param model */
export interface CashPM extends BaseEntity {
  /**  */
  id?: string;
  /** 设备编号 */
  devNo?: string;
  /** 用户ID */
  userId?: string;
  /** 订单编号 */
  orderNo?: string;
  /** 游戏名称 */
  playName?: string;
  /** 保安码 */
  dataArea?: string;
  /** 票号 */
  ticketCode?: string;
  /** 中奖金额 */
  winMoney?: string;
  /** 兑奖时间 */
  cashTime?: string;
  /** 兑奖状态 */
  status?: number;
  /** 签名 */
  sign?: string;
}

/** cash page param model */
export type CashPPM = BasicPageParams & CashPM;

/** cash list result model */
export type CashLRM = BasicFetchResult<CashIM>;
