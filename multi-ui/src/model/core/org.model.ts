import { BasicFetchResult, BasicPageParams,
  BaseEntity } from '@/model/basic';

/** org info model */
export interface OrgIM extends BaseEntity {
  /** 机构id */
  id: string;
  /** 机构编号 */
  orgNo: string;
  /** 区域 */
  areaId: string;
  /** 机构名称 */
  orgName: string;
  /** 联系人 */
  contact: string;
  /** 联系方式 */
  contactUs: string;
  /** 联系地址 */
  contactAddress: string;
  /** 已授权业务 */
  authBus: string;
  /** 状态 */
  status: number;
}

/** org list model */
export type OrgLM = OrgIM[];

/** org param model */
export interface OrgPM extends BaseEntity {
  /** 机构id */
  id?: string;
  /** 机构编号 */
  orgNo?: string;
  /** 区域 */
  areaId?: string;
  /** 机构名称 */
  orgName?: string;
  /** 联系人 */
  contact?: string;
  /** 联系方式 */
  contactUs?: string;
  /** 联系地址 */
  contactAddress?: string;
  /** 已授权业务 */
  authBus?: string;
  /** 状态 */
  status?: number;
}

/** org page param model */
export type OrgPPM = BasicPageParams & OrgPM;

/** org list result model */
export type OrgLRM = BasicFetchResult<OrgIM>;
