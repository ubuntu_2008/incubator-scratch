import {BasicFetchResult, BasicPageParams, BaseEntity} from '@/model/basic';

/** terminalModel info model */
export interface TerminalModelIM extends BaseEntity {
  /** 终端型号id */
  id: string;
  /** 型号名称 */
  modelName: string;
  /** 品牌名称 */
  brandId: string;
  /** 终端类型 */
  terminalType: string;
  /** 屏幕类型 */
  screenType: string;
  /** 机头/仓位容量(张) */
  hpStorage: number;
  hp: string;
  /** 状态 */
  status: number;
}

/** terminalModel list model */
export type TerminalModelLM = TerminalModelIM[];

/** terminalModel param model */
export interface TerminalModelPM extends BaseEntity {
  /** 终端型号id */
  id?: string;
  /** 型号名称 */
  modelName?: string;
  /** 品牌名称 */
  brandId?: string;
  /** 终端类型 */
  terminalType?: string;
  /** 屏幕类型 */
  screenType?: string;
  /** 机头/仓位容量(张) */
  hpStorage?: number;
  hp?: string;
  /** 状态 */
  status?: number;
}

/** terminalModel page param model */
export type TerminalModelPPM = BasicPageParams & TerminalModelPM;

/** terminalModel list result model */
export type TerminalModelLRM = BasicFetchResult<TerminalModelIM>;
