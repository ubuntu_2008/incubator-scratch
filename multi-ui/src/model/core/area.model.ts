import { BasicFetchResult, BasicPageParams, TreeEntity } from '@/model/basic';

/** area info model */
export interface AreaIM extends TreeEntity<AreaIM> {
  /** 区域id */
  id: string;
  /** 区域名称 */
  name: string;
  /** 区域状态 */
  status: string;
}

/** area list model */
export type AreaLM = AreaIM[];

/** area param model */
export interface AreaPM extends TreeEntity<AreaPM> {
  /** 区域id */
  id?: string;
  /** 区域名称 */
  name?: string;
  /** 区域状态 */
  status?: string;
}

/** area page param model */
export type AreaPPM = BasicPageParams & AreaPM;

/** area list result model */
export type AreaLRM = BasicFetchResult<AreaIM>;
