import { AreaIM, AreaPM, AreaLM } from '@/model/core/area.model';
import { defHttp } from '@/utils/http/axios';

enum Api {
  LIST_AREA = '/core/admin/area/list',
  GET_AREA = '/core/admin/area/',
  ADD_AREA = '/core/admin/area',
  EDIT_AREA = '/core/admin/area',
  EDIT_STATUS_AREA = '/core/admin/area/status',
  DEL_BATCH_AREA = '/core/admin/area/batch/',
}

/** 查询区域列表 */
export const listAreaApi = (params?: AreaPM) =>
  defHttp.get<AreaLM>({ url: Api.LIST_AREA, params });

/** 查询区域详细 */
export const getAreaApi = (id: string) =>
  defHttp.get<AreaIM>({ url: Api.GET_AREA, params: id });

/** 新增区域 */
export const addAreaApi = (params: AreaIM) =>
  defHttp.post({ url: Api.ADD_AREA, params });

/** 修改区域 */
export const editAreaApi = (params: AreaIM) =>
  defHttp.put({ url: Api.EDIT_AREA, params });

/** 修改区域状态 */
export const editStatusAreaApi = (id: string, status: string) =>
  defHttp.put({
    url: Api.EDIT_STATUS_AREA,
    params: { id: id, status: status },
  });

/** 删除区域 */
export const delAreaApi = (ids: string | string[]) =>
  defHttp.delete({ url: Api.DEL_BATCH_AREA, params: ids.toString() });
