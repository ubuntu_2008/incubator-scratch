import {TerminalIM, TerminalPPM, TerminalLRM} from '@/model/core/terminal.model';
import {defHttp} from '@/utils/http/axios';

enum Api {

  ALLOCATION = '/core/admin/terminal/allocation',
  LIST_TERMINAL = '/core/admin/terminal/list',
  GET_TERMINAL = '/core/admin/terminal/',
  GET_TERMINAL_HPTICKET = '/core/admin/terminal/hpTicket/',
  ADD_TERMINAL = '/core/admin/terminal',
  EDIT_TERMINAL = '/core/admin/terminal',
  EDIT_STATUS_TERMINAL = '/core/admin/terminal/status',
  DEL_BATCH_TERMINAL = '/core/admin/terminal/batch/',
}

/**
 * 终端分配
 * @param id
 * @param siteId
 */
export const allocationApi = (id: string, siteId: string) =>
  defHttp.put({
      url: Api.ALLOCATION,
      params: {id: id, siteId: siteId},
    }
  );

/** 查询终端信息列表 */
export const listTerminalApi = (params?: TerminalPPM) =>
  defHttp.get<TerminalLRM>({url: Api.LIST_TERMINAL, params});

/** 查询终端信息详细 */
export const getTerminalApi = (id: string) =>
  defHttp.get<TerminalIM>({url: Api.GET_TERMINAL, params: id});

/**
 * 查询终端对应仓票余量
 */
export const getTerminalHpTicketApi = (devNo: string) =>
  defHttp.get({url: Api.GET_TERMINAL_HPTICKET, params: devNo});


/** 新增终端信息 */
export const addTerminalApi = (params: TerminalIM) =>
  defHttp.post({url: Api.ADD_TERMINAL, params});

/** 修改终端信息 */
export const editTerminalApi = (params: TerminalIM) =>
  defHttp.put({url: Api.EDIT_TERMINAL, params});

/** 修改终端信息状态 */
export const editStatusTerminalApi = (id: string, status: number) =>
  defHttp.put({
    url: Api.EDIT_STATUS_TERMINAL,
    params: {id: id, status: status},
  });

/** 删除终端信息 */
export const delTerminalApi = (ids: string | string[]) =>
  defHttp.delete({url: Api.DEL_BATCH_TERMINAL, params: ids.toString()});
