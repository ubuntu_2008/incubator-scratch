import {SiteIM, SiteLRM, SitePM, SitePPM} from '@/model/core/site.model';
import {defHttp} from '@/utils/http/axios';
import {formatToDateTime} from '@/utils/dateUtil';
import dayjs from 'dayjs';
import {SiteAuthPlayIM} from "@/model/core/siteAuthPlay.model";
import {SiteAuthPlayListLM} from "@/model/core/siteAuthPlayList.model";

enum Api {
  LIST_SITE = '/core/admin/site/list',
  GET_SITE = '/core/admin/site/',
  ADD_SITE = '/core/admin/site',
  EDIT_SITE = '/core/admin/site',
  EDIT_STATUS_SITE = '/core/admin/site/status',
  DEL_BATCH_SITE = '/core/admin/site/batch/',
  EXPORT_SITE = '/core/admin/site/export',

  GET_SITE_NO = '/core/admin/site/getSiteNo',
  AUTH_BUS = '/core/admin/site/authBus',
  AUTH_PLAY = '/core/admin/site/authPlay',
  LIST_AUTH_PLAY = '/core/admin/site/listAuthPlay',
}

/** 获取机构编号 */
export const getSiteNoApi = () =>
  defHttp.get<SiteIM>({url: Api.GET_SITE_NO});

export const authBusApi = (id: string, authBus: string) =>
  defHttp.put({
      url: Api.AUTH_BUS,
      params: {id: id, authBus: authBus},
    }
  );

export const authPlayApi = (params: SiteAuthPlayIM) =>
  defHttp.post({url: Api.AUTH_PLAY, params});

export const listAuthPlayApi = (siteId: string) =>
  defHttp.get<SiteAuthPlayListLM>({
    url: Api.LIST_AUTH_PLAY,
    params: {siteId: siteId},
  });


/** 查询网点列表 */
export const listSiteApi = (params?: SitePPM) =>
  defHttp.get<SiteLRM>({url: Api.LIST_SITE, params});

/** 查询网点详细 */
export const getSiteApi = (id: string) =>
  defHttp.get<SiteIM>({url: Api.GET_SITE, params: id});

/** 新增网点 */
export const addSiteApi = (params: SiteIM) =>
  defHttp.post({url: Api.ADD_SITE, params});

/** 修改网点 */
export const editSiteApi = (params: SiteIM) =>
  defHttp.put({url: Api.EDIT_SITE, params});

/** 修改网点状态 */
export const editStatusSiteApi = (id: string, status: number) =>
  defHttp.put({
    url: Api.EDIT_STATUS_SITE,
    params: {id: id, status: status},
  });

/** 删除网点 */
export const delSiteApi = (ids: string | string[]) =>
  defHttp.delete({url: Api.DEL_BATCH_SITE, params: ids.toString()});

/** 导出网点 */
export const exportSiteApi = async (params?: SitePM) =>
  defHttp.export<any>(
    {url: Api.EXPORT_SITE, params: params},
    '网点_' + formatToDateTime(dayjs()) + '.xlsx',
  );
