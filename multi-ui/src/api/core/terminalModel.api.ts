import { TerminalModelIM, TerminalModelPPM, TerminalModelLRM } from '@/model/core/terminalModel.model';
import { defHttp } from '@/utils/http/axios';

enum Api {
  LIST_TERMINALMODEL = '/core/admin/terminalModel/list',
  GET_TERMINALMODEL = '/core/admin/terminalModel/',
  ADD_TERMINALMODEL = '/core/admin/terminalModel',
  EDIT_TERMINALMODEL = '/core/admin/terminalModel',
  EDIT_STATUS_TERMINALMODEL = '/core/admin/terminalModel/status',
  DEL_BATCH_TERMINALMODEL = '/core/admin/terminalModel/batch/',
  HP = '/core/admin/terminalModel/hp',
}

export const hpApi = (id: string, hp: string) =>
  defHttp.put({
      url: Api.HP,
      params: {id: id, hp: hp},
    }
  );

/** 查询终端型号列表 */
export const listTerminalModelApi = (params?: TerminalModelPPM) =>
  defHttp.get<TerminalModelLRM>({ url: Api.LIST_TERMINALMODEL, params });

/** 查询终端型号详细 */
export const getTerminalModelApi = (id: string) =>
  defHttp.get<TerminalModelIM>({ url: Api.GET_TERMINALMODEL, params: id });

/** 新增终端型号 */
export const addTerminalModelApi = (params: TerminalModelIM) =>
  defHttp.post({ url: Api.ADD_TERMINALMODEL, params });

/** 修改终端型号 */
export const editTerminalModelApi = (params: TerminalModelIM) =>
  defHttp.put({ url: Api.EDIT_TERMINALMODEL, params });

/** 修改终端型号状态 */
export const editStatusTerminalModelApi = (id: string, status: number) =>
  defHttp.put({
    url: Api.EDIT_STATUS_TERMINALMODEL,
    params: { id: id, status: status },
  });

/** 删除终端型号 */
export const delTerminalModelApi = (ids: string | string[]) =>
  defHttp.delete({ url: Api.DEL_BATCH_TERMINALMODEL, params: ids.toString() });
