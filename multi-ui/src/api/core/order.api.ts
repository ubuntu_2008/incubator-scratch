import {OrderIM, OrderPPM, OrderLRM} from '@/model/core/order.model';
import {defHttp} from '@/utils/http/axios';

enum Api {
  LIST_ORDER = '/core/admin/order/list',
  GET_ORDER = '/core/admin/order/',
}

/** 查询订单列表 */
export const listOrderApi = (params?: OrderPPM) =>
  defHttp.get<OrderLRM>({url: Api.LIST_ORDER, params});

/** 查询订单详细 */
export const getOrderApi = (id: string) =>
  defHttp.get<OrderIM>({url: Api.GET_ORDER, params: id});
