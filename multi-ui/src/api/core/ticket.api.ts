import { TicketIM, TicketPPM, TicketLRM } from '@/model/core/ticket.model';
import { defHttp } from '@/utils/http/axios';

enum Api {
  LIST_TICKET = '/core/admin/ticket/list',
  GET_TICKET = '/core/admin/ticket/',
  ADD_TICKET = '/core/admin/ticket',
  EDIT_TICKET = '/core/admin/ticket',
  EDIT_STATUS_TICKET = '/core/admin/ticket/status',
  DEL_BATCH_TICKET = '/core/admin/ticket/batch/',
}

/** 查询票信息管理列表 */
export const listTicketApi = (params?: TicketPPM) =>
  defHttp.get<TicketLRM>({ url: Api.LIST_TICKET, params });

/** 查询票信息管理详细 */
export const getTicketApi = (id: string) =>
  defHttp.get<TicketIM>({ url: Api.GET_TICKET, params: id });

/** 新增票信息管理 */
export const addTicketApi = (params: TicketIM) =>
  defHttp.post({ url: Api.ADD_TICKET, params });

/** 修改票信息管理 */
export const editTicketApi = (params: TicketIM) =>
  defHttp.put({ url: Api.EDIT_TICKET, params });

/** 修改票信息管理状态 */
export const editStatusTicketApi = (id: string, status: number) =>
  defHttp.put({
    url: Api.EDIT_STATUS_TICKET,
    params: { id: id, status: status },
  });

/** 删除票信息管理 */
export const delTicketApi = (ids: string | string[]) =>
  defHttp.delete({ url: Api.DEL_BATCH_TICKET, params: ids.toString() });
