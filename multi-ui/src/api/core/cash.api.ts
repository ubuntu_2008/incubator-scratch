import { CashIM, CashPPM, CashLRM } from '@/model/core/cash.model';
import { defHttp } from '@/utils/http/axios';

enum Api {
  LIST_CASH = '/core/admin/cash/list',
  GET_CASH = '/core/admin/cash/',
}

/** 查询兑奖列表 */
export const listCashApi = (params?: CashPPM) =>
  defHttp.get<CashLRM>({ url: Api.LIST_CASH, params });

/** 查询兑奖详细 */
export const getCashApi = (id: any) =>
  defHttp.get<CashIM>({ url: Api.GET_CASH, params: id });
