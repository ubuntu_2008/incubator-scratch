import {CpIM, CpLRM, CpPPM} from '@/model/core/cp.model';
import {defHttp} from '@/utils/http/axios';
import {CpAuthPlayIM} from "@/model/core/cpAuthPlay.model";
import {CpAuthPlayListLM} from "@/model/core/cpAuthPlayList.model";

enum Api {
  LIST_CP = '/core/admin/cp/list',
  GET_CP = '/core/admin/cp/',
  ADD_CP = '/core/admin/cp',
  EDIT_CP = '/core/admin/cp',
  EDIT_STATUS_CP = '/core/admin/cp/status',
  DEL_BATCH_CP = '/core/admin/cp/batch/',
  GET_CP_NO = '/core/admin/cp/getCpNo',
  AUTH_BUS = '/core/admin/cp/authBus',
  AUTH_PLAY = '/core/admin/cp/authPlay',
  LIST_AUTH_PLAY = '/core/admin/cp/listAuthPlay',
}

/** 获取渠道编号 */
export const getCpNoApi = () =>
  defHttp.get<CpIM>({url: Api.GET_CP_NO});

export const authBusApi = (id: string, authBus: string) =>
  defHttp.put({
      url: Api.AUTH_BUS,
      params: {id: id, authBus: authBus},
    }
  );

export const authPlayApi = (params: CpAuthPlayIM) =>
  defHttp.post({url: Api.AUTH_PLAY, params});


export const listAuthPlayApi = (cpId: string) =>
  defHttp.get<CpAuthPlayListLM>({
    url: Api.LIST_AUTH_PLAY,
    params: {cpId: cpId},
  });


/** 查询信息管理列表 */
export const listCpApi = (params?: CpPPM) =>
  defHttp.get<CpLRM>({url: Api.LIST_CP, params});

/** 查询信息管理详细 */
export const getCpApi = (id: string) =>
  defHttp.get<CpIM>({url: Api.GET_CP, params: id});

/** 新增信息管理 */
export const addCpApi = (params: CpIM) =>
  defHttp.post({url: Api.ADD_CP, params});

/** 修改信息管理 */
export const editCpApi = (params: CpIM) =>
  defHttp.put({url: Api.EDIT_CP, params});

/** 修改信息管理状态 */
export const editStatusCpApi = (id: string, status: number) =>
  defHttp.put({
    url: Api.EDIT_STATUS_CP,
    params: {id: id, status: status},
  });

/** 删除信息管理 */
export const delCpApi = (ids: string | string[]) =>
  defHttp.delete({url: Api.DEL_BATCH_CP, params: ids.toString()});
