import {OrgIM, OrgLRM, OrgPPM} from '@/model/core/org.model';
import {defHttp} from '@/utils/http/axios';
import {OrgAuthPlayIM} from "@/model/core/orgAuthPlay.model";
import {OrgAuthPlayListLM} from "@/model/core/orgAuthPlayList.model";

enum Api {
  LIST_ORG = '/core/admin/org/list',
  GET_ORG = '/core/admin/org/',
  ADD_ORG = '/core/admin/org',
  EDIT_ORG = '/core/admin/org',
  EDIT_STATUS_ORG = '/core/admin/org/status',
  DEL_BATCH_ORG = '/core/admin/org/batch/',
  GET_ORG_NO = '/core/admin/org/getOrgNo',
  AUTH_BUS = '/core/admin/org/authBus',
  AUTH_PLAY = '/core/admin/org/authPlay',
  LIST_AUTH_PLAY = '/core/admin/org/listAuthPlay',
}

/** 获取机构编号 */
export const getOrgNoApi = () =>
  defHttp.get<OrgIM>({url: Api.GET_ORG_NO});

export const authBusApi = (id: string, authBus: string) =>
  defHttp.put({
      url: Api.AUTH_BUS,
      params: {id: id, authBus: authBus},
    }
  );

export const authPlayApi = (params: OrgAuthPlayIM) =>
  defHttp.post({url: Api.AUTH_PLAY, params});


export const listAuthPlayApi = (orgId: string) =>
  defHttp.get<OrgAuthPlayListLM>({
      url: Api.LIST_AUTH_PLAY,
      params: {orgId: orgId},
    });




/** 查询信息管理列表 */
export const listOrgApi = (params?: OrgPPM) =>
  defHttp.get<OrgLRM>({url: Api.LIST_ORG, params});

/** 查询信息管理详细 */
export const getOrgApi = (id: string) =>
  defHttp.get<OrgIM>({url: Api.GET_ORG, params: id});

/** 新增信息管理 */
export const addOrgApi = (params: OrgIM) =>
  defHttp.post({url: Api.ADD_ORG, params});


/** 修改信息管理 */
export const editOrgApi = (params: OrgIM) =>
  defHttp.put({url: Api.EDIT_ORG, params});

/** 修改信息管理状态 */
export const editStatusOrgApi = (id: string, status: number) =>
  defHttp.put({
    url: Api.EDIT_STATUS_ORG,
    params: {id: id, status: status},
  });

/** 删除信息管理 */
export const delOrgApi = (ids: string | string[]) =>
  defHttp.delete({url: Api.DEL_BATCH_ORG, params: ids.toString()});
