/** 信息管理权限标识 */
export enum OrgAuth {
  // 查看信息管理列表
  LIST = 'core:org:list',
  // 查询信息管理详情
  SINGLE = 'core:org:single',
  // 新增信息管理
  ADD = 'core:org:add',
  // 修改信息管理
  EDIT = 'core:org:edit',
  // 修改信息管理状态
  EDIT_STATUS = 'core:org:es',
  // 删除信息管理
  DELETE = 'core:org:delete',
}
