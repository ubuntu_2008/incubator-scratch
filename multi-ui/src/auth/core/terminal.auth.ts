/** 终端信息权限标识 */
export enum TerminalAuth {
  // 查看终端信息列表
  LIST = 'core:terminal:list',
  // 查询终端信息详情
  SINGLE = 'core:terminal:single',
  // 新增终端信息
  ADD = 'core:terminal:add',
  // 修改终端信息
  EDIT = 'core:terminal:edit',
  // 修改终端信息状态
  EDIT_STATUS = 'core:terminal:es',
  // 删除终端信息
  DELETE = 'core:terminal:delete',
}
