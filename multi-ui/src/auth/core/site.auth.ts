/** 网点权限标识 */
export enum SiteAuth {
  // 查看网点列表
  LIST = 'core:site:list',
  // 查询网点详情
  SINGLE = 'core:site:single',
  // 新增网点
  ADD = 'core:site:add',
  // 修改网点
  EDIT = 'core:site:edit',
  // 修改网点状态
  EDIT_STATUS = 'core:site:es',
  // 删除网点
  DELETE = 'core:site:delete',
  // 网点导入
  IMPORT = 'core:site:import',
  // 网点导出
  EXPORT = 'core:site:export',
}
