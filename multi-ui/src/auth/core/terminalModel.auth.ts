/** 终端型号权限标识 */
export enum TerminalModelAuth {
  // 查看终端型号列表
  LIST = 'core:terminalModel:list',
  // 查询终端型号详情
  SINGLE = 'core:terminalModel:single',
  // 新增终端型号
  ADD = 'core:terminalModel:add',
  // 修改终端型号
  EDIT = 'core:terminalModel:edit',
  // 修改终端型号状态
  EDIT_STATUS = 'core:terminalModel:es',
  // 删除终端型号
  DELETE = 'core:terminalModel:delete',
}
