/** 兑奖权限标识 */
export enum CashAuth {
  // 查看兑奖列表
  LIST = 'core:cash:list',
  // 查询兑奖详情
  SINGLE = 'core:cash:single',
}
