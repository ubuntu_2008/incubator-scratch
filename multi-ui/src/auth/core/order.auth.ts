/** 订单权限标识 */
export enum OrderAuth {
  // 查看订单列表
  LIST = 'core:order:list',
  // 查询订单详情
  SINGLE = 'core:order:single',
}
