/** 区域权限标识 */
export enum AreaAuth {
  // 查看区域列表
  LIST = 'core:area:list',
  // 查询区域详情
  SINGLE = 'core:area:single',
  // 新增区域
  ADD = 'core:area:add',
  // 修改区域
  EDIT = 'core:area:edit',
  // 修改区域状态
  EDIT_STATUS = 'core:area:es',
  // 删除区域
  DELETE = 'core:area:delete',
}
