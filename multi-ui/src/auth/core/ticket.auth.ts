/** 票信息管理权限标识 */
export enum TicketAuth {
  // 查看票信息管理列表
  LIST = 'core:ticket:list',
  // 查询票信息管理详情
  SINGLE = 'core:ticket:single',
  // 新增票信息管理
  ADD = 'core:ticket:add',
  // 修改票信息管理
  EDIT = 'core:ticket:edit',
  // 修改票信息管理状态
  EDIT_STATUS = 'core:ticket:es',
  // 删除票信息管理
  DELETE = 'core:ticket:delete',
}
