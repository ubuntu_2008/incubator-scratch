/** 信息管理权限标识 */
export enum CpAuth {
  // 查看信息管理列表
  LIST = 'core:cp:list',
  // 查询信息管理详情
  SINGLE = 'core:cp:single',
  // 新增信息管理
  ADD = 'core:cp:add',
  // 修改信息管理
  EDIT = 'core:cp:edit',
  // 修改信息管理状态
  EDIT_STATUS = 'core:cp:es',
  // 删除信息管理
  DELETE = 'core:cp:delete',
}
