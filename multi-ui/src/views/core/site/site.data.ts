import { FormSchema } from '@/components/Form';
import { BasicColumn } from '@/components/Table';
import { DescItem } from '@/components/Description';
import { dicDictList } from '@/api/sys/dict.api';
import { dictConversion } from '@/utils/scratch';
import { SiteIM } from '@/model/core/site.model';
import { isNotEmpty } from '@/utils/is';
import { listDeptApi } from '@/api/system/organize/dept.api';
import { listCpApi } from '@/api/core/cp.api';

/** 字典查询 */
export const dictMap = await dicDictList(['base_site_auth_bus', 'sys_normal_disable']);

/** 字典表 */
export const dict: any = {
  DicBaseSiteAuthBusOptions: dictMap['base_site_auth_bus'],
  DicNormalDisableOptions: dictMap['sys_normal_disable'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '网点编号',
    dataIndex: 'siteNo',
    width: 220,
  },
  {
    title: '区域',
    dataIndex: 'areaId',
    width: 220,
    customRender: async ({ record }) => {
      // let data = await getDeptApi(record.areaId) as DeptIM;
      // record.areaId = data.name;
      // return record;
      record.areaId = record.dept.name;
      return record;
    },
  },
  {
    title: '所属渠道',
    dataIndex: 'cpId',
    width: 220,
    customRender: async ({ record }) => {
      record.cpId = record.cp.cpName;
      return record;
    },
  },
  {
    title: '网点名称',
    dataIndex: 'siteName',
    width: 220,
  },
  {
    title: '身份证号',
    dataIndex: 'cardNo',
    width: 220,
  },
  {
    title: '联系人',
    dataIndex: 'contact',
    width: 220,
  },
  {
    title: '联系方式',
    dataIndex: 'contactUs',
    width: 220,
  },
  {
    title: '已授权业务',
    dataIndex: 'authBus',
    width: 220,
    customRender: ({ record }) => {
      const data = record as SiteIM;
      let nd;
      if (isNotEmpty(data.authBus)) {
        data.authBus.split(',').map((v, i) => {
          if (i === 0) {
            nd = dictConversion(dict.DicBaseSiteAuthBusOptions, v);
          } else {
            nd = nd + ',' + dictConversion(dict.DicBaseSiteAuthBusOptions, v);
          }
        });
      } else {
        nd = '';
      }
      return nd;
    },
  },
  {
    title: '网点状态',
    dataIndex: 'status',
    width: 220,
    customRender: ({ record }) => {
      const data = record as SiteIM;
      return dictConversion(dict.DicNormalDisableOptions, data.status);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 220,
  },
  {
    title: '更新时间',
    dataIndex: 'updateTime',
    width: 220,
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '网点编号',
    field: 'siteNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'ApiTreeSelect',
    componentProps: {
      showSearch: true,
      api: listDeptApi,
      treeNodeFilterProp: 'name',
      labelField: 'name',
      valueField: 'id',
      getPopupContainer: () => document.body,
    },
    colProps: { span: 6 },
  },
  {
    label: '所属渠道',
    field: 'cpId',
    component: 'ApiSelect',
    colProps: { span: 6 },
    componentProps: {
      showSearch: true,
      api: listCpApi,
      resultField: 'items',
      labelField: 'cpName',
      valueField: 'id',
    },
  },
  {
    label: '网点名称',
    field: 'siteName',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseSiteAuthBusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
  {
    label: '网点状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
];

/** 表单数据 - 业务授权 */
export const resAuthBusFormSchema: FormSchema[] = [
  {
    label: '网点id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: { span: 12 },
  },
  {
    label: '网点编号',
    field: 'siteNo',
    component: 'Input',
    required: true,
    colProps: { span: 24 },
    componentProps: { disabled: true },
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'TreeSelect',
    componentProps: {
      showSearch: true,
      treeNodeFilterProp: 'name',
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
      disabled: true,
    },
    required: true,
    colProps: { span: 24 },
  },
  {
    label: '所属渠道',
    field: 'cpId',
    component: 'Select',
    colProps: { span: 24 },
    componentProps: { disabled: true },
  },
  {
    label: '网点名称',
    field: 'siteName',
    component: 'Input',
    required: true,
    colProps: { span: 24 },
    componentProps: { disabled: true },
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    required: true,
    colProps: { span: 12 },
    ifShow: false,
    componentProps: { disabled: true },
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    required: true,
    colProps: { span: 12 },
    ifShow: false,
    componentProps: { disabled: true },
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    component: 'InputTextArea',
    required: true,
    colProps: { span: 24 },
    ifShow: false,
    componentProps: { disabled: true },
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      disabled: true,
    },
    required: true,
    colProps: { span: 24 },
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'CheckboxGroup',
    componentProps: {
      options: dict.DicBaseSiteAuthBusOptions,
    },
    colProps: { span: 24 },
  },
];

/** 表单数据 */
export const formSchema: FormSchema[] = [
  {
    label: '网点id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: { span: 12 },
  },
  {
    label: '网点编号',
    field: 'siteNo',
    component: 'Input',
    colProps: { span: 12 },
    componentProps: { disabled: true },
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'TreeSelect',
    componentProps: {
      showSearch: true,
      treeNodeFilterProp: 'name',
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
    required: true,
    colProps: { span: 12 },
  },
  {
    label: '所属渠道',
    field: 'cpId',
    component: 'ApiSelect',
    colProps: { span: 12 },
    componentProps: {
      showSearch: true,
      api: listCpApi,
      resultField: 'items',
      labelField: 'cpName',
      valueField: 'id',
    },
  },
  {
    label: '网点名称',
    field: 'siteName',
    component: 'Input',
    required: true,
    colProps: { span: 12 },
  },
  {
    label: '身份证号',
    field: 'cardNo',
    component: 'Input',
    required: true,
    colProps: { span: 8 },
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    required: true,
    colProps: { span: 8 },
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    required: true,
    colProps: { span: 8 },
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    component: 'InputTextArea',
    colProps: { span: 24 },
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseSiteAuthBusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    show: false,
    colProps: { span: 12 },
  },
  {
    label: '网点状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
    },
    required: true,
    colProps: { span: 12 },
  },
  {
    label: '',
    field: 'mapField',
    component: 'Input',
    slot: 'mapSlot',
    colProps: { span: 24 },
  },
  {
    label: '网点地址',
    field: 'siteAddress',
    component: 'InputTextArea',
    // required: true,
    colProps: { span: 24 },
    componentProps: { disabled: true },
    required: true,
  },
  {
    label: '经度',
    field: 'longitude',
    component: 'Input',
    colProps: { span: 12 },
    componentProps: { disabled: true },
    required: true,
  },
  {
    label: '纬度',
    field: 'latitude',
    component: 'Input',
    colProps: { span: 12 },
    componentProps: { disabled: true },
    required: true,
  },
];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '网点编号',
    field: 'siteNo',
    span: 8,
  },
  {
    label: '区域',
    field: 'areaId',
    span: 8,
  },
  {
    label: '所属渠道',
    field: 'cpId',
    span: 8,
  },
  {
    label: '网点名称',
    field: 'siteName',
    span: 8,
  },
  {
    label: '身份证号',
    field: 'cardNo',
    span: 8,
  },
  {
    label: '联系人',
    field: 'contact',
    span: 8,
  },
  {
    label: '联系方式',
    field: 'contactUs',
    span: 8,
  },
  {
    label: '已授权业务',
    field: 'authBus',
    render: (val) => {
      return dictConversion(dict.DicBaseSiteAuthBusOptions, val);
    },
    span: 8,
  },
  {
    label: '网点状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicNormalDisableOptions, val);
    },
    span: 8,
  },
  {
    label: '网点地址',
    field: 'siteAddress',
    span: 8,
  },
  {
    label: '经度',
    field: 'longitude',
    span: 8,
  },
  {
    label: '纬度',
    field: 'latitude',
    span: 8,
  },
];
