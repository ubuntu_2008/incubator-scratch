import { FormSchema } from '@/components/Form';
import { BasicColumn } from '@/components/Table';
import { DescItem } from '@/components/Description';
import { dicDictList } from '@/api/sys/dict.api';
import { dictConversion } from '@/utils/scratch';
import { DicSortEnum } from '@/enums/basic';
import { TicketIM } from '@/model/core/ticket.model';
import { listAreaApi } from '@/api/core/area.api';
import {getDeptApi, listDeptApi} from "@/api/system/organize/dept.api";
import {DeptIM} from "@/model/system/organize";

/** 字典查询 */
export const dictMap = await dicDictList([
    'base_ticket_type',
    'sys_normal_disable',
    'base_ticket_area_type',
    'base_ticket_xy',
  'base_ticket_award_level_name',
]);

/** 字典表 */
export const dict: any = {
  DicBaseTicketTypeOptions: dictMap['base_ticket_type'],
  DicNormalDisableOptions: dictMap['sys_normal_disable'],
  DicBaseTicketAreaTypeOptions: dictMap['base_ticket_area_type'],
  DicBaseTicketXyOptions: dictMap['base_ticket_xy'],
  DicBaseTicketAwardLevelNameOptions: dictMap['base_ticket_award_level_name'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '样票图片',
    dataIndex: 'img',
    slots: { customRender: 'img' },
    width: 120,
  },
  {
    title: '方案编号',
    dataIndex: 'playNo',
    width: 120,
  },
  {
    title: '玩法名称',
    dataIndex: 'playName',
    width: 220,
  },
  {
    title: '票面价值(元)',
    dataIndex: 'value',
    width: 120,
  },
  {
    title: '票类型',
    dataIndex: 'type',
    width: 120,
    customRender: ({ record }) => {
      const data = record as TicketIM;
      return dictConversion(dict.DicBaseTicketTypeOptions, data.type);
    },
  },
  {
    title: '横/竖票面',
    dataIndex: 'xy',
    width: 120,
    customRender: ({ record }) => {
      const data = record as TicketIM;
      return dictConversion(dict.DicBaseTicketXyOptions, data.xy);
    },
  },
  {
    title: '票面尺寸宽(毫米)',
    dataIndex: 'sizeX',
    width: 120,
  },
  {
    title: '票面尺寸高(毫米)',
    dataIndex: 'sizeY',
    width: 120,
  },
  {
    title: '售兑区域类型',
    dataIndex: 'areaType',
    width: 120,
    customRender: ({ record }) => {
      const data = record as TicketIM;
      return dictConversion(dict.DicBaseTicketAreaTypeOptions, data.areaType);
    },
  },
  {
    title: '售兑区域',
    dataIndex: 'areaId',
    width: 120,
    customRender: async ({record}) => {
      const data = await getDeptApi(record.areaId) as DeptIM;
      record.areaId = data.name;
      return record;
    },
  },
  {
    title: '上市年份',
    dataIndex: 'listYear',
    width: 220,
  },
  {
    title: '退市年份',
    dataIndex: 'delistYear',
    width: 220,
  },
  {
    title: '票类说明',
    dataIndex: 'desc',
    width: 220,
  },
  {
    title: '玩法规则',
    dataIndex: 'playDesc',
    width: 220,
  },
  {
    title: '奖组规模(万张)',
    dataIndex: 'awardGroupSize',
    width: 220,
  },
  {
    title: '奖级箱数(箱)',
    dataIndex: 'awardLevelCaseNum',
    width: 220,
  },
  {
    title: '每箱重量(公斤)',
    dataIndex: 'caseWeight',
    width: 220,
  },
  {
    title: '每箱盒数(盒)',
    dataIndex: 'caseBoxNum',
    width: 220,
  },
  {
    title: '每盒本数(本)',
    dataIndex: 'boxBenNum',
    width: 220,
  },
  {
    title: '每箱本数(本)',
    dataIndex: 'caseBenNum',
    width: 220,
  },
  {
    title: '每本张数(张)',
    dataIndex: 'benZhangNum',
    width: 220,
  },
  {
    title: '最高奖金(元)',
    dataIndex: 'maxMoney',
    width: 220,
  },
  {
    title: '中奖率(%)',
    dataIndex: 'winRate',
    width: 220,
  },
  {
    title: '返奖率(%)',
    dataIndex: 'rewardRate',
    width: 220,
  },
  {
    title: '中奖机会',
    dataIndex: 'winChance',
    width: 220,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 220,
    customRender: ({ record }) => {
      const data = record as TicketIM;
      return dictConversion(dict.DicNormalDisableOptions, data.status);
    },
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '方案编号',
    field: 'playNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '玩法名称',
    field: 'playName',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '票类型',
    field: 'type',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
  {
    label: '横/竖票面',
    field: 'xy',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketXyOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
  {
    label: '售兑区域类型',
    field: 'areaType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketAreaTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
  {
    label: '售兑区域',
    field: 'areaId',
    component: 'ApiTreeSelect',
    componentProps: {
      showSearch: true,
      api: listDeptApi,
      treeNodeFilterProp: 'name',
      labelField: 'name',
      valueField: 'id',
      getPopupContainer: () => document.body,
    },
    colProps: { span: 6 },
  },
  {
    label: '上市年份',
    field: 'listYear',
    component: 'YearPicker',
    colProps: { span: 6 },
  },
  {
    label: '退市年份',
    field: 'delistYear',
    component: 'YearPicker',
    colProps: { span: 6 },
  },
  {
    label: '状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    required: true,
    colProps: { span: 6 },
  },
];

/** 表单数据 */
export const formSchema: FormSchema[] = [
  {
    field: 'divider-basic',
    component: 'Divider',
    label: '基本信息',
    colProps: {
      span: 24,
    },
  },
  {
    label: '机构id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: { span: 0 },
    labelWidth: 130
  },
  {
    label: '方案编号',
    field: 'playNo',
    component: 'Input',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '玩法名称',
    field: 'playName',
    component: 'Input',
    colProps: { span: 12 },
    labelWidth: 130
  },
  {
    label: '票面价值(元)',
    field: 'value',
    component: 'Input',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '票类型',
    field: 'type',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '横/竖票面',
    field: 'xy',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketXyOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '票面尺寸宽(毫米)',
    field: 'sizeX',
    component: 'InputNumber',
    defaultValue: DicSortEnum.ZERO,
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '票面尺寸高(毫米)',
    field: 'sizeY',
    component: 'InputNumber',
    defaultValue: DicSortEnum.ZERO,
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '售兑区域类型',
    field: 'areaType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketAreaTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '售兑区域',
    field: 'areaId',
    component: 'ApiTreeSelect',
    componentProps: {
      showSearch: true,
      api: listDeptApi,
      treeNodeFilterProp: 'name',
      labelField: 'name',
      valueField: 'id',
      getPopupContainer: () => document.body,
    },
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '上市年份',
    field: 'listYear',
    component: 'YearPicker',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '退市年份',
    field: 'delistYear',
    component: 'YearPicker',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '样票图片',
    field: 'img',
    component: 'ImageUpload',
    colProps: { span: 24 },
    labelWidth: 130
  },
  {
    label: '票类说明',
    field: 'desc',
    component: 'InputTextArea',
    colProps: { span: 12 },
    labelWidth: 130

  },
  {
    label: '玩法规则',
    field: 'playDesc',
    component: 'InputTextArea',
    colProps: { span: 12 },
    labelWidth: 130
  },
  {
    field: 'divider-basic',
    component: 'Divider',
    label: '奖组规模',
    colProps: {
      span: 24,
    },
  },
  {
    label: '奖组规模(万张)',
    field: 'awardGroupSize',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '奖级箱数(箱)',
    field: 'awardLevelCaseNum',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '每箱重量(公斤)',
    field: 'caseWeight',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '每箱盒数(盒)',
    field: 'caseBoxNum',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '每盒本数(本)',
    field: 'boxBenNum',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '每箱本数(本)',
    field: 'caseBenNum',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '每本张数(张)',
    field: 'benZhangNum',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    field: 'divider-basic',
    component: 'Divider',
    label: '中奖信息',
    colProps: {
      span: 24,
    },
  },
  {
    label: '最高奖金(元)',
    field: 'maxMoney',
    component: 'Input',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '中奖率(%)',
    field: 'winRate',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '返奖率(%)',
    field: 'rewardRate',
    component: 'InputNumber',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '中奖机会',
    field: 'winChance',
    component: 'Input',
    colProps: { span: 6 },
    labelWidth: 130
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
    },
    required: true,
    colProps: { span: 12 },
    labelWidth: 130
  },
  {
    field: 'divider-basic',
    component: 'Divider',
    label: '奖级配置',
    colProps: {
      span: 24,
    },
  }
];



export const appendSchema: FormSchema[] = [
  {
    label: '奖级名称',
    field: 'levels[0].name',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTicketAwardLevelNameOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
    required: true,
  },
  {
    label: '中奖金额(元)',
    field: 'levels[0].winMoney',
    component: 'Input',
    colProps: { span: 6},
    required: true,
  },
  {
    label: '中奖数量',
    field: 'levels[0].winNum',
    component: 'Input',
    colProps: { span: 6},
    required: true,
  },
  {
    label: '',
    field: 'levels[0].sort',
    defaultValue: '0',
    component: 'Input',
    ifShow: false
  },
  {
    field: '0',
    component: 'Input',
    label: '',
    slot: 'add',
    colProps: { span: 6 },
  },
  ];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '方案编号',
    field: 'playNo',
    span: 12,
  },
  {
    label: '玩法名称',
    field: 'playName',
    span: 12,
  },
  {
    label: '票面价值(元)',
    field: 'value',
    span: 12,
  },
  {
    label: '票类型',
    field: 'type',
    render: (val) => {
      return dictConversion(dict.DicBaseTicketTypeOptions, val);
    },
    span: 6,
  },
  {
    label: '横/竖票面',
    field: 'xy',
    render: (val) => {
      return dictConversion(dict.DicBaseTicketXyOptions, val);
    },
    span: 6,
  },
  {
    label: '票面尺寸宽(毫米)',
    field: 'sizeX',
    span: 6,
  },
  {
    label: '票面尺寸高(毫米)',
    field: 'sizeY',
    span: 6,
  },
  {
    label: '售兑区域类型',
    field: 'areaType',
    render: (val) => {
      return dictConversion(dict.DicBaseTicketAreaTypeOptions, val);
    },
    span: 6,
  },
  {
    label: '售兑区域',
    field: 'areaId',
    span: 6,
  },
  {
    label: '上市年份',
    field: 'listYear',
    span: 6,
  },
  {
    label: '退市年份',
    field: 'delistYear',
    span: 6,
  },
  {
    label: '样票图片',
    field: 'img',
    span: 6,
  },
  {
    label: '票类说明',
    field: 'desc',
    span: 6,
  },
  {
    label: '玩法规则',
    field: 'playDesc',
    span: 6,
  },
  {
    label: '奖组规模(万张)',
    field: 'awardGroupSize',
    span: 6,
  },
  {
    label: '奖级箱数(箱)',
    field: 'awardLevelCaseNum',
    span: 6,
  },
  {
    label: '每箱重量(公斤)',
    field: 'caseWeight',
    span: 6,
  },
  {
    label: '每箱盒数(盒)',
    field: 'caseBoxNum',
    span: 6,
  },
  {
    label: '每盒本数(本)',
    field: 'boxBenNum',
    span: 6,
  },
  {
    label: '每箱本数(本)',
    field: 'caseBenNum',
    span: 6,
  },
  {
    label: '每本张数(张)',
    field: 'benZhangNum',
    span: 6,
  },
  {
    label: '最高奖金(元)',
    field: 'maxMoney',
    span: 6,
  },
  {
    label: '中奖率(%)',
    field: 'winRate',
    span: 6,
  },
  {
    label: '返奖率(%)',
    field: 'rewardRate',
    span: 6,
  },
  {
    label: '中奖机会',
    field: 'winChance',
    span: 6,
  },
  {
    label: '状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicNormalDisableOptions, val);
    },
    span: 24,
  },
];

export function getBasicColumns(): BasicColumn[] {
  return [
    {
      title: '奖级名称',
      dataIndex: 'name',
      fixed: 'left',
      width: 200,
    },
    {
      title: '中奖金额(元)',
      dataIndex: 'winMoney',
      width: 150,
    },
    {
      title: '中奖数量',
      dataIndex: 'winNum',
      width: 150,
    },
  ];
}

