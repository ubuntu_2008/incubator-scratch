import {FormSchema} from '@/components/Form';
import {BasicColumn} from '@/components/Table';
import {DescItem} from '@/components/Description';
import {dicDictList} from '@/api/sys/dict.api';
import {dictConversion} from '@/utils/scratch';
import {TerminalIM} from '@/model/core/terminal.model';
import {listTerminalModelApi} from "@/api/core/terminalModel.api";
import {listSiteApi} from "@/api/core/site.api";
import {isNotEmpty} from "@/utils/is";

/** 字典查询 */
export const dictMap = await dicDictList([
  'base_terminal_model_brand',
  'sys_normal_disable',
]);

/** 字典表 */
export const dict: any = {
  DicBaseTerminalModelBrandOptions: dictMap['base_terminal_model_brand'],
  DicNormalDisableOptions: dictMap['sys_normal_disable'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '网点名称',
    dataIndex: 'siteId',
    width: 220,
    customRender: ({record}) => {
     return isNotEmpty(record.site)?record.site.siteName:"";
    },
  },
  {
    title: '品牌名称',
    dataIndex: 'brandId',
    width: 150,
    customRender: ({record}) => {
      const data = record as TerminalIM;
      return dictConversion(dict.DicBaseTerminalModelBrandOptions, data.brandId);
    },
  },
  {
    title: '型号名称',
    dataIndex: 'modelId',
    width: 250,
    customRender: async ({record}) => {
      record.modelId = record.model.modelName;
      return record;
    },
  },
  {
    title: 'mac地址',
    dataIndex: 'mac',
    width: 150,
  },
  {
    title: '终端ID',
    dataIndex: 'devNo',
    width: 150,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    customRender: ({record}) => {
      const data = record as TerminalIM;
      return dictConversion(dict.DicNormalDisableOptions, data.status);
    },
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '品牌名称',
    field: 'brandId',
    component: 'Select',
    componentProps: ({formModel, formActionType}) => {
      return {
        options: dict.DicBaseTerminalModelBrandOptions,
        showSearch: true,
        optionFilterProp: 'label',
        onChange: async (e: any) => {
          formModel.modelId = undefined;
          const {updateSchema} = formActionType;
          let modelsOptions = await listTerminalModelApi({brandId: e});
          updateSchema({
            field: 'modelId',
            componentProps: {
              options: modelsOptions.items,
              optionFilterProp: 'modelName',
              fieldNames: {
                label: 'modelName',
                key: 'id',
                value: 'id',
              },
            },
          });
        },
      };
    },
    colProps: {span: 6},
  },
  {
    label: '型号名称',
    field: 'modelId',
    component: 'Select',
    componentProps: {
      options: [],
    },
    colProps: {span: 6},
  },
  {
    label: 'mac地址',
    field: 'mac',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '终端ID',
    field: 'devNo',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
];

/** 表单数据 */
export const formSchema: FormSchema[] = [
  {
    label: '终端id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 12},
  },
  {
    label: '所属网点',
    field: 'siteId',
    component: 'Input',
    show: false,
    colProps: {span: 12},
  },
  {
    label: '品牌名称',
    field: 'brandId',
    component: 'Select',
    colProps: {span: 12},
    required: true,
    componentProps: ({formModel, formActionType}) => {
      return {
        options: dict.DicBaseTerminalModelBrandOptions,
        showSearch: true,
        optionFilterProp: 'label',
        onChange: async (e: any) => {
          formModel.modelId = undefined;
          const {updateSchema} = formActionType;
          let modelsOptions = await listTerminalModelApi({brandId: e});
          updateSchema({
            field: 'modelId',
            componentProps: {
              options: modelsOptions.items,
              optionFilterProp: 'modelName',
              fieldNames: {
                label: 'modelName',
                key: 'id',
                value: 'id',
              },
            },
          });
        },
      };
    },
  },
  {
    label: '型号名称',
    field: 'modelId',
    component: 'Select',
    colProps: {span: 12},
    componentProps: {
      options: [],
    },
    required: true,
  },
  {
    label: 'mac地址',
    field: 'mac',
    component: 'Input',
    colProps: {span: 12},
    required: true,
  },
  {
    label: '终端ID',
    field: 'devNo',
    component: 'Input',
    colProps: {span: 12},
    required: true,
  },
  {
    label: '备注',
    field: 'remarks',
    component: 'InputTextArea',
    colProps: {span: 24},
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
    },
    required: true,
    colProps: {span: 12},
  },
];


/** 终端分配 */
export const resAllocationFormSchema: FormSchema[] = [
  {
    label: '终端id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 12},
    componentProps: {disabled: true},
  },
  {
    label: '品牌名称',
    field: 'brandId',
    component: 'Select',
    colProps: {span: 12},
    required: true,
    componentProps: ({formModel, formActionType}) => {
      return {
        disabled: true,
        options: dict.DicBaseTerminalModelBrandOptions,
        showSearch: true,
        optionFilterProp: 'label',
        onChange: async (e: any) => {
          formModel.modelId = undefined;
          const {updateSchema} = formActionType;
          let modelsOptions = await listTerminalModelApi({brandId: e});
          updateSchema({
            field: 'modelId',
            componentProps: {
              options: modelsOptions.items,
              optionFilterProp: 'modelName',
              fieldNames: {
                label: 'modelName',
                key: 'id',
                value: 'id',
              },
            },
          });
        },
      };
    },
  },
  {
    label: '型号名称',
    field: 'modelId',
    component: 'Select',
    colProps: {span: 12},
    componentProps: {
      options: [],
      disabled: true
    },
    required: true,

  },
  {
    label: 'mac地址',
    field: 'mac',
    component: 'Input',
    colProps: {span: 12},
    required: true,
    componentProps: {
      disabled: true
    },
  },
  {
    label: '终端ID',
    field: 'devNo',
    component: 'Input',
    colProps: {span: 12},
    required: true,
    componentProps: {
      disabled: true
    },
  },
  {
    label: '备注',
    field: 'remarks',
    component: 'InputTextArea',
    colProps: {span: 24},
    componentProps: {
      disabled: true
    },
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      disabled: true,
      options: dict.DicNormalDisableOptions,
    },
    required: true,
    colProps: {span: 12},
  },
  {
    label: '所属网点',
    field: 'siteId',
    component: 'ApiSelect',
    colProps: {span: 24},
    componentProps: {
      showSearch: true,
      api: listSiteApi,
      resultField: 'items',
      labelField: 'siteName',
      valueField: 'id',
    },
    required: true,
  },
];


/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '品牌名称',
    field: 'brandId',
    render: (val) => {
      return dictConversion(dict.DicBaseTerminalModelBrandOptions, val);
    },
    span: 8,
  },
  {
    label: '型号名称',
    field: 'modelId',
    span: 8,
  },
  {
    label: 'mac地址',
    field: 'mac',
    span: 8,
  },
  {
    label: '终端ID',
    field: 'devNo',
    span: 8,
  },
  {
    label: '备注',
    field: 'remarks',
    span: 8,
  },
  {
    label: '状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicNormalDisableOptions, val);
    },
    span: 8,
  },
  {
    label: '创建时间',
    field: 'createTime',
    span: 8,
  },
];
