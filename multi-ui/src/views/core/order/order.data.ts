import { FormSchema } from '@/components/Form';
import { BasicColumn } from '@/components/Table';
import { DescItem } from '@/components/Description';
import { dicDictList } from '@/api/sys/dict.api';
import { dictConversion } from '@/utils/scratch';
import { OrderIM } from '@/model/core/order.model';
import {listDeptApi} from "@/api/system/organize/dept.api";

/** 字典查询 */
export const dictMap = await dicDictList([
    'base_order_order_way',
    'base_order_status',
]);

/** 字典表 */
export const dict: any = {
  DicBaseOrderOrderWayOptions: dictMap['base_order_order_way'],
  DicBaseOrderStatusOptions: dictMap['base_order_status'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '区域ID ',
    dataIndex: 'areaId',
    width: 220,
    customRender: async ({ record }) => {
      record.areaId = record.dept.name;
      return record;
    },
  },
  {
    title: '机构编号',
    dataIndex: 'orgNo',
    width: 220,
  },
  {
    title: '渠道编号',
    dataIndex: 'cpNo',
    width: 220,
  },
  {
    title: '站点编号',
    dataIndex: 'siteNo',
    width: 220,
  },
  {
    title: '终端编号',
    dataIndex: 'devNo',
    width: 220,
  },
  {
    title: '票名称',
    dataIndex: 'ticketId',
    width: 220,
  },
  {
    title: '票号',
    dataIndex: 'ticketNo',
    width: 220,
  },
  {
    title: '订单编号 ',
    dataIndex: 'orderNo',
    width: 220,
  },
  {
    title: '订单状态',
    dataIndex: 'orderStatus',
    width: 220,
    customRender: ({ record }) => {
      const data = record as OrderIM;
      return dictConversion(dict.DicBaseOrderStatusOptions, data.orderStatus);
    },
  },
  {
    title: '订单时间 ',
    dataIndex: 'orderTime',
    width: 220,
  },
  {
    title: '订单失效时间',
    dataIndex: 'orderTimeOut',
    width: 220,
  },
  {
    title: '订单类型',
    dataIndex: 'orderType',
    width: 220,
    customRender: ({ record }) => {
      const data = record as OrderIM;
      return dictConversion(dict.DicBaseOrderOrderWayOptions, data.orderType);
    },
  },
  {
    title: '支付时间 ',
    dataIndex: 'payTime',
    width: 220,
  },
  {
    title: '退款时间',
    dataIndex: 'refundTime',
    width: 220,
  },
  {
    title: '出票时间',
    dataIndex: 'buyTime',
    width: 220,
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '所属区域',
    field: 'areaId',
    component: 'ApiTreeSelect',
    componentProps: {
      showSearch: true,
      api: listDeptApi,
      treeNodeFilterProp: 'name',
      labelField: 'name',
      valueField: 'id',
      getPopupContainer: () => document.body,
    },
    colProps: { span: 6 },
  },
  {
    label: '机构编号',
    field: 'orgNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '渠道编号',
    field: 'cpNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '站点编号',
    field: 'siteNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '终端编号',
    field: 'devNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '票名称',
    field: 'ticketId',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '票号',
    field: 'ticketNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '订单编号 ',
    field: 'orderNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '订单状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseOrderStatusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
  {
    label: '订单类型',
    field: 'orderType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseOrderOrderWayOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
];

/** 表单数据 */
export const formSchema: FormSchema[] = [
];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '区域ID ',
    field: 'areaId',
    span: 8,
  },
  {
    label: '机构编号',
    field: 'orgNo',
    span: 8,
  },
  {
    label: '渠道编号',
    field: 'cpNo',
    span: 8,
  },
  {
    label: '站点编号',
    field: 'siteNo',
    span: 8,
  },
  {
    label: '终端编号',
    field: 'devNo',
    span: 8,
  },
  {
    label: '票名称',
    field: 'ticketId',
    span: 8,
  },
  {
    label: '票号',
    field: 'ticketNo',
    span: 8,
  },
  {
    label: '订单编号 ',
    field: 'orderNo',
    span: 8,
  },
  {
    label: '订单金额 ',
    field: 'orderMoney',
    span: 8,
  },
  {
    label: '订单状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicBaseOrderStatusOptions, val);
    },
    span: 8,
  },
  {
    label: '订单时间 ',
    field: 'orderTime',
    span: 8,
  },
  {
    label: '订单失效时间',
    field: 'orderTimeOut',
    span: 8,
  },
  {
    label: '订单类型',
    field: 'orderType',
    render: (val) => {
      return dictConversion(dict.DicBaseOrderOrderWayOptions, val);
    },
    span: 8,
  },
  {
    label: '支付时间 ',
    field: 'payTime',
    span: 8,
  },
  {
    label: '退款时间',
    field: 'refundTime',
    span: 8,
  },
  {
    label: '出票时间',
    field: 'buyTime',
    span: 8,
  },
  {
    label: '票号JSON',
    field: 'orderContent',
    span: 8,
  },
  {
    label: '签名',
    field: 'sign',
    span: 8,
  },
];
