import {FormSchema} from '@/components/Form';
import {BasicColumn} from '@/components/Table';
import {DescItem} from '@/components/Description';
import {dicDictList} from '@/api/sys/dict.api';
import {dictConversion} from '@/utils/scratch';
import {OrgIM} from '@/model/core/org.model';
import {isNotEmpty} from "@/utils/is";
import {getDeptApi, listDeptApi} from "@/api/system/organize/dept.api";
import {DeptIM} from "@/model/system/organize";

/** 字典查询 */
export const dictMap = await dicDictList([
  'base_org_auth_bus',
  'sys_normal_disable',
]);

/** 字典表 */
export const dict: any = {
  DicBaseOrgAuthBusOptions: dictMap['base_org_auth_bus'],
  DicNormalDisableOptions: dictMap['sys_normal_disable'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '机构编号',
    dataIndex: 'orgNo',
    width: 220,
  },
  {
    title: '区域',
    dataIndex: 'areaId',
    width: 220,
    customRender: async ({record}) => {
      let data = await getDeptApi(record.areaId) as DeptIM;
      record.areaId = data.name;
      return record;
    },
  },
  {
    title: '机构名称',
    dataIndex: 'orgName',
    width: 220,
  },
  {
    title: '联系人',
    dataIndex: 'contact',
    width: 220,
  },
  {
    title: '联系方式',
    dataIndex: 'contactUs',
    width: 220,
  },
  {
    title: '已授权业务',
    dataIndex: 'authBus',
    width: 220,
    customRender: ({record}) => {
      const data = record as OrgIM;
      let nd;
      if (isNotEmpty(data.authBus)) {
        data.authBus.split(',').map((v, i) => {
          if (i === 0) {
            nd = dictConversion(dict.DicBaseOrgAuthBusOptions, v)
          } else {
            nd = nd + ',' + dictConversion(dict.DicBaseOrgAuthBusOptions, v);
          }
        })
      }else{
        nd = '';
      }
      return nd;
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 220,
    customRender: ({record}) => {
      const data = record as OrgIM;
      return dictConversion(dict.DicNormalDisableOptions, data.status);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 220,
  },
  {
    title: '更新时间',
    dataIndex: 'updateTime',
    width: 220,
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '机构编号',
    field: 'orgNo',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'ApiTreeSelect',
    componentProps: {
      showSearch: true,
      api: listDeptApi,
      treeNodeFilterProp: 'name',
      labelField: 'name',
      valueField: 'id',
      getPopupContainer: () => document.body,
    },
    colProps: {span: 6},
  },
  {
    label: '机构名称',
    field: 'orgName',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseOrgAuthBusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
  {
    label: '状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
];


/** 表单数据 - 业务授权 */
export const resAuthBusFormSchema: FormSchema[] = [
  {
    label: '机构id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 12},
  },
  {
    label: '机构编号',
    field: 'orgNo',
    component: 'Input',
    required: true,
    colProps: {span: 24},
    componentProps: {disabled: true},
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'TreeSelect',
    componentProps: {
      showSearch: true,
      treeNodeFilterProp: 'name',
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
      disabled: true
    },
    required: true,
    colProps: {span: 24},
  },
  {
    label: '机构名称',
    field: 'orgName',
    component: 'Input',
    required: true,
    colProps: {span: 24},
    componentProps: {disabled: true},
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    required: true,
    colProps: {span: 12},
    ifShow: false,
    componentProps: {disabled: true},
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    required: true,
    colProps: {span: 12},
    ifShow: false,
    componentProps: {disabled: true},
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    component: 'InputTextArea',
    required: true,
    colProps: {span: 24},
    ifShow: false,
    componentProps: {disabled: true},
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      disabled: true
    },
    required: true,
    colProps: {span: 24},
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'CheckboxGroup',
    componentProps: {
      options: dict.DicBaseOrgAuthBusOptions,
    },
    colProps: {span: 24},
  },
];


/** 表单数据 */
export const formSchema: FormSchema[] = [
  {
    label: '机构id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 12},
  },
  {
    label: '机构编号',
    field: 'orgNo',
    component: 'Input',
    required: true,
    colProps: {span: 24},
    componentProps: {disabled: true},
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'TreeSelect',
    componentProps: {
      showSearch: true,
      treeNodeFilterProp: 'name',
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
    required: true,
    colProps: {span: 24},
  },
  {
    label: '机构名称',
    field: 'orgName',
    component: 'Input',
    required: true,
    colProps: {span: 24},
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    required: true,
    colProps: {span: 12},
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    required: true,
    colProps: {span: 12},
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    component: 'InputTextArea',
    required: true,
    colProps: {span: 24},
  },
  {
    label: '已授权业务',
    show: false,
    field: 'authBus',
    component: 'RadioGroup',
    componentProps: {
      options: dict.DicBaseOrgAuthBusOptions,
    },
    colProps: { span: 12 },
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
    },
    required: true,
    colProps: {span: 12},
  },
];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '机构编号',
    field: 'orgNo',
    span: 8,
  },
  {
    label: '区域',
    field: 'areaId',
    span: 8,
  },
  {
    label: '机构名称',
    field: 'orgName',
    span: 8,
  },
  {
    label: '联系人',
    field: 'contact',
    span: 8,
  },
  {
    label: '联系方式',
    field: 'contactUs',
    span: 8,
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    span: 8,
  },
  {
    label: '已授权业务',
    field: 'authBus',
    render: (val) => {
      return dictConversion(dict.DicBaseOrgAuthBusOptions, val);
    },
    span: 8,
  },
  {
    label: '状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicNormalDisableOptions, val);
    },
    span: 8,
  },
];
