import {FormSchema} from '@/components/Form';
import {BasicColumn} from '@/components/Table';
import {DescItem} from '@/components/Description';
import {dicDictList} from '@/api/sys/dict.api';
import {dictConversion} from '@/utils/scratch';
import {TerminalModelIM} from '@/model/core/terminalModel.model';

/** 字典查询 */
export const dictMap = await dicDictList([
  'base_terminal_model_terminal_type',
  'base_terminal_model_screen_type',
  'base_terminal_model_brand',
  'sys_normal_disable'
]);

/** 字典表 */
export const dict: any = {
  DicBaseTerminalModelTerminalTypeOptions: dictMap['base_terminal_model_terminal_type'],
  DicBaseTerminalModelScreenTypeOptions: dictMap['base_terminal_model_screen_type'],
  DicBaseTerminalModelBrandOptions: dictMap['base_terminal_model_brand'],
  DicNormalDisableOptions: dictMap['sys_normal_disable'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '型号名称',
    dataIndex: 'modelName',
    width: 220,
  },
  {
    title: '品牌名称',
    dataIndex: 'brandId',
    width: 220,
    customRender: ({record}) => {
      const data = record as TerminalModelIM;
      return dictConversion(dict.DicBaseTerminalModelBrandOptions, data.brandId);
    },
  },
  {
    title: '终端类型',
    dataIndex: 'terminalType',
    width: 220,
    customRender: ({record}) => {
      const data = record as TerminalModelIM;
      return dictConversion(dict.DicBaseTerminalModelTerminalTypeOptions, data.terminalType);
    },
  },
  {
    title: '屏幕类型',
    dataIndex: 'screenType',
    width: 220,
    customRender: ({record}) => {
      const data = record as TerminalModelIM;
      return dictConversion(dict.DicBaseTerminalModelScreenTypeOptions, data.screenType);
    },
  },
  {
    title: '机头/仓位容量(张)',
    dataIndex: 'hpStorage',
    width: 220,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 220,
    customRender: ({record}) => {
      const data = record as TerminalModelIM;
      return dictConversion(dict.DicNormalDisableOptions, data.status);
    },
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '型号名称',
    field: 'modelName',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '品牌名称',
    field: 'brandId',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTerminalModelBrandOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
  {
    label: '终端类型',
    field: 'terminalType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTerminalModelTerminalTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
  {
    label: '屏幕类型',
    field: 'screenType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTerminalModelScreenTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
  {
    label: '机头/仓位容量(张)',
    field: 'hpStorage',
    component: 'InputNumber',
    colProps: {span: 6},
  },
  {
    label: '状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
];

/** 表单数据 */
export const formSchema: FormSchema[] = [
  {
    label: '终端型号id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 24},
  },
  {
    label: '机头仓位配置',
    field: 'hp',
    component: 'Input',
    show: false,
    colProps: {span: 24},
  },
  {
    label: '型号名称',
    field: 'modelName',
    component: 'Input',
    colProps: {span: 24},
    required: true
  },
  {
    label: '品牌名称',
    field: 'brandId',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTerminalModelBrandOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 24},
    required: true
  },
  {
    label: '终端类型',
    field: 'terminalType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTerminalModelTerminalTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 12},
    required: true
  },
  {
    label: '屏幕类型',
    field: 'screenType',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseTerminalModelScreenTypeOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 12},
    required: true
  },
  {
    label: '容量(张)',
    field: 'hpStorage',
    component: 'InputNumber',
    colProps: {span: 12},
    required: true,
    componentProps:{
      placeholder: '请输入机头/仓位容量(张)',
    }
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
    },
    colProps: {span: 24},
    required: true
  },
];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '型号名称',
    field: 'modelName',
    span: 8,
  },
  {
    label: '品牌名称',
    field: 'brandId',
    render: (val) => {
      return dictConversion(dict.DicBaseTerminalModelBrandOptions, val);
    },
    span: 8,
  },
  {
    label: '终端类型',
    field: 'terminalType',
    render: (val) => {
      return dictConversion(dict.DicBaseTerminalModelTerminalTypeOptions, val);
    },
    span: 8,
  },
  {
    label: '屏幕类型',
    field: 'screenType',
    render: (val) => {
      return dictConversion(dict.DicBaseTerminalModelScreenTypeOptions, val);
    },
    span: 8,
  },
  {
    label: '机头/仓位容量(张)',
    field: 'hpStorage',
    span: 8,
  },
  {
    label: '状态',
    field: 'status',
    span: 8,
  },
];
