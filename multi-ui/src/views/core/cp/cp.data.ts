import {FormSchema} from '@/components/Form';
import {BasicColumn} from '@/components/Table';
import {DescItem} from '@/components/Description';
import {dicDictList} from '@/api/sys/dict.api';
import {dictConversion} from '@/utils/scratch';
import {isNotEmpty} from "@/utils/is";
import {CpIM} from '@/model/core/cp.model';
import {getDeptApi, listDeptApi} from "@/api/system/organize/dept.api";
import {DeptIM} from "@/model/system/organize";

/** 字典查询 */
export const dictMap = await dicDictList(['base_cp_auth_bus', 'sys_normal_disable']);

/** 字典表 */
export const dict: any = {
  DicBaseCpAuthBusOptions: dictMap['base_cp_auth_bus'],
  DicNormalDisableOptions: dictMap['sys_normal_disable'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '渠道编号',
    dataIndex: 'cpNo',
    width: 220,
  },
  {
    title: '区域',
    dataIndex: 'areaId',
    width: 220,
    customRender: async ({record}) => {
      const data = await getDeptApi(record.areaId) as DeptIM;
      record.areaId = data.name;
      return record;
    },
  },
  {
    title: '渠道名称',
    dataIndex: 'cpName',
    width: 220,
  },
  {
    title: '联系人',
    dataIndex: 'contact',
    width: 220,
  },
  {
    title: '联系方式',
    dataIndex: 'contactUs',
    width: 220,
  },
  {
    title: '已授权业务',
    dataIndex: 'authBus',
    width: 220,
    customRender: ({record}) => {
      const data = record as CpIM;
      let nd;
      if (isNotEmpty(data.authBus)) {
        data.authBus.split(',').map((v, i) => {
          if (i === 0) {
            nd = dictConversion(dict.DicBaseCpAuthBusOptions, v)
          } else {
            nd = nd + ',' + dictConversion(dict.DicBaseCpAuthBusOptions, v);
          }
        })
      } else {
        nd = '';
      }
      return nd;
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 220,
    customRender: ({record}) => {
      const data = record as CpIM;
      return dictConversion(dict.DicNormalDisableOptions, data.status);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 220,
  },
  {
    title: '更新时间',
    dataIndex: 'updateTime',
    width: 220,
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '渠道编号',
    field: 'cpNo',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'ApiTreeSelect',
    componentProps: {
      showSearch: true,
      api: listDeptApi,
      treeNodeFilterProp: 'name',
      labelField: 'name',
      valueField: 'id',
      getPopupContainer: () => document.body,
    },
    colProps: {span: 6},
  },
  {
    label: '渠道名称',
    field: 'cpName',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    colProps: {span: 6},
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseCpAuthBusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
  {
    label: '状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: {span: 6},
  },
];


/** 表单数据 - 业务授权 */
export const resAuthBusFormSchema: FormSchema[] = [
  {
    label: '渠道id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 12},
  },
  {
    label: '渠道编号',
    field: 'cpNo',
    component: 'Input',
    required: true,
    colProps: {span: 24},
    componentProps: {disabled: true},
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'TreeSelect',
    componentProps: {
      showSearch: true,
      treeNodeFilterProp: 'name',
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
      disabled: true
    },
    required: true,
    colProps: {span: 24},
  },
  {
    label: '渠道名称',
    field: 'cpName',
    component: 'Input',
    required: true,
    colProps: {span: 24},
    componentProps: {disabled: true},
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    required: true,
    colProps: {span: 12},
    ifShow: false,
    componentProps: {disabled: true},
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    required: true,
    colProps: {span: 12},
    ifShow: false,
    componentProps: {disabled: true},
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    component: 'InputTextArea',
    required: true,
    colProps: {span: 24},
    ifShow: false,
    componentProps: {disabled: true},
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
      disabled: true
    },
    required: true,
    colProps: {span: 24},
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'CheckboxGroup',
    componentProps: {
      options: dict.DicBaseCpAuthBusOptions,
    },
    colProps: {span: 24},
  },
];


/** 表单数据 */
export const formSchema: FormSchema[] = [
  {
    label: '渠道id',
    field: 'id',
    component: 'Input',
    show: false,
    colProps: {span: 12},
  },
  {
    label: '渠道编号',
    field: 'cpNo',
    component: 'Input',
    required: true,
    colProps: {span: 24},
    componentProps: {disabled: true},
  },
  {
    label: '所属区域',
    field: 'areaId',
    component: 'TreeSelect',
    componentProps: {
      showSearch: true,
      treeNodeFilterProp: 'name',
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
    required: true,
    colProps: {span: 24},
  },
  {
    label: '渠道名称',
    field: 'cpName',
    component: 'Input',
    required: true,
    colProps: {span: 24},
  },
  {
    label: '联系人',
    field: 'contact',
    component: 'Input',
    required: true,
    colProps: {span: 12},
  },
  {
    label: '联系方式',
    field: 'contactUs',
    component: 'Input',
    required: true,
    colProps: {span: 12},
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    component: 'InputTextArea',
    required: true,
    colProps: {span: 24},
  },
  {
    label: '已授权业务',
    field: 'authBus',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseCpAuthBusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    show: false,
    colProps: {span: 24},
  },
  {
    label: '状态',
    field: 'status',
    component: 'RadioButtonGroup',
    componentProps: {
      options: dict.DicNormalDisableOptions,
    },
    required: true,
    colProps: {span: 12},
  },
];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '渠道编号1',
    field: 'cpNo',
    span: 8,
  },
  {
    label: '区域',
    field: 'areaId',
    span: 8,
  },
  {
    label: '渠道名称',
    field: 'cpName',
    span: 8,
  },
  {
    label: '联系人',
    field: 'contact',
    span: 8,
  },
  {
    label: '联系方式',
    field: 'contactUs',
    span: 8,
  },
  {
    label: '联系地址',
    field: 'contactAddress',
    span: 8,
  },
  {
    label: '已授权业务',
    field: 'authBus',
    render: (val) => {
      return dictConversion(dict.DicBaseCpAuthBusOptions, val);
    },
    span: 8,
  },
  {
    label: '状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicNormalDisableOptions, val);
    },
    span: 8,
  },
];
