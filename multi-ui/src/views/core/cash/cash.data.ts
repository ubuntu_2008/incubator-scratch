import { FormSchema } from '@/components/Form';
import { BasicColumn } from '@/components/Table';
import { DescItem } from '@/components/Description';
import { dicDictList } from '@/api/sys/dict.api';
import { dictConversion } from '@/utils/scratch';
import { CashIM } from '@/model/core/cash.model';

/** 字典查询 */
export const dictMap = await dicDictList([
    'base_cash_status',
]);

/** 字典表 */
export const dict: any = {
  DicBaseCashStatusOptions: dictMap['base_cash_status'],
};

/** 表格数据 */
export const columns: BasicColumn[] = [
  {
    title: '设备编号',
    dataIndex: 'devNo',
    width: 220,
  },
  {
    title: '用户ID',
    dataIndex: 'userId',
    width: 220,
  },
  {
    title: '订单编号',
    dataIndex: 'orderNo',
    width: 220,
  },
  {
    title: '游戏名称',
    dataIndex: 'playName',
    width: 220,
  },
  {
    title: '保安码',
    dataIndex: 'dataArea',
    width: 220,
  },
  {
    title: '票号',
    dataIndex: 'ticketCode',
    width: 220,
  },
  {
    title: '中奖金额',
    dataIndex: 'winMoney',
    width: 220,
  },
  {
    title: '兑奖时间',
    dataIndex: 'cashTime',
    width: 220,
  },
  {
    title: '兑奖状态',
    dataIndex: 'status',
    width: 220,
    customRender: ({ record }) => {
      const data = record as CashIM;
      return dictConversion(dict.DicBaseCashStatusOptions, data.status);
    },
  },
];

/** 查询数据 */
export const searchFormSchema: FormSchema[] = [
  {
    label: '设备编号',
    field: 'devNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '用户ID',
    field: 'userId',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '订单编号',
    field: 'orderNo',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '游戏名称',
    field: 'playName',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '保安码',
    field: 'dataArea',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '票号',
    field: 'ticketCode',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '中奖金额',
    field: 'winMoney',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '兑奖时间',
    field: 'cashTime',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    label: '兑奖状态',
    field: 'status',
    component: 'Select',
    componentProps: {
      options: dict.DicBaseCashStatusOptions,
      showSearch: true,
      optionFilterProp: 'label',
    },
    colProps: { span: 6 },
  },
];

/** 表单数据 */
export const formSchema: FormSchema[] = [
];

/** 详情数据 */
export const detailSchema: DescItem[] = [
  {
    label: '设备编号',
    field: 'devNo',
    span: 8,
  },
  {
    label: '用户ID',
    field: 'userId',
    span: 8,
  },
  {
    label: '订单编号',
    field: 'orderNo',
    span: 8,
  },
  {
    label: '游戏名称',
    field: 'playName',
    span: 8,
  },
  {
    label: '保安码',
    field: 'dataArea',
    span: 8,
  },
  {
    label: '票号',
    field: 'ticketCode',
    span: 8,
  },
  {
    label: '中奖金额',
    field: 'winMoney',
    span: 8,
  },
  {
    label: '兑奖时间',
    field: 'cashTime',
    span: 8,
  },
  {
    label: '兑奖状态',
    field: 'status',
    render: (val) => {
      return dictConversion(dict.DicBaseCashStatusOptions, val);
    },
    span: 8,
  },
  {
    label: '签名',
    field: 'sign',
    span: 8,
  },
];
