module.exports = {
  root: true,
  extends: ['@scratch'],
  rules: {
    'no-undef': 'off',
  },
  globals: {
    Fn: true,
    ElRef: true,
    PropType: true,
    Nullable: true,
    Recordable: true,
  },
};
