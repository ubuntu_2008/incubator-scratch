package com.scratch.system.monitor.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.system.api.log.domain.dto.SysLoginLogDto;
import com.scratch.system.api.log.domain.po.SysLoginLogPo;
import com.scratch.system.api.log.domain.query.SysLoginLogQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 系统服务 | 监控模块 | 访问日志 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysLoginLogConverter extends BaseConverter<SysLoginLogQuery, SysLoginLogDto, SysLoginLogPo> {
}
