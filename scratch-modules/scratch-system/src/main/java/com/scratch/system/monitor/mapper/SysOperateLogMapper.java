package com.scratch.system.monitor.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.log.domain.dto.SysOperateLogDto;
import com.scratch.system.api.log.domain.po.SysOperateLogPo;
import com.scratch.system.api.log.domain.query.SysOperateLogQuery;

/**
 * 系统服务 | 监控模块 | 操作日志管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysOperateLogMapper extends BaseMapper<SysOperateLogQuery, SysOperateLogDto, SysOperateLogPo> {
}
