package com.scratch.system.organize.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.system.api.organize.domain.dto.SysEnterpriseDto;
import com.scratch.system.api.organize.domain.query.SysEnterpriseQuery;
import com.scratch.system.organize.service.ISysEnterpriseService;

/**
 * 系统服务 | 组织模块 | 企业管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BSysEnterpriseController extends BaseController<SysEnterpriseQuery, SysEnterpriseDto, ISysEnterpriseService> {

    /** 定义节点名称 */
    protected String getNodeName() {
        return "企业";
    }

}
