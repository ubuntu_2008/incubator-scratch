package com.scratch.system.file.mapper;

import com.scratch.system.file.domain.query.SysFileQuery;
import com.scratch.system.file.domain.dto.SysFileDto;
import com.scratch.system.file.domain.po.SysFilePo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 系统服务 | 素材模块 | 文件管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysFileMapper extends BaseMapper<SysFileQuery, SysFileDto, SysFilePo> {
}