package com.scratch.system.authority.mapper.merge;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.authority.domain.merge.SysRoleMenuMerge;

/**
 * 系统服务 | 权限模块 | 角色-菜单关联 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysRoleMenuMergeMapper extends BasicMapper<SysRoleMenuMerge> {
}
