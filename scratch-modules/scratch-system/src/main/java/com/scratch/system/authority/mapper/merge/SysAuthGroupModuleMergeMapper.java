package com.scratch.system.authority.mapper.merge;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.authority.domain.merge.SysAuthGroupModuleMerge;

/**
 * 系统服务 | 权限模块 | 企业权限组-模块关联 数据层
 *
 * @author scratch
 */
@Master
public interface SysAuthGroupModuleMergeMapper extends BasicMapper<SysAuthGroupModuleMerge> {
}