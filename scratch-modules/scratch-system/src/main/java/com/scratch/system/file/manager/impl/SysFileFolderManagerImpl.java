package com.scratch.system.file.manager.impl;

import com.scratch.system.file.domain.po.SysFileFolderPo;
import com.scratch.system.file.domain.dto.SysFileFolderDto;
import com.scratch.system.file.domain.query.SysFileFolderQuery;
import com.scratch.system.file.domain.model.SysFileFolderConverter;
import com.scratch.system.file.mapper.SysFileFolderMapper;
import com.scratch.common.web.entity.manager.impl.TreeManagerImpl;
import com.scratch.system.file.manager.ISysFileFolderManager;
import org.springframework.stereotype.Component;

/**
 * 系统服务 | 素材模块 | 文件分类管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysFileFolderManagerImpl extends TreeManagerImpl<SysFileFolderQuery, SysFileFolderDto, SysFileFolderPo, SysFileFolderMapper, SysFileFolderConverter> implements ISysFileFolderManager {
}