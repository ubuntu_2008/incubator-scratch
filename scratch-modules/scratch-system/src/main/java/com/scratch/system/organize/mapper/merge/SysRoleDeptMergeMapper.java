package com.scratch.system.organize.mapper.merge;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.organize.domain.merge.SysRoleDeptMerge;

/**
 * 系统服务 | 组织模块 | 角色-部门关联（权限范围） 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysRoleDeptMergeMapper extends BasicMapper<SysRoleDeptMerge> {
}