package com.scratch.system.monitor.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.core.utils.core.ObjectUtil;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.system.api.log.domain.dto.SysLoginLogDto;
import com.scratch.system.api.log.domain.po.SysLoginLogPo;
import com.scratch.system.api.log.domain.query.SysLoginLogQuery;
import com.scratch.system.monitor.domain.model.SysLoginLogConverter;
import com.scratch.system.monitor.manager.ISysLoginLogManager;
import com.scratch.system.monitor.mapper.SysLoginLogMapper;
import org.springframework.stereotype.Component;

/**
 * 系统服务 | 监控模块 | 访问日志管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysLoginLogManagerImpl extends BaseManagerImpl<SysLoginLogQuery, SysLoginLogDto, SysLoginLogPo, SysLoginLogMapper, SysLoginLogConverter> implements ISysLoginLogManager {

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLoginLog() {
        baseMapper.delete(Wrappers.update());
    }

    /**
     * 查询条件构造 | 列表查询
     *
     * @param loginLog 数据查询对象
     */
    @Override
    protected LambdaQueryWrapper<SysLoginLogPo> selectListQuery(SysLoginLogQuery loginLog) {
        return Wrappers.<SysLoginLogPo>query(loginLog).lambda()
                .func(i -> {
                    if (ObjectUtil.isAllNotEmpty(loginLog.getAccessTimeStart(), loginLog.getAccessTimeEnd())) {
                        i.between(SysLoginLogPo::getAccessTime, loginLog.getAccessTimeStart(), loginLog.getAccessTimeEnd());
                    }
                });
    }
}
