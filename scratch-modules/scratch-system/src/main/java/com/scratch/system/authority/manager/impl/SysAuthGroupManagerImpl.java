package com.scratch.system.authority.manager.impl;

import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.system.authority.domain.dto.SysAuthGroupDto;
import com.scratch.system.authority.domain.model.SysAuthGroupConverter;
import com.scratch.system.authority.domain.po.SysAuthGroupPo;
import com.scratch.system.authority.domain.query.SysAuthGroupQuery;
import com.scratch.system.authority.manager.ISysAuthGroupManager;
import com.scratch.system.authority.mapper.SysAuthGroupMapper;
import org.springframework.stereotype.Component;

/**
 * 系统服务 | 权限模块 | 企业权限组管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysAuthGroupManagerImpl extends BaseManagerImpl<SysAuthGroupQuery, SysAuthGroupDto, SysAuthGroupPo, SysAuthGroupMapper, SysAuthGroupConverter> implements ISysAuthGroupManager {
}