package com.scratch.system.dict.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.dict.domain.dto.SysDictTypeDto;
import com.scratch.system.api.dict.domain.po.SysDictTypePo;
import com.scratch.system.api.dict.domain.query.SysDictTypeQuery;

/**
 * 系统服务 | 字典模块 | 字典类型管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysDictTypeMapper extends BaseMapper<SysDictTypeQuery, SysDictTypeDto, SysDictTypePo> {
}
