package com.scratch.system.file.service;

import com.scratch.system.file.domain.query.SysFileFolderQuery;
import com.scratch.system.file.domain.dto.SysFileFolderDto;
import com.scratch.common.web.entity.service.ITreeService;

/**
 * 系统服务 | 素材模块 | 文件分类管理 服务层
 *
 * @author scratch
 */
public interface ISysFileFolderService extends ITreeService<SysFileFolderQuery, SysFileFolderDto> {
}