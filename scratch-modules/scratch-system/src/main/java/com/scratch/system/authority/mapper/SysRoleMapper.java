package com.scratch.system.authority.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.authority.domain.dto.SysRoleDto;
import com.scratch.system.api.authority.domain.po.SysRolePo;
import com.scratch.system.api.authority.domain.query.SysRoleQuery;

/**
 * 岗位管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysRoleMapper extends BaseMapper<SysRoleQuery, SysRoleDto, SysRolePo> {
}
