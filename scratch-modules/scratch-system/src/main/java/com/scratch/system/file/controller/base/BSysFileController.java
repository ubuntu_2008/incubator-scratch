package com.scratch.system.file.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.system.file.domain.dto.SysFileDto;
import com.scratch.system.file.domain.query.SysFileQuery;
import com.scratch.system.file.service.ISysFileService;

/**
 * 系统服务 | 素材模块 | 文件管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BSysFileController extends BaseController<SysFileQuery, SysFileDto, ISysFileService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "菜单文件" ;
    }
}
