package com.scratch.system.organize.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.system.api.organize.domain.dto.SysEnterpriseDto;
import com.scratch.system.api.organize.domain.query.SysEnterpriseQuery;

/**
 * 系统服务 | 组织模块 | 企业管理 数据封装层
 *
 * @author scratch
 */
public interface ISysEnterpriseManager extends IBaseManager<SysEnterpriseQuery, SysEnterpriseDto> {

    /**
     * 根据名称查询状态正常企业对象
     *
     * @param name 名称
     * @return 企业对象
     */
    SysEnterpriseDto selectByName(String name);
}
