package com.scratch.system.authority.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.system.api.authority.domain.dto.SysModuleDto;
import com.scratch.system.api.authority.domain.po.SysModulePo;
import com.scratch.system.api.authority.domain.query.SysModuleQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 系统服务 | 权限模块 | 模块 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysModuleConverter extends BaseConverter<SysModuleQuery, SysModuleDto, SysModulePo> {
}
