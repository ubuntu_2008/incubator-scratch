package com.scratch.system.authority.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.system.authority.domain.dto.SysAuthGroupDto;
import com.scratch.system.authority.domain.query.SysAuthGroupQuery;

/**
 * 系统服务 | 权限模块 | 企业权限组管理 数据封装层
 *
 * @author scratch
 */
public interface ISysAuthGroupManager extends IBaseManager<SysAuthGroupQuery, SysAuthGroupDto> {
}