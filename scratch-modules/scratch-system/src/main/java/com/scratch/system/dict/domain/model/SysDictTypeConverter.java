package com.scratch.system.dict.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.system.api.dict.domain.dto.SysDictTypeDto;
import com.scratch.system.api.dict.domain.po.SysDictTypePo;
import com.scratch.system.api.dict.domain.query.SysDictTypeQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 系统服务 | 字典模块 | 字典类型 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysDictTypeConverter extends BaseConverter<SysDictTypeQuery, SysDictTypeDto, SysDictTypePo> {
}
