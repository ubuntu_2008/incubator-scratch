package com.scratch.system.authority.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.authority.domain.dto.SysModuleDto;
import com.scratch.system.api.authority.domain.po.SysModulePo;
import com.scratch.system.api.authority.domain.query.SysModuleQuery;

/**
 * 系统服务 | 权限模块 | 角色管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysModuleMapper extends BaseMapper<SysModuleQuery, SysModuleDto, SysModulePo> {
}
