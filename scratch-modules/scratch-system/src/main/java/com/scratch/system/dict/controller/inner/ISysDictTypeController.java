package com.scratch.system.dict.controller.inner;

import com.scratch.common.core.constant.basic.SecurityConstants;
import com.scratch.common.core.context.SecurityContextHolder;
import com.scratch.common.core.web.result.R;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.common.security.annotation.InnerAuth;
import com.scratch.system.dict.controller.base.BSysDictTypeController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统服务 | 字典模块 | 字典类型管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@RestController
@RequestMapping("/inner/dict/type")
public class ISysDictTypeController extends BSysDictTypeController {

    /**
     * 同步字典缓存 | 租户数据
     *
     * @return 结果
     */
    @InnerAuth
    @GetMapping(value = "/sync")
    public R<Boolean> syncCacheInner() {
        return R.ok(baseService.syncCache());
    }

    /**
     * 刷新字典缓存 | 租户数据
     */
    @Override
    @InnerAuth
    @GetMapping("/refresh")
    @Log(title = "字典类型", businessType = BusinessType.REFRESH)
    public R<Boolean> refreshCacheInner() {
        return super.refreshCacheInner();
    }

    /**
     * 刷新字典缓存 | 默认数据
     */
    @InnerAuth(isAnonymous = true)
    @GetMapping("/common/refresh")
    @Log(title = "字典类型", businessType = BusinessType.REFRESH)
    public R<Boolean> refreshTeCacheInner() {
        SecurityContextHolder.setEnterpriseId(SecurityConstants.COMMON_TENANT_ID.toString());
        return super.refreshCacheInner();
    }
}
