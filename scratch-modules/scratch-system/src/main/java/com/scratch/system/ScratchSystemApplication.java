package com.scratch.system;

import com.scratch.common.security.annotation.EnableCustomConfig;
import com.scratch.common.security.annotation.EnableResourceServer;
import com.scratch.common.security.annotation.EnableRyFeignClients;
import com.scratch.common.swagger.annotation.EnableCustomSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 系统模块
 *  
 * @author scratch
 */
@EnableCustomConfig
@EnableCustomSwagger
@EnableResourceServer
@EnableRyFeignClients
@SpringBootApplication
public class ScratchSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScratchSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}