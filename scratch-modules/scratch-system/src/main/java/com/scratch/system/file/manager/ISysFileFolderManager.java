package com.scratch.system.file.manager;

import com.scratch.system.file.domain.dto.SysFileFolderDto;
import com.scratch.system.file.domain.query.SysFileFolderQuery;
import com.scratch.common.web.entity.manager.ITreeManager;

/**
 * 系统服务 | 素材模块 | 文件分类管理 数据封装层
 *
 * @author scratch
 */
public interface ISysFileFolderManager extends ITreeManager<SysFileFolderQuery, SysFileFolderDto> {
}