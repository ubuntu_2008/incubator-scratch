package com.scratch.system.organize.mapper.merge;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.organize.domain.merge.SysOrganizeRoleMerge;

/**
 * 系统服务 | 组织模块 | 组织-角色关联（角色绑定） 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysOrganizeRoleMergeMapper extends BasicMapper<SysOrganizeRoleMerge> {
}
