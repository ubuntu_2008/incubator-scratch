package com.scratch.system.organize.mapper.merge;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.organize.domain.merge.SysUserPostMerge;

/**
 * 系统服务 | 组织模块 | 用户-岗位关联 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysUserPostMergeMapper extends BasicMapper<SysUserPostMerge> {
}
