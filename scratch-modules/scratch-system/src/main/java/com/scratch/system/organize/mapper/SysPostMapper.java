package com.scratch.system.organize.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.organize.domain.dto.SysPostDto;
import com.scratch.system.api.organize.domain.po.SysPostPo;
import com.scratch.system.api.organize.domain.query.SysPostQuery;

/**
 * 系统服务 | 组织模块 | 岗位管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysPostMapper extends BaseMapper<SysPostQuery, SysPostDto, SysPostPo> {
}
