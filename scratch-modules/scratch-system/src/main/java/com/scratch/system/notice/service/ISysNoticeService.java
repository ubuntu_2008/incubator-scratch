package com.scratch.system.notice.service;

import com.scratch.common.web.entity.service.IBaseService;
import com.scratch.system.notice.domain.dto.SysNoticeDto;
import com.scratch.system.notice.domain.query.SysNoticeQuery;

/**
 * 系统服务 | 消息模块 | 通知公告管理 服务层
 *
 * @author scratch
 */
public interface ISysNoticeService extends IBaseService<SysNoticeQuery, SysNoticeDto> {
}