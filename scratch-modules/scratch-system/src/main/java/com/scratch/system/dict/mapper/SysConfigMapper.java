package com.scratch.system.dict.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.dict.domain.dto.SysConfigDto;
import com.scratch.system.api.dict.domain.po.SysConfigPo;
import com.scratch.system.api.dict.domain.query.SysConfigQuery;

/**
 * 系统服务 | 字典模块 | 参数管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysConfigMapper extends BaseMapper<SysConfigQuery, SysConfigDto, SysConfigPo> {
}