package com.scratch.system.authority.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.authority.domain.dto.SysClientDto;
import com.scratch.system.api.authority.domain.po.SysClientPo;
import com.scratch.system.api.authority.domain.query.SysClientQuery;

/**
 * 系统服务 | 权限模块 | 客户端管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysClientMapper extends BaseMapper<SysClientQuery, SysClientDto, SysClientPo> {
}
