package com.scratch.system.notice.manager.impl;

import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.system.notice.domain.dto.SysNoticeDto;
import com.scratch.system.notice.domain.model.SysNoticeConverter;
import com.scratch.system.notice.domain.po.SysNoticePo;
import com.scratch.system.notice.domain.query.SysNoticeQuery;
import com.scratch.system.notice.manager.ISysNoticeManager;
import com.scratch.system.notice.mapper.SysNoticeMapper;
import org.springframework.stereotype.Component;

/**
 * 系统服务 | 消息模块 | 通知公告管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysNoticeManagerImpl extends BaseManagerImpl<SysNoticeQuery, SysNoticeDto, SysNoticePo, SysNoticeMapper, SysNoticeConverter> implements ISysNoticeManager {
}
