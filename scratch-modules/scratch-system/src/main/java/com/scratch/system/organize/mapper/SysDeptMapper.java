package com.scratch.system.organize.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.TreeMapper;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import com.scratch.system.api.organize.domain.po.SysDeptPo;
import com.scratch.system.api.organize.domain.query.SysDeptQuery;

/**
 * 系统服务 | 组织模块 | 部门管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysDeptMapper extends TreeMapper<SysDeptQuery, SysDeptDto, SysDeptPo> {
}
