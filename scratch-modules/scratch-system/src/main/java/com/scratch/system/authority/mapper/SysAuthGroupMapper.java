package com.scratch.system.authority.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.authority.domain.dto.SysAuthGroupDto;
import com.scratch.system.authority.domain.po.SysAuthGroupPo;
import com.scratch.system.authority.domain.query.SysAuthGroupQuery;

/**
 * 系统服务 | 权限模块 | 企业权限组管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysAuthGroupMapper extends BaseMapper<SysAuthGroupQuery, SysAuthGroupDto, SysAuthGroupPo> {
}