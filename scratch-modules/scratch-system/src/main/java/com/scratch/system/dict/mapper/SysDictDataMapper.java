package com.scratch.system.dict.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.dict.domain.dto.SysDictDataDto;
import com.scratch.system.api.dict.domain.po.SysDictDataPo;
import com.scratch.system.api.dict.domain.query.SysDictDataQuery;

/**
 * 系统服务 | 字典模块 | 字典数据管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysDictDataMapper extends BaseMapper<SysDictDataQuery, SysDictDataDto, SysDictDataPo> {
}
