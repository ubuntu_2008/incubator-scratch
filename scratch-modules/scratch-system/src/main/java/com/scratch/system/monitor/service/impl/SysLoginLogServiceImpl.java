package com.scratch.system.monitor.service.impl;


import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.system.api.log.domain.dto.SysLoginLogDto;
import com.scratch.system.api.log.domain.query.SysLoginLogQuery;
import com.scratch.system.monitor.domain.correlate.SysLoginLogCorrelate;
import com.scratch.system.monitor.manager.ISysLoginLogManager;
import com.scratch.system.monitor.service.ISysLoginLogService;
import org.springframework.stereotype.Service;

/**
 * 系统服务 | 监控模块 | 访问日志管理 服务层处理
 *
 * @author scratch
 */
@Service
public class SysLoginLogServiceImpl extends BaseServiceImpl<SysLoginLogQuery, SysLoginLogDto, SysLoginLogCorrelate, ISysLoginLogManager> implements ISysLoginLogService {

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLoginLog() {
        baseManager.cleanLoginLog();
    }
}
