package com.scratch.system.notice.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.system.notice.domain.dto.SysNoticeDto;
import com.scratch.system.notice.domain.po.SysNoticePo;
import com.scratch.system.notice.domain.query.SysNoticeQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 系统服务 | 消息模块 | 通知公告 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysNoticeConverter extends BaseConverter<SysNoticeQuery, SysNoticeDto, SysNoticePo> {
}
