package com.scratch.system.dict.controller.base;

import cn.hutool.core.collection.CollUtil;
import com.scratch.common.cache.utils.DictUtil;
import com.scratch.common.core.constant.basic.BaseConstants;
import com.scratch.common.core.utils.core.ObjectUtil;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.system.api.dict.domain.dto.SysDictTypeDto;
import com.scratch.system.api.dict.domain.query.SysDictTypeQuery;
import com.scratch.system.dict.service.ISysDictTypeService;

import java.util.List;

/**
 * 系统服务 | 字典模块 | 字典类型管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BSysDictTypeController extends BaseController<SysDictTypeQuery, SysDictTypeDto, ISysDictTypeService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "字典类型";
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    public AjaxResult listByCode(String code) {
        if (StrUtil.isEmpty(code)) {
            warn("请传入编码后再查询字典");
        }
        return AjaxResult.success(DictUtil.getDictCache(code));
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    public AjaxResult listByCodeList(List<String> codeList) {
        if (CollUtil.isEmpty(codeList)) {
            warn("请传入编码后再查询字典");
        }
        return AjaxResult.success(DictUtil.getDictCache(codeList));
    }

    /**
     * 前置校验 增加/修改
     */
    @Override
    protected void AEHandle(BaseConstants.Operate operate, SysDictTypeDto dictType) {
        if (ObjectUtil.isNull(dictType.getTenantId()) && baseService.checkDictCodeUnique(dictType.getId(), dictType.getCode())) {
            warn(StrUtil.format("{}{}{}失败，字典编码已存在", operate.getInfo(), getNodeName(), dictType.getName()));
        }
    }
}
