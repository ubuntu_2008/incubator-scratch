package com.scratch.system.file.mapper;

import com.scratch.system.file.domain.query.SysFileFolderQuery;
import com.scratch.system.file.domain.dto.SysFileFolderDto;
import com.scratch.system.file.domain.po.SysFileFolderPo;
import com.scratch.common.web.entity.mapper.TreeMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 系统服务 | 素材模块 | 文件分类管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysFileFolderMapper extends TreeMapper<SysFileFolderQuery, SysFileFolderDto, SysFileFolderPo> {
}