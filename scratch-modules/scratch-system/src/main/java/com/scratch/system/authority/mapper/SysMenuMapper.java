package com.scratch.system.authority.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.TreeMapper;
import com.scratch.system.api.authority.domain.dto.SysMenuDto;
import com.scratch.system.api.authority.domain.po.SysMenuPo;
import com.scratch.system.api.authority.domain.query.SysMenuQuery;

/**
 * 系统服务 | 权限模块 | 菜单管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysMenuMapper extends TreeMapper<SysMenuQuery, SysMenuDto, SysMenuPo> {
}
