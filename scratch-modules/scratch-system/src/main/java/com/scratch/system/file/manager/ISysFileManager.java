package com.scratch.system.file.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.system.file.domain.dto.SysFileDto;
import com.scratch.system.file.domain.query.SysFileQuery;

/**
 * 系统服务 | 素材模块 | 文件管理 数据封装层
 *
 * @author scratch
 */
public interface ISysFileManager extends IBaseManager<SysFileQuery, SysFileDto> {

    /**
     * 根据文件Url删除文件
     *
     * @param url 文件路径
     * @return 结果
     */
    int deleteByUrl(String url);
}