package com.scratch.system.authority.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.service.CorrelateService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 系统服务 | 权限模块 | 客户端 关联映射
 *
 * @author scratch
 */
@Getter
@AllArgsConstructor
public enum SysClientCorrelate implements CorrelateService {

    ;

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}
