package com.scratch.system.dict.controller.base;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.cache.utils.DictUtil;
import com.scratch.common.core.constant.basic.BaseConstants;
import com.scratch.common.core.utils.core.CollUtil;
import com.scratch.common.core.utils.core.ObjectUtil;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.system.api.dict.domain.dto.SysConfigDto;
import com.scratch.system.api.dict.domain.query.SysConfigQuery;
import com.scratch.system.dict.service.ISysConfigService;

import java.util.List;

/**
 * 系统服务 | 字典模块 | 参数管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BSysConfigController extends BaseController<SysConfigQuery, SysConfigDto, ISysConfigService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "参数";
    }

    /**
     * 查询参数
     *
     * @param code 参数编码
     * @return 参数
     */
    public AjaxResult getValueByCode(String code) {
        Object obj = DictUtil.getConfigCache(CacheConstants.ConfigType.getByCode(code));
        return success(obj);
    }

    /**
     * 前置校验 增加/修改
     */
    @Override
    protected void AEHandle(BaseConstants.Operate operate, SysConfigDto config) {
        if (ObjectUtil.isNull(config.getTenantId()) && baseService.checkConfigCodeUnique(config.getId(), config.getCode())) {
            warn(StrUtil.format("{}{}{}失败，参数编码已存在", operate.getInfo(), getNodeName(), config.getName()));
        }
    }

    /**
     * 前置校验 删除
     *
     * @param idList Id集合
     */
    @Override
    protected void RHandle(BaseConstants.Operate operate, List<Long> idList) {
        if (operate.isDelete()) {
            int size = idList.size();
            // remove oneself or admin
            for (int i = idList.size() - 1; i >= 0; i--) {
                if (baseService.checkIsBuiltIn(idList.get(i))) {
                    idList.remove(i);
                }
            }
            if (CollUtil.isEmpty(idList)) {
                warn(StrUtil.format("{}失败，不能删除内置参数！", operate.getInfo()));
            } else if (idList.size() != size) {
                baseService.deleteByIds(idList);
                warn(StrUtil.format("成功{}除内置参数外的所有参数！", operate.getInfo()));
            }
        }
    }
}
