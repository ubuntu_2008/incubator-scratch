package com.scratch.system.file.service.impl;

import com.scratch.common.web.entity.service.impl.TreeServiceImpl;
import com.scratch.system.file.domain.correlate.SysFileFolderCorrelate;
import com.scratch.system.file.domain.dto.SysFileFolderDto;
import com.scratch.system.file.domain.query.SysFileFolderQuery;
import com.scratch.system.file.manager.ISysFileFolderManager;
import com.scratch.system.file.service.ISysFileFolderService;
import org.springframework.stereotype.Service;

/**
 * 系统服务 | 素材模块 | 文件分类管理 服务层处理
 *
 * @author scratch
 */
@Service
public class SysFileFolderServiceImpl extends TreeServiceImpl<SysFileFolderQuery, SysFileFolderDto, SysFileFolderCorrelate, ISysFileFolderManager> implements ISysFileFolderService {
}