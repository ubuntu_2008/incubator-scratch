package com.scratch.system.authority.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.domain.Direct;
import com.scratch.common.web.correlate.domain.Indirect;
import com.scratch.common.web.correlate.service.CorrelateService;
import com.scratch.system.api.authority.domain.dto.SysMenuDto;
import com.scratch.system.api.authority.domain.dto.SysModuleDto;
import com.scratch.system.api.organize.domain.dto.SysEnterpriseDto;
import com.scratch.system.authority.domain.merge.SysAuthGroupMenuMerge;
import com.scratch.system.authority.domain.merge.SysRoleMenuMerge;
import com.scratch.system.authority.mapper.merge.SysAuthGroupMenuMergeMapper;
import com.scratch.system.authority.mapper.merge.SysRoleMenuMergeMapper;
import com.scratch.system.authority.service.ISysModuleService;
import com.scratch.system.organize.service.ISysEnterpriseService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.scratch.common.web.correlate.contant.CorrelateConstants.SubOperate.DELETE;
import static com.scratch.common.web.correlate.contant.CorrelateConstants.SubOperate.SELECT;

/**
 * 系统服务 | 权限模块 | 菜单 关联映射
 *
 * @author scratch
 */
@Getter
@AllArgsConstructor
public enum SysMenuCorrelate implements CorrelateService {

    EN_INFO_SELECT("企业查询|（企业信息）", new ArrayList<>() {{
        // 菜单 | 企业信息
        add(new Direct<>(SELECT, ISysEnterpriseService.class, SysMenuDto::getTenantId, SysEnterpriseDto::getId, SysMenuDto::getEnterpriseInfo));
    }}),
    INFO_LIST("默认列表|（模块）", new ArrayList<>() {{
        // 菜单 | 模块
        add(new Direct<>(SELECT, ISysModuleService.class, SysMenuDto::getModuleId, SysModuleDto::getId, SysMenuDto::getModule));
    }}),
    BASE_DEL("默认删除|（角色-菜单关联 | 企业权限组-菜单关联）", new ArrayList<>() {{
        // 菜单 | 角色-菜单关联
        add(new Indirect<>(DELETE, SysRoleMenuMergeMapper.class, SysRoleMenuMerge::getMenuId, SysMenuDto::getId));
        // 模块 | 企业权限组-菜单关联
        add(new Indirect<>(DELETE, SysAuthGroupMenuMergeMapper.class, SysAuthGroupMenuMerge::getMenuId, SysMenuDto::getId));
    }});

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}
