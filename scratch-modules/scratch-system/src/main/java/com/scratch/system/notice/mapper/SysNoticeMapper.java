package com.scratch.system.notice.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.notice.domain.dto.SysNoticeDto;
import com.scratch.system.notice.domain.po.SysNoticePo;
import com.scratch.system.notice.domain.query.SysNoticeQuery;

/**
 * 系统服务 | 消息模块 | 通知公告管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysNoticeMapper extends BaseMapper<SysNoticeQuery, SysNoticeDto, SysNoticePo> {
}
