package com.scratch.system.authority.mapper.merge;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.authority.domain.merge.SysTenantAuthGroupMerge;

/**
 * 系统服务 | 权限模块 | 企业-企业权限组关联 数据层
 *
 * @author scratch
 */
@Master
public interface SysTenantAuthGroupMergeMapper extends BasicMapper<SysTenantAuthGroupMerge> {
}