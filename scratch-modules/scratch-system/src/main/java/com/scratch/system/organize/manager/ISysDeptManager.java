package com.scratch.system.organize.manager;

import com.scratch.common.web.entity.manager.ITreeManager;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import com.scratch.system.api.organize.domain.query.SysDeptQuery;

/**
 * 系统服务 | 组织模块 | 部门管理 数据封装层
 *
 * @author scratch
 */
public interface ISysDeptManager extends ITreeManager<SysDeptQuery, SysDeptDto> {
}
