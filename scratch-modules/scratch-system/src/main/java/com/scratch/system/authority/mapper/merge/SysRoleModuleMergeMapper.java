package com.scratch.system.authority.mapper.merge;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BasicMapper;
import com.scratch.system.authority.domain.merge.SysRoleModuleMerge;

/**
 * 系统服务 | 权限模块 | 角色-模块关联 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysRoleModuleMergeMapper extends BasicMapper<SysRoleModuleMerge> {
}
