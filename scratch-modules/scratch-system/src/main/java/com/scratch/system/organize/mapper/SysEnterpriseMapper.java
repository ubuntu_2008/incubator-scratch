package com.scratch.system.organize.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.organize.domain.dto.SysEnterpriseDto;
import com.scratch.system.api.organize.domain.po.SysEnterprisePo;
import com.scratch.system.api.organize.domain.query.SysEnterpriseQuery;

/**
 * 系统服务 | 组织模块 | 企业管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysEnterpriseMapper extends BaseMapper<SysEnterpriseQuery, SysEnterpriseDto, SysEnterprisePo> {
}
