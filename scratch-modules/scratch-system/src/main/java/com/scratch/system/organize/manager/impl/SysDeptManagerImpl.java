package com.scratch.system.organize.manager.impl;

import com.scratch.common.web.entity.manager.impl.TreeManagerImpl;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import com.scratch.system.api.organize.domain.po.SysDeptPo;
import com.scratch.system.api.organize.domain.query.SysDeptQuery;
import com.scratch.system.organize.domain.model.SysDeptConverter;
import com.scratch.system.organize.manager.ISysDeptManager;
import com.scratch.system.organize.mapper.SysDeptMapper;
import org.springframework.stereotype.Component;

/**
 * 系统服务 | 组织模块 | 部门管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysDeptManagerImpl extends TreeManagerImpl<SysDeptQuery, SysDeptDto, SysDeptPo, SysDeptMapper, SysDeptConverter> implements ISysDeptManager {
}
