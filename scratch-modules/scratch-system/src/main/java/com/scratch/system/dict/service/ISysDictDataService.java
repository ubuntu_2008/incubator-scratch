package com.scratch.system.dict.service;

import com.scratch.common.web.entity.service.IBaseService;
import com.scratch.system.api.dict.domain.dto.SysDictDataDto;
import com.scratch.system.api.dict.domain.query.SysDictDataQuery;

/**
 * 系统服务 | 字典模块 | 字典数据管理 服务层
 *
 * @author scratch
 */
public interface ISysDictDataService extends IBaseService<SysDictDataQuery, SysDictDataDto> {
}
