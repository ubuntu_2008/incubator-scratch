package com.scratch.system.authority.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.system.api.authority.domain.dto.SysClientDto;
import com.scratch.system.api.authority.domain.po.SysClientPo;
import com.scratch.system.api.authority.domain.query.SysClientQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 系统服务 | 权限模块 | 客户端 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysClientConverter extends BaseConverter<SysClientQuery, SysClientDto, SysClientPo> {
}