package com.scratch.system.dict.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.OperateConstants;
import com.scratch.common.core.constant.basic.SecurityConstants;
import com.scratch.common.core.context.SecurityContextHolder;
import com.scratch.common.core.exception.ServiceException;
import com.scratch.common.core.utils.core.CollUtil;
import com.scratch.common.core.utils.core.ObjectUtil;
import com.scratch.common.core.utils.core.SpringUtil;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.redis.constant.RedisConstants;
import com.scratch.common.security.utils.SecurityUserUtils;
import com.scratch.common.security.utils.SecurityUtils;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.system.api.dict.domain.dto.SysDictDataDto;
import com.scratch.system.api.dict.domain.dto.SysDictTypeDto;
import com.scratch.system.api.dict.domain.po.SysDictDataPo;
import com.scratch.system.api.dict.domain.query.SysDictDataQuery;
import com.scratch.system.dict.domain.correlate.SysDictDataCorrelate;
import com.scratch.system.dict.manager.ISysDictDataManager;
import com.scratch.system.dict.service.ISysDictDataService;
import com.scratch.system.dict.service.ISysDictTypeService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统服务 | 字典模块 | 字典数据管理 业务层处理
 *
 * @author scratch
 */
@Service
public class SysDictDataServiceImpl extends BaseServiceImpl<SysDictDataQuery, SysDictDataDto, SysDictDataCorrelate, ISysDictDataManager> implements ISysDictDataService {

    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.SYS_DICT_KEY;
    }

    /**
     * 单条操作 - 开始处理
     *
     * @param operate 服务层 - 操作类型
     * @param newDto  新数据对象（删除时不存在）
     * @param id      Id集合（非删除时不存在）
     */
    @Override
    protected SysDictDataDto startHandle(OperateConstants.ServiceType operate, SysDictDataDto newDto, Serializable id) {
        SecurityContextHolder.setTenantIgnore();
        SysDictDataDto originDto = super.startHandle(operate, newDto, id);
        SecurityContextHolder.clearTenantIgnore();
        switch (operate) {
            case ADD, EDIT, EDIT_STATUS -> {
                if (ObjectUtil.isNotNull(newDto.getDictTypeId())) {
                    SysDictTypeDto dictType = SpringUtil.getBean(ISysDictTypeService.class).selectByIdIgnore(newDto.getDictTypeId());
                    newDto.setDictTypeInfo(dictType);
                }
            }
        }

        switch (operate) {
            case EDIT, EDIT_STATUS -> {
                if (ObjectUtil.notEqual(originDto.getTenantId(), SecurityUtils.getEnterpriseId())) {
                    if (SecurityUserUtils.isAdminTenant()) {
                        SecurityContextHolder.setEnterpriseId(Objects.isNull(newDto.getTenantId())? SecurityConstants.COMMON_TENANT_ID.toString():newDto.getTenantId().toString());
                    } else {
                        throw new ServiceException("修改失败，无权限！");
                    }
                }
            }
            case ADD -> {
                if (ObjectUtil.notEqual(newDto.getDictTypeInfo().getTenantId(), SecurityUtils.getEnterpriseId())) {
                    if (SecurityUserUtils.isAdminTenant()) {
                        SecurityContextHolder.setEnterpriseId(newDto.getDictTypeInfo().getTenantId().toString());
                    } else {
                        throw new ServiceException("新增失败，无权限！");
                    }
                }
            }
            case DELETE -> {
                if (SecurityUserUtils.isAdminTenant()) {
                    SecurityContextHolder.setTenantIgnore();
                }
            }
        }
        return originDto;
    }

    /**
     * 单条操作 - 结束处理
     *
     * @param operate   服务层 - 操作类型
     * @param row       操作数据条数
     * @param originDto 源数据对象（新增时不存在）
     * @param newDto    新数据对象（删除时不存在）
     */
    @Override
    protected void endHandle(OperateConstants.ServiceType operate, int row, SysDictDataDto originDto, SysDictDataDto newDto) {
        switch (operate) {
            case DELETE -> {
                if (SecurityUserUtils.isAdminTenant()) {
                    SecurityContextHolder.clearTenantIgnore();
                }
            }
            case ADD, EDIT, EDIT_STATUS -> SecurityContextHolder.rollLastEnterpriseId();
        }
        super.endHandle(operate, row, originDto, newDto);
    }

    /**
     * 批量操作 - 开始处理
     *
     * @param operate 服务层 - 操作类型
     * @param newList 新数据对象集合（删除时不存在）
     * @param idList  Id集合（非删除时不存在）
     */
    @Override
    protected List<SysDictDataDto> startBatchHandle(OperateConstants.ServiceType operate, Collection<SysDictDataDto> newList, Collection<? extends Serializable> idList) {
        List<SysDictDataDto> originList = super.startBatchHandle(operate, newList, idList);
        if (operate == OperateConstants.ServiceType.BATCH_DELETE) {
            if (SecurityUserUtils.isAdminTenant()) {
                SecurityContextHolder.setTenantIgnore();
            }
        }
        return originList;
    }

    /**
     * 批量操作 - 结束处理
     *
     * @param operate    服务层 - 操作类型
     * @param rows       操作数据条数
     * @param originList 源数据对象集合（新增时不存在）
     * @param newList    新数据对象集合（删除时不存在）
     */
    @Override
    protected void endBatchHandle(OperateConstants.ServiceType operate, int rows, Collection<SysDictDataDto> originList, Collection<SysDictDataDto> newList) {
        if (operate == OperateConstants.ServiceType.BATCH_DELETE) {
            if (SecurityUserUtils.isAdminTenant()) {
                SecurityContextHolder.clearTenantIgnore();
            }
        }
        super.endBatchHandle(operate, rows, originList, newList);
    }

    /**
     * 缓存更新
     *
     * @param operate      服务层 - 操作类型
     * @param operateCache 缓存操作类型
     * @param dto          数据对象
     * @param dtoList      数据对象集合
     */
    @Override
    public void refreshCache(OperateConstants.ServiceType operate, RedisConstants.OperateType operateCache, SysDictDataDto dto, Collection<SysDictDataDto> dtoList) {
        String cacheKey = StrUtil.format(getCacheKey().getCode(), SecurityUtils.getEnterpriseId());
        if (operate.isSingle()) {
            List<SysDictDataDto> dictList = baseManager.selectListByCode(dto.getCode());
            redisService.refreshMapValueCache(cacheKey, dto::getCode, () -> dictList);
        } else if (operate.isBatch()) {
            Set<String> codes = dtoList.stream().map(SysDictDataPo::getCode).collect(Collectors.toSet());
            if (CollUtil.isEmpty(codes)) {
                return;
            }
            List<SysDictDataDto> dictList = baseManager.selectListByCodes(codes);
            Map<String, List<SysDictDataDto>> dictMap = dictList.stream().collect(Collectors.groupingBy(SysDictDataDto::getCode));
            codes.forEach(item -> redisService.refreshMapValueCache(cacheKey, () -> item, () -> dictMap.get(item)));
        }
    }
}