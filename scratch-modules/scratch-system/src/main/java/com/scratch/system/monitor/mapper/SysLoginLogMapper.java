package com.scratch.system.monitor.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.system.api.log.domain.dto.SysLoginLogDto;
import com.scratch.system.api.log.domain.po.SysLoginLogPo;
import com.scratch.system.api.log.domain.query.SysLoginLogQuery;

/**
 * 系统服务 | 监控模块 | 访问日志管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysLoginLogMapper extends BaseMapper<SysLoginLogQuery, SysLoginLogDto, SysLoginLogPo> {
}
