package com.scratch.system.notice.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.system.notice.domain.dto.SysNoticeDto;
import com.scratch.system.notice.domain.query.SysNoticeQuery;

/**
 * 系统服务 | 消息模块 | 通知公告管理 数据封装层
 *
 * @author scratch
 */
public interface ISysNoticeManager extends IBaseManager<SysNoticeQuery, SysNoticeDto> {
}
