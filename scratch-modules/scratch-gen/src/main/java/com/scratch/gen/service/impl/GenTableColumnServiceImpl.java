package com.scratch.gen.service.impl;

import com.scratch.common.core.constant.basic.TenantConstants;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.gen.domain.correlate.GenTableColumnCorrelate;
import com.scratch.gen.domain.dto.GenTableColumnDto;
import com.scratch.gen.domain.query.GenTableColumnQuery;
import com.scratch.gen.manager.impl.GenTableColumnManagerImpl;
import com.scratch.gen.service.IGenTableColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务字段管理 服务层实现
 *
 * @author scratch
 */
@Service
public class GenTableColumnServiceImpl extends BaseServiceImpl<GenTableColumnQuery, GenTableColumnDto, GenTableColumnCorrelate, GenTableColumnManagerImpl> implements IGenTableColumnService {

    /**
     * 根据表名称查询数据库表列信息
     *
     * @param tableName  表名称
     * @param sourceName 数据源
     * @return 数据库表列信息
     */
    @Override
    public List<GenTableColumnDto> selectDbTableColumnsByName(String tableName, String sourceName) {
        return StrUtil.isNotBlank(sourceName) && StrUtil.notEquals(TenantConstants.Source.MASTER.getCode(), sourceName)
                ? baseManager.selectDbTableColumnsByName(tableName, sourceName)
                : baseManager.selectDbTableColumnsByName(tableName);
    }
}