package com.scratch.gen.domain.query;

import com.scratch.gen.domain.po.GenTableColumnPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 业务字段 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GenTableColumnQuery extends GenTableColumnPo {

    @Serial
    private static final long serialVersionUID = 1L;

}