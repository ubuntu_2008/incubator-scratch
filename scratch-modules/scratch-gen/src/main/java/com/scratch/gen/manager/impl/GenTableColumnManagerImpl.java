package com.scratch.gen.manager.impl;

import com.scratch.common.core.context.SecurityContextHolder;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.gen.domain.dto.GenTableColumnDto;
import com.scratch.gen.domain.model.GenTableColumnConverter;
import com.scratch.gen.domain.po.GenTableColumnPo;
import com.scratch.gen.domain.query.GenTableColumnQuery;
import com.scratch.gen.manager.IGenTableColumnManager;
import com.scratch.gen.mapper.GenTableColumnMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 业务字段管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class GenTableColumnManagerImpl extends BaseManagerImpl<GenTableColumnQuery, GenTableColumnDto, GenTableColumnPo, GenTableColumnMapper, GenTableColumnConverter> implements IGenTableColumnManager {

    /**
     * 根据表名称查询数据库表列信息 | 主库
     *
     * @param tableName 表名称
     * @return 数据库表列信息
     */
    @Master
    @Override
    public List<GenTableColumnDto> selectDbTableColumnsByName(String tableName) {
        return selectDbTableColumnsByName(tableName, null);
    }

    /**
     * 根据表名称查询数据库表列信息 | 租户库
     *
     * @param tableName  表名称
     * @param sourceName 数据源
     * @return 数据库表列信息
     */
    @Isolate
    @Override
    public List<GenTableColumnDto> selectDbTableColumnsByName(String tableName, String sourceName) {
        if (StrUtil.isNotBlank(sourceName)) {
            SecurityContextHolder.setSourceName(sourceName);
        }
        List<GenTableColumnDto> list = baseMapper.selectDbTableColumnsByName(tableName);
        if (StrUtil.isNotBlank(sourceName)) {
            SecurityContextHolder.rollLastSourceName();
        }
        return list;
    }
}
