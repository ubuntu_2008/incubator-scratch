package com.scratch.gen.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.gen.domain.dto.GenTableDto;
import com.scratch.gen.domain.po.GenTablePo;
import com.scratch.gen.domain.query.GenTableQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 业务 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface GenTableConverter extends BaseConverter<GenTableQuery, GenTableDto, GenTablePo> {
}