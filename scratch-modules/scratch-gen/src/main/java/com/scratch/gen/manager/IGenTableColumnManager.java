package com.scratch.gen.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.gen.domain.dto.GenTableColumnDto;
import com.scratch.gen.domain.query.GenTableColumnQuery;

import java.util.List;

/**
 * 业务字段管理 数据封装层
 *
 * @author scratch
 */
public interface IGenTableColumnManager extends IBaseManager<GenTableColumnQuery, GenTableColumnDto> {

    /**
     * 根据表名称查询数据库表列信息 | 主库
     *
     * @param tableName 表名称
     * @return 数据库表列信息
     */
    List<GenTableColumnDto> selectDbTableColumnsByName(String tableName);

    /**
     * 根据表名称查询数据库表列信息 | 租户库
     *
     * @param tableName  表名称
     * @param sourceName 数据源
     * @return 数据库表列信息
     */
    List<GenTableColumnDto> selectDbTableColumnsByName(String tableName, String sourceName);
}
