package com.scratch.gen.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.gen.domain.dto.GenTableColumnDto;
import com.scratch.gen.domain.po.GenTableColumnPo;
import com.scratch.gen.domain.query.GenTableColumnQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 业务字段 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface GenTableColumnConverter extends BaseConverter<GenTableColumnQuery, GenTableColumnDto, GenTableColumnPo> {
}