package com.scratch.gen.manager.impl;

import com.scratch.common.core.context.SecurityContextHolder;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.gen.domain.dto.GenTableDto;
import com.scratch.gen.domain.model.GenTableConverter;
import com.scratch.gen.domain.po.GenTablePo;
import com.scratch.gen.domain.query.GenTableQuery;
import com.scratch.gen.manager.IGenTableManager;
import com.scratch.gen.mapper.GenTableMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 业务管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class GenTableManagerImpl extends BaseManagerImpl<GenTableQuery, GenTableDto, GenTablePo, GenTableMapper, GenTableConverter> implements IGenTableManager {

    /**
     * 查询数据库列表
     *
     * @param table 业务对象
     * @return 数据库表集合
     */
    @Master
    @Override
    public List<GenTableDto> selectDbTableList(GenTableQuery table) {
        return selectDbTableList(table, null);
    }

    /**
     * 查询数据库列表
     *
     * @param table      业务对象
     * @param sourceName 数据源
     * @return 数据库表集合
     */
    @Isolate
    @Override
    public List<GenTableDto> selectDbTableList(GenTableQuery table, String sourceName) {
        if (StrUtil.isNotBlank(sourceName)) {
            SecurityContextHolder.setSourceName(sourceName);
        }
        List<GenTableDto> list = baseMapper.selectDbTableList(table);
        if (StrUtil.isNotBlank(sourceName)) {
            SecurityContextHolder.rollLastSourceName();
        }
        return list;
    }

    /**
     * 根据表名称组查询数据库列表 | 主库
     *
     * @param names 表名称组
     * @return 数据库表集合
     */
    @Master
    @Override
    public List<GenTableDto> selectDbTableListByNames(String[] names) {
        return selectDbTableListByNames(names, null);
    }

    /**
     * 根据表名称组查询数据库列表 | 租户库
     *
     * @param names      表名称组
     * @param sourceName 数据源
     * @return 数据库表集合
     */
    @Isolate
    @Override
    public List<GenTableDto> selectDbTableListByNames(String[] names, String sourceName) {
        if (StrUtil.isNotBlank(sourceName)) {
            SecurityContextHolder.setSourceName(sourceName);
        }
        List<GenTableDto> list = baseMapper.selectDbTableListByNames(names);
        if (StrUtil.isNotBlank(sourceName)) {
            SecurityContextHolder.rollLastSourceName();
        }
        return list;
    }
}
