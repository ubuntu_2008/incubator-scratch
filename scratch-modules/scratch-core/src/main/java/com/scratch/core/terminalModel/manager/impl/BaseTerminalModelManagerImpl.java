package com.scratch.core.terminalModel.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.core.api.terminalModel.domain.po.BaseTerminalModelPo;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.core.terminalModel.domain.model.BaseTerminalModelConverter;
import com.scratch.core.terminalModel.mapper.BaseTerminalModelMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.terminalModel.manager.IBaseTerminalModelManager;
import org.springframework.stereotype.Component;

/**
 * 终端型号管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseTerminalModelManagerImpl extends BaseManagerImpl<BaseTerminalModelQuery, BaseTerminalModelDto, BaseTerminalModelPo, BaseTerminalModelMapper, BaseTerminalModelConverter> implements IBaseTerminalModelManager {
    @Override
    public int hp(Long id, String hp) {
        return baseMapper.update(null,
                Wrappers.<BaseTerminalModelPo>update().lambda()
                        .set(BaseTerminalModelPo::getHp, hp)
                        .eq(BaseTerminalModelPo::getId, id));
    }
}