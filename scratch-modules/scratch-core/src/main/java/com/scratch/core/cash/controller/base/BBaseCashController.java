package com.scratch.core.cash.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.core.cash.service.IBaseCashService;

/**
 * 兑奖管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseCashController extends BaseController<BaseCashQuery, BaseCashDto, IBaseCashService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "兑奖" ;
    }
}
