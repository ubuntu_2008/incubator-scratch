package com.scratch.core.site.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.api.site.domain.po.BaseSitePo;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 网点 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseSiteConverter extends BaseConverter<BaseSiteQuery, BaseSiteDto, BaseSitePo> {
}
