package com.scratch.core.ticket.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.api.ticket.domain.po.BaseTicketPo;
import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 票信息管理 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseTicketConverter extends BaseConverter<BaseTicketQuery, BaseTicketDto, BaseTicketPo> {
}
