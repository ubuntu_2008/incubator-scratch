package com.scratch.core.area.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.query.SysAreaQuery;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.area.controller.base.BSysAreaController;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 区域管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/area")
public class ASysAreaController extends BSysAreaController {

    /**
     * 查询区域列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:area:list')")
    public AjaxResult list(SysAreaQuery area) {
        return super.list(area);
    }

    /**
     * 查询区域详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:area:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 区域新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:area:add')")
    @Log(title = "区域管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody SysAreaDto area) {
        return super.add(area);
    }

    /**
     * 区域修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:area:edit')")
    @Log(title = "区域管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody SysAreaDto area) {
        return super.edit(area);
    }

    /**
     * 区域修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:area:edit', 'core:area:es')")
    @Log(title = "区域管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody SysAreaDto area) {
        return super.editStatus(area);
    }

    /**
     * 区域批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:area:delete')")
    @Log(title = "区域管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
