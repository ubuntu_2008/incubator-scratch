package com.scratch.core.terminalModel.service;

import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.common.web.entity.service.IBaseService;

import java.util.Map;

/**
 * 终端型号管理 服务层
 *
 * @author scratch
 */
public interface IBaseTerminalModelService extends IBaseService<BaseTerminalModelQuery, BaseTerminalModelDto> {
    int hp(BaseTerminalModelDto terminalModel);



}