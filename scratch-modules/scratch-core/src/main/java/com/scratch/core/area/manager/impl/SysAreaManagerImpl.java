package com.scratch.core.area.manager.impl;

import com.scratch.core.area.domain.po.SysAreaPo;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.core.area.domain.model.SysAreaConverter;
import com.scratch.core.area.mapper.SysAreaMapper;
import com.scratch.common.web.entity.manager.impl.TreeManagerImpl;
import com.scratch.core.area.manager.ISysAreaManager;
import org.springframework.stereotype.Component;

/**
 * 区域管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysAreaManagerImpl extends TreeManagerImpl<SysAreaQuery, SysAreaDto, SysAreaPo, SysAreaMapper, SysAreaConverter> implements ISysAreaManager {
}