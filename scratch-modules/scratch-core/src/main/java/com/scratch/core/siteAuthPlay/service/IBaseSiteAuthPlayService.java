package com.scratch.core.siteAuthPlay.service;

import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.common.web.entity.service.IBaseService;

import java.util.List;

/**
 * 站点玩法授权管理 服务层
 *
 * @author scratch
 */
public interface IBaseSiteAuthPlayService extends IBaseService<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto> {
    int authPlay(Long baseSiteId,Long baseTicketId, String authSell, String bkgeSell,String authCash,String bkgeCash);

    List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(String siteId);
}