package com.scratch.core.site.service;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.common.web.entity.service.IBaseService;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;

import java.util.List;

/**
 * 网点管理 服务层
 *
 * @author scratch
 */
public interface IBaseSiteService extends IBaseService<BaseSiteQuery, BaseSiteDto> {
    BaseSiteDto getSiteNo();

    void addAfter(AjaxResult ajaxResult);

    int authBus(BaseSiteDto site);

    int authPlay(BaseSiteAuthPlayDto baseSiteAuthPlayDto);

    List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(String siteId);


}