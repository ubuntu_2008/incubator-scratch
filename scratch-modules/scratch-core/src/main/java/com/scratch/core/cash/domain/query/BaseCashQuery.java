package com.scratch.core.cash.domain.query;

import com.scratch.core.api.cash.domain.po.BaseCashPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 兑奖 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseCashQuery extends BaseCashPo {

    @Serial
    private static final long serialVersionUID = 1L;
}