package com.scratch.core.cp.mapper;

import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.api.cp.domain.po.BaseCpPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 信息管理管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseCpMapper extends BaseMapper<BaseCpQuery, BaseCpDto, BaseCpPo> {
}