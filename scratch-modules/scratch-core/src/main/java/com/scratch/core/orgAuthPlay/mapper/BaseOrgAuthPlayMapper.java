package com.scratch.core.orgAuthPlay.mapper;

import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.core.api.orgAuthPlay.domain.po.BaseOrgAuthPlayPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 机构玩法授权管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseOrgAuthPlayMapper extends BaseMapper<BaseOrgAuthPlayQuery, BaseOrgAuthPlayDto, BaseOrgAuthPlayPo> {
    @Select("SELECT a.id AS baseTicketId,a.img,a.play_name AS playName ,a.play_no AS playNo,IF(b.auth_sell=1 OR b.auth_sell IS NULL,'',0)  AS authSell,IF(b.auth_cash=1 OR b.auth_cash IS NULL,'',0) AS authCash FROM base_ticket a\n" +
            "LEFT JOIN   `base_org_auth_play` b ON\n" +
            "a.id=b.base_ticket_id  AND\n" +
            "b.base_org_id=#{orgId}  \n" +
            "WHERE  a.del_flag=0 ORDER BY a.create_time DESC")
    List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(@Param("orgId") String orgId);
}