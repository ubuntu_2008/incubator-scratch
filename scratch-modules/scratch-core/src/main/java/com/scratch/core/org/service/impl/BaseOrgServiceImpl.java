package com.scratch.core.org.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.OperateConstants;
import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.redis.constant.RedisConstants;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.core.org.domain.correlate.BaseOrgCorrelate;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.core.org.manager.IBaseOrgManager;
import com.scratch.core.org.service.IBaseOrgService;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import com.scratch.core.orgAuthPlay.service.IBaseOrgAuthPlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 信息管理管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseOrgServiceImpl extends BaseServiceImpl<BaseOrgQuery, BaseOrgDto, BaseOrgCorrelate, IBaseOrgManager> implements IBaseOrgService {

    @Autowired
    private IBaseOrgManager baseOrgManager;

    @Autowired
    private IBaseOrgAuthPlayService baseOrgAuthPlayService;

    private final static String ORG_NO_B = "ORG";
    private final static Long ORG_NO_C = 10001L;

    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.ORG;
    }


    /**
     * 查询信息管理对象列表 | 数据权限
     *
     * @param org 信息管理对象
     * @return 信息管理对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseOrgMapper"})
    public List<BaseOrgDto> selectListScope(BaseOrgQuery org) {
        return super.selectListScope(org);
    }

    @Override
    public BaseOrgDto getOrgNo() {
        String orgNo = redisService.hasKey(RedisConstants.BaseKey.BASE_ORG_NO.getCode()) ?
                ORG_NO_B + (ORG_NO_C + ((Integer) redisService.getCacheObject(RedisConstants.BaseKey.BASE_ORG_NO.getCode())))
                : ORG_NO_B + ORG_NO_C;
        BaseOrgDto baseOrgDto = new BaseOrgDto();
        baseOrgDto.setOrgNo(orgNo);
        return baseOrgDto;
    }

    @Override
    public void addAfter(AjaxResult ajaxResult) {
        if (ajaxResult.isSuccess()) {
            redisService.redisTemplate.opsForValue().increment(RedisConstants.BaseKey.BASE_ORG_NO.getCode(), 1);
        }
    }


    @Override
    public int authBus(BaseOrgDto org) {
        BaseOrgDto originDto = startHandle(OperateConstants.ServiceType.EDIT, org, null);
        int row = baseOrgManager.authBus(org.getId(), org.getAuthBus());
        BaseOrgDto newOrg = originDto;
        newOrg.setAuthBus(org.getAuthBus());
        endHandle(OperateConstants.ServiceType.EDIT, row, originDto, newOrg);
        return row;
    }

    @Override
    public int authPlay(BaseOrgAuthPlayDto baseOrgAuthPlayDto) {
        BaseOrgAuthPlayQuery baseOrgAuthPlayQuery = new BaseOrgAuthPlayQuery();
        baseOrgAuthPlayQuery.setBaseOrgId(baseOrgAuthPlayDto.getBaseOrgId());
        baseOrgAuthPlayQuery.setBaseTicketId(baseOrgAuthPlayDto.getBaseTicketId());
        List<BaseOrgAuthPlayDto> list = baseOrgAuthPlayService.selectList(baseOrgAuthPlayQuery);
        if (list.size() > 0) {
            baseOrgAuthPlayDto.setId(list.get(0).getId());
            return baseOrgAuthPlayService.update(baseOrgAuthPlayDto);
        } else {
            return baseOrgAuthPlayService.insert(baseOrgAuthPlayDto);
        }
    }


    @Override
    public List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(String orgId) {
        return baseOrgAuthPlayService.listOrgAuthPlay(orgId);
    }
}