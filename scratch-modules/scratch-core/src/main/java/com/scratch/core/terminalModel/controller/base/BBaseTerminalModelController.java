package com.scratch.core.terminalModel.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.core.terminalModel.service.IBaseTerminalModelService;

/**
 * 终端型号管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseTerminalModelController extends BaseController<BaseTerminalModelQuery, BaseTerminalModelDto, IBaseTerminalModelService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "终端型号" ;
    }
}
