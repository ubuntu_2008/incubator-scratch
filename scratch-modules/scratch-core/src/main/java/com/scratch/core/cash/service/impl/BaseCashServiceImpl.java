package com.scratch.core.cash.service.impl;

import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.core.cash.domain.correlate.BaseCashCorrelate;
import com.scratch.core.cash.service.IBaseCashService;
import com.scratch.core.cash.manager.IBaseCashManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 兑奖管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseCashServiceImpl extends BaseServiceImpl<BaseCashQuery, BaseCashDto, BaseCashCorrelate, IBaseCashManager> implements IBaseCashService {

    /**
     * 查询兑奖对象列表 | 数据权限
     *
     * @param cash 兑奖对象
     * @return 兑奖对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseCashMapper"})
    public List<BaseCashDto> selectListScope(BaseCashQuery cash) {
        return super.selectListScope(cash);
    }

}