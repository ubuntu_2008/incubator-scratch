package com.scratch.core.cpAuthPlay.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.api.cpAuthPlay.domain.po.BaseCpAuthPlayPo;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 渠道玩法授权 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseCpAuthPlayConverter extends BaseConverter<BaseCpAuthPlayQuery, BaseCpAuthPlayDto, BaseCpAuthPlayPo> {
}
