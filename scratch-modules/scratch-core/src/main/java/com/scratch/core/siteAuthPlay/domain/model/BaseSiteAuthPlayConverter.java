package com.scratch.core.siteAuthPlay.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.api.siteAuthPlay.domain.po.BaseSiteAuthPlayPo;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 站点玩法授权 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseSiteAuthPlayConverter extends BaseConverter<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto, BaseSiteAuthPlayPo> {
}
