package com.scratch.core.area.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.area.controller.base.BSysAreaController;

/**
 * 区域管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/area")
public class ISysAreaController extends BSysAreaController {
}
