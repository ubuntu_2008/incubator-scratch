package com.scratch.core.order.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.order.controller.base.BBaseOrderController;

/**
 * 订单管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/order")
public class IBaseOrderController extends BBaseOrderController {
}
