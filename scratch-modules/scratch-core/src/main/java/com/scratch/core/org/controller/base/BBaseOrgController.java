package com.scratch.core.org.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.core.org.service.IBaseOrgService;

/**
 * 信息管理管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseOrgController extends BaseController<BaseOrgQuery, BaseOrgDto, IBaseOrgService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "信息管理" ;
    }
}
