package com.scratch.core.ticket.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.ticket.controller.base.BBaseTicketController;

/**
 * 票信息管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/ticket")
public class IBaseTicketController extends BBaseTicketController {
}
