package com.scratch.core.order.manager;

import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 订单管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseOrderManager extends IBaseManager<BaseOrderQuery, BaseOrderDto> {
}