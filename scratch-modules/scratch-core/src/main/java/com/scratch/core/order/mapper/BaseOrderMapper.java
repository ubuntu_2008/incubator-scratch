package com.scratch.core.order.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.core.api.order.domain.po.BaseOrderPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Master;

/**
 * 订单管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseOrderMapper extends BaseMapper<BaseOrderQuery, BaseOrderDto, BaseOrderPo> {
}