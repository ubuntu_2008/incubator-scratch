package com.scratch.core.org.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.org.domain.model.BaseOrgConverter;
import com.scratch.core.api.org.domain.po.BaseOrgPo;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.core.org.manager.IBaseOrgManager;
import com.scratch.core.org.mapper.BaseOrgMapper;
import org.springframework.stereotype.Component;

/**
 * 信息管理管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseOrgManagerImpl extends BaseManagerImpl<BaseOrgQuery, BaseOrgDto, BaseOrgPo, BaseOrgMapper, BaseOrgConverter> implements IBaseOrgManager {

    @Override
    public int authBus(Long id, String authBus) {
        return baseMapper.update(null,
                Wrappers.<BaseOrgPo>update().lambda()
                        .set(BaseOrgPo::getAuthBus, authBus)
                        .eq(BaseOrgPo::getId, id));
    }

}