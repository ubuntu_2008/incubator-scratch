package com.scratch.core.terminalModel.mapper;

import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.api.terminalModel.domain.po.BaseTerminalModelPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 终端型号管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseTerminalModelMapper extends BaseMapper<BaseTerminalModelQuery, BaseTerminalModelDto, BaseTerminalModelPo> {
}