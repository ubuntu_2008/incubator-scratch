package com.scratch.core.orgAuthPlay.manager;

import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

import java.util.List;

/**
 * 机构玩法授权管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseOrgAuthPlayManager extends IBaseManager<BaseOrgAuthPlayQuery, BaseOrgAuthPlayDto> {

    @Deprecated
    int authPlay(Long baseOrgId,Long baseTicketId,String authBus,String authPlay);

    List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(String orgId);

}