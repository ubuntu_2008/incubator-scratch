package com.scratch.core.ticket.service;

import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.common.web.entity.service.IBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * 票信息管理 服务层
 *
 * @author scratch
 */
public interface IBaseTicketService extends IBaseService<BaseTicketQuery, BaseTicketDto> {


    BaseTicketDto getInfoTicket(Serializable id);

    Boolean addTicket(BaseTicketDto ticket);

    Boolean editTicket(BaseTicketDto ticket);


    int batchRemoveTicket(List<Long> idList);


}