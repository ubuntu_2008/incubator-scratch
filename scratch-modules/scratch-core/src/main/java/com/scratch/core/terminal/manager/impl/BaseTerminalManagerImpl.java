package com.scratch.core.terminal.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.core.api.terminal.domain.po.BaseTerminalPo;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.core.terminal.domain.model.BaseTerminalConverter;
import com.scratch.core.terminal.mapper.BaseTerminalMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.terminal.manager.IBaseTerminalManager;
import org.springframework.stereotype.Component;

/**
 * 终端信息管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseTerminalManagerImpl extends BaseManagerImpl<BaseTerminalQuery, BaseTerminalDto, BaseTerminalPo, BaseTerminalMapper, BaseTerminalConverter> implements IBaseTerminalManager {
    @Override
    public int allocation(Long id, Long siteId) {
        return baseMapper.update(null,
                Wrappers.<BaseTerminalPo>update().lambda()
                        .set(BaseTerminalPo::getSiteId, siteId)
                        .eq(BaseTerminalPo::getId, id));
    }
}