package com.scratch.core.siteAuthPlay.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.siteAuthPlay.controller.base.BBaseSiteAuthPlayController;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 站点玩法授权管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/siteAuthPlay")
public class ABaseSiteAuthPlayController extends BBaseSiteAuthPlayController {

    /**
     * 查询站点玩法授权列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:siteAuthPlay:list')")
    public AjaxResult list(BaseSiteAuthPlayQuery siteAuthPlay) {
        return super.list(siteAuthPlay);
    }

    /**
     * 查询站点玩法授权详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:siteAuthPlay:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 站点玩法授权新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:siteAuthPlay:add')")
    @Log(title = "站点玩法授权管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseSiteAuthPlayDto siteAuthPlay) {
        return super.add(siteAuthPlay);
    }

    /**
     * 站点玩法授权修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:siteAuthPlay:edit')")
    @Log(title = "站点玩法授权管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseSiteAuthPlayDto siteAuthPlay) {
        return super.edit(siteAuthPlay);
    }

    /**
     * 站点玩法授权批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:siteAuthPlay:delete')")
    @Log(title = "站点玩法授权管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
