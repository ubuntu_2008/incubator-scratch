package com.scratch.core.cpAuthPlay.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.api.cpAuthPlay.domain.po.BaseCpAuthPlayPo;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.core.cpAuthPlay.domain.model.BaseCpAuthPlayConverter;
import com.scratch.core.cpAuthPlay.mapper.BaseCpAuthPlayMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.cpAuthPlay.manager.IBaseCpAuthPlayManager;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 渠道玩法授权管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseCpAuthPlayManagerImpl extends BaseManagerImpl<BaseCpAuthPlayQuery, BaseCpAuthPlayDto, BaseCpAuthPlayPo, BaseCpAuthPlayMapper, BaseCpAuthPlayConverter> implements IBaseCpAuthPlayManager {
    @Override
    public int authPlay(Long baseCpId, Long baseTicketId, String authSell, String authCash) {
        return baseMapper.update(null,
                Wrappers.<BaseCpAuthPlayPo>update().lambda()
                        .set(BaseCpAuthPlayPo::getAuthSell, authSell)
                        .set(BaseCpAuthPlayPo::getAuthCash, authCash)
                        .eq(BaseCpAuthPlayPo::getBaseCpId, baseCpId)
                        .eq(BaseCpAuthPlayPo::getBaseTicketId, baseTicketId));
    }

    @Override
    public List<BaseCpAuthPlayDataDto> listCpAuthPlay(String cpId) {
        return baseMapper.listCpAuthPlay(cpId);
    }


}