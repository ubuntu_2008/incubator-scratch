package com.scratch.core.level.domain.query;

import com.scratch.core.api.level.domain.po.BaseTicketAwardLevelPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 奖级 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTicketAwardLevelQuery extends BaseTicketAwardLevelPo {

    @Serial
    private static final long serialVersionUID = 1L;
}