package com.scratch.core.orgAuthPlay.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.core.api.orgAuthPlay.domain.po.BaseOrgAuthPlayPo;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 机构玩法授权 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseOrgAuthPlayConverter extends BaseConverter<BaseOrgAuthPlayQuery, BaseOrgAuthPlayDto, BaseOrgAuthPlayPo> {
}
