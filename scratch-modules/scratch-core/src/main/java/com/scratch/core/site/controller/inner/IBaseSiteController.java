package com.scratch.core.site.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.site.controller.base.BBaseSiteController;

/**
 * 网点管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/site")
public class IBaseSiteController extends BBaseSiteController {
}
