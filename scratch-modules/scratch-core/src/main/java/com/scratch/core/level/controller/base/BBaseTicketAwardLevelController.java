package com.scratch.core.level.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.core.level.service.IBaseTicketAwardLevelService;

/**
 * 奖级管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseTicketAwardLevelController extends BaseController<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto, IBaseTicketAwardLevelService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "奖级" ;
    }
}
