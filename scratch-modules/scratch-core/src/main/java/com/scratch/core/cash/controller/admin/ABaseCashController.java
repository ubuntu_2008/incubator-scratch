package com.scratch.core.cash.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.cash.controller.base.BBaseCashController;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * 兑奖管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/cash")
public class ABaseCashController extends BBaseCashController {

    /**
     * 查询兑奖列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:cash:list')")
    public AjaxResult list(BaseCashQuery cash) {
        return super.list(cash);
    }

    /**
     * 查询兑奖详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:cash:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }
}
