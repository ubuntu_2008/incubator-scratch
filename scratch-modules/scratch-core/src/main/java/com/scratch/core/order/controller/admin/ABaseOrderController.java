package com.scratch.core.order.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.order.controller.base.BBaseOrderController;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * 订单管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/order")
public class ABaseOrderController extends BBaseOrderController {

    /**
     * 查询订单列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:order:list')")
    public AjaxResult list(BaseOrderQuery order) {
        return super.list(order);
    }

    /**
     * 查询订单详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:order:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }
}
