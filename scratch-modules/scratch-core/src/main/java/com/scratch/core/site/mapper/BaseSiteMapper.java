package com.scratch.core.site.mapper;

import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.api.site.domain.po.BaseSitePo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 网点管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseSiteMapper extends BaseMapper<BaseSiteQuery, BaseSiteDto, BaseSitePo> {
}