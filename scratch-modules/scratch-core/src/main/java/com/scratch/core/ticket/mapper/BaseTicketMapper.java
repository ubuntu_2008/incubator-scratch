package com.scratch.core.ticket.mapper;

import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.api.ticket.domain.po.BaseTicketPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 票信息管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseTicketMapper extends BaseMapper<BaseTicketQuery, BaseTicketDto, BaseTicketPo> {
}