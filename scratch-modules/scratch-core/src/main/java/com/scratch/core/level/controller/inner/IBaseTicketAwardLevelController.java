package com.scratch.core.level.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.level.controller.base.BBaseTicketAwardLevelController;

/**
 * 奖级管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/level")
public class IBaseTicketAwardLevelController extends BBaseTicketAwardLevelController {
}
