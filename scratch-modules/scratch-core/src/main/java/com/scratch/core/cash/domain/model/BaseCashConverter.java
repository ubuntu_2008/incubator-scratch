package com.scratch.core.cash.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.api.cash.domain.po.BaseCashPo;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 兑奖 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseCashConverter extends BaseConverter<BaseCashQuery, BaseCashDto, BaseCashPo> {
}
