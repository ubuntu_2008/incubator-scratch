package com.scratch.core.area.manager;

import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.common.web.entity.manager.ITreeManager;

/**
 * 区域管理 数据封装层
 *
 * @author scratch
 */
public interface ISysAreaManager extends ITreeManager<SysAreaQuery, SysAreaDto> {
}