package com.scratch.core.ticket.manager;

import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 票信息管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseTicketManager extends IBaseManager<BaseTicketQuery, BaseTicketDto> {
}