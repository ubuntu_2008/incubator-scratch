package com.scratch.core.terminal.domain.query;

import com.scratch.core.api.terminal.domain.po.BaseTerminalPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 终端信息 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTerminalQuery extends BaseTerminalPo {

    @Serial
    private static final long serialVersionUID = 1L;
}