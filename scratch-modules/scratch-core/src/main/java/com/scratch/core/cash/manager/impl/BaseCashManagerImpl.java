package com.scratch.core.cash.manager.impl;

import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.api.cash.domain.po.BaseCashPo;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.core.cash.domain.model.BaseCashConverter;
import com.scratch.core.cash.manager.IBaseCashManager;
import com.scratch.core.cash.mapper.BaseCashMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import org.springframework.stereotype.Component;

/**
 * 兑奖管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseCashManagerImpl extends BaseManagerImpl<BaseCashQuery, BaseCashDto, BaseCashPo, BaseCashMapper, BaseCashConverter> implements IBaseCashManager {
}