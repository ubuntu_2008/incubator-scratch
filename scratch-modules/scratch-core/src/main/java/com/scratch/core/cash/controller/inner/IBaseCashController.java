package com.scratch.core.cash.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.cash.controller.base.BBaseCashController;

/**
 * 兑奖管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/cash")
public class IBaseCashController extends BBaseCashController {
}
