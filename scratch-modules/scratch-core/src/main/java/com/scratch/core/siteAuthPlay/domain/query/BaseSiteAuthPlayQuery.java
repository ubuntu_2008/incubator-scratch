package com.scratch.core.siteAuthPlay.domain.query;

import com.scratch.core.api.siteAuthPlay.domain.po.BaseSiteAuthPlayPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 站点玩法授权 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseSiteAuthPlayQuery extends BaseSiteAuthPlayPo {

    @Serial
    private static final long serialVersionUID = 1L;
}