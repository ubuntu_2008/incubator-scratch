package com.scratch.core.order.manager.impl;

import com.scratch.core.api.order.domain.po.BaseOrderPo;
import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.core.order.domain.model.BaseOrderConverter;
import com.scratch.core.order.mapper.BaseOrderMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.order.manager.IBaseOrderManager;
import org.springframework.stereotype.Component;

/**
 * 订单管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseOrderManagerImpl extends BaseManagerImpl<BaseOrderQuery, BaseOrderDto, BaseOrderPo, BaseOrderMapper, BaseOrderConverter> implements IBaseOrderManager {
}