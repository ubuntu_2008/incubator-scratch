package com.scratch.core.level.manager;

import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 奖级管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseTicketAwardLevelManager extends IBaseManager<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto> {
}