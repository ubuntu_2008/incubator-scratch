package com.scratch.core.siteAuthPlay.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.core.siteAuthPlay.domain.correlate.BaseSiteAuthPlayCorrelate;
import com.scratch.core.siteAuthPlay.service.IBaseSiteAuthPlayService;
import com.scratch.core.siteAuthPlay.manager.IBaseSiteAuthPlayManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 站点玩法授权管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseSiteAuthPlayServiceImpl extends BaseServiceImpl<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto, BaseSiteAuthPlayCorrelate, IBaseSiteAuthPlayManager> implements IBaseSiteAuthPlayService {


    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.SITE_AUTH_PLAY;
    }
    /**
     * 查询站点玩法授权对象列表 | 数据权限
     *
     * @param siteAuthPlay 站点玩法授权对象
     * @return 站点玩法授权对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseSiteAuthPlayMapper"})
    public List<BaseSiteAuthPlayDto> selectListScope(BaseSiteAuthPlayQuery siteAuthPlay) {
        return super.selectListScope(siteAuthPlay);
    }

    @Override
    public int authPlay(Long baseSiteId, Long baseTicketId, String authSell, String bkgeSell, String authCash, String bkgeCash) {
        return baseManager.authPlay(baseSiteId, baseTicketId, authSell, bkgeSell, authCash, bkgeCash);
    }

    @Override
    public List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(String siteId) {
        return baseManager.listSiteAuthPlay(siteId);
    }
}