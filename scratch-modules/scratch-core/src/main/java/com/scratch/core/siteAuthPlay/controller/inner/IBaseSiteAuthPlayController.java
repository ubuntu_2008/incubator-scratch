package com.scratch.core.siteAuthPlay.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.siteAuthPlay.controller.base.BBaseSiteAuthPlayController;

/**
 * 站点玩法授权管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/siteAuthPlay")
public class IBaseSiteAuthPlayController extends BBaseSiteAuthPlayController {
}
