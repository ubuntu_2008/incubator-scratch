package com.scratch.core.terminal.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.OperateConstants;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.redis.constant.RedisConstants;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.core.terminal.domain.correlate.BaseTerminalCorrelate;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.core.terminal.manager.IBaseTerminalManager;
import com.scratch.core.terminal.service.IBaseTerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 终端信息管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseTerminalServiceImpl extends BaseServiceImpl<BaseTerminalQuery, BaseTerminalDto, BaseTerminalCorrelate, IBaseTerminalManager> implements IBaseTerminalService {
    @Autowired
    private IBaseTerminalManager baseTerminalManager;


    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.TERMINAL;
    }


    /**
     * 查询终端信息对象列表 | 数据权限
     *
     * @param terminal 终端信息对象
     * @return 终端信息对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseTerminalMapper"})
    public List<BaseTerminalDto> selectListScope(BaseTerminalQuery terminal) {
        List<BaseTerminalDto> BaseTerminalDtoList = baseManager.selectList(terminal);
        startCorrelates(BaseTerminalCorrelate.BASE_LIST);
        List<BaseTerminalDto> list = subCorrelates(BaseTerminalDtoList);
        return list;
    }


    @Override
    public BaseTerminalDto selectById(Serializable id) {
        BaseTerminalDto baseTerminalDto = subCorrelates(baseManager.selectById(id), BaseTerminalCorrelate.BASE_LIST);
        return baseTerminalDto;
    }

    @Override
    public int allocation(BaseTerminalDto baseTerminalDto) {
        BaseTerminalDto originDto = startHandle(OperateConstants.ServiceType.EDIT, baseTerminalDto, null);
        int row = baseTerminalManager.allocation(baseTerminalDto.getId(), baseTerminalDto.getSiteId());
        BaseTerminalDto newTerminal = originDto;
        newTerminal.setSiteId(baseTerminalDto.getSiteId());
        endHandle(OperateConstants.ServiceType.EDIT, row, originDto, newTerminal);
        return row;
    }

    @Override
    public Map<String, Object> hpTicket(String devNo) {
        String baseTicketLoadKey = StrUtil.format(RedisConstants.BaseKey.BASE_TICKET_LOAD.getCode(), devNo);
        return redisService.getCacheMap(baseTicketLoadKey);
    }
}