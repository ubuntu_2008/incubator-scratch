package com.scratch.core.site.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.site.domain.model.BaseSiteConverter;
import com.scratch.core.api.site.domain.po.BaseSitePo;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.core.site.manager.IBaseSiteManager;
import com.scratch.core.site.mapper.BaseSiteMapper;
import org.springframework.stereotype.Component;

/**
 * 网点管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseSiteManagerImpl extends BaseManagerImpl<BaseSiteQuery, BaseSiteDto, BaseSitePo, BaseSiteMapper, BaseSiteConverter> implements IBaseSiteManager {
    @Override
    public int authBus(Long id, String authBus) {
        return baseMapper.update(null,
                Wrappers.<BaseSitePo>update().lambda()
                        .set(BaseSitePo::getAuthBus, authBus)
                        .eq(BaseSitePo::getId, id));
    }
}