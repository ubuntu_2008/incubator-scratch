package com.scratch.core.terminal.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.api.terminal.domain.po.BaseTerminalPo;
import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 终端信息 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseTerminalConverter extends BaseConverter<BaseTerminalQuery, BaseTerminalDto, BaseTerminalPo> {
}
