package com.scratch.core.order.service.impl;

import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.core.order.domain.correlate.BaseOrderCorrelate;
import com.scratch.core.order.service.IBaseOrderService;
import com.scratch.core.order.manager.IBaseOrderManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseOrderServiceImpl extends BaseServiceImpl<BaseOrderQuery, BaseOrderDto, BaseOrderCorrelate, IBaseOrderManager> implements IBaseOrderService {

    /**
     * 查询订单对象列表 | 数据权限
     *
     * @param order 订单对象
     * @return 订单对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseOrderMapper"})
    public List<BaseOrderDto> selectListScope(BaseOrderQuery order) {
        List<BaseOrderDto> BaseSiteDtoList = baseManager.selectList(order);
        startCorrelates(BaseOrderCorrelate.BASE_LIST);
        return subCorrelates(BaseSiteDtoList);
    }

}