package com.scratch.core.area.service.impl;

import com.scratch.core.area.domain.correlate.SysAreaCorrelate;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.core.area.manager.ISysAreaManager;
import com.scratch.core.area.service.ISysAreaService;
import com.scratch.common.web.entity.service.impl.TreeServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 区域管理 服务层处理
 *
 * @author scratch
 */
@Service
public class SysAreaServiceImpl extends TreeServiceImpl<SysAreaQuery, SysAreaDto, SysAreaCorrelate, ISysAreaManager> implements ISysAreaService {

    /**
     * 查询区域对象列表 | 数据权限
     *
     * @param area 区域对象
     * @return 区域对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"SysAreaMapper"})
    public List<SysAreaDto> selectListScope(SysAreaQuery area) {
        return super.selectListScope(area);
    }

}