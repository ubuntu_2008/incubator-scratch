package com.scratch.core.cpAuthPlay.service;

import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.common.web.entity.service.IBaseService;

import java.util.List;

/**
 * 渠道玩法授权管理 服务层
 *
 * @author scratch
 */
public interface IBaseCpAuthPlayService extends IBaseService<BaseCpAuthPlayQuery, BaseCpAuthPlayDto> {
    int authPlay(Long baseCpId,Long baseTicketId,String authBus,String authPlay);

    List<BaseCpAuthPlayDataDto> listCpAuthPlay(String cpId);
}