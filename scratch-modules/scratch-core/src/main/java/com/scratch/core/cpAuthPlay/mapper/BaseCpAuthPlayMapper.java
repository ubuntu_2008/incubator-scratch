package com.scratch.core.cpAuthPlay.mapper;

import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.api.cpAuthPlay.domain.po.BaseCpAuthPlayPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;

import java.util.List;

/**
 * 渠道玩法授权管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseCpAuthPlayMapper extends BaseMapper<BaseCpAuthPlayQuery, BaseCpAuthPlayDto, BaseCpAuthPlayPo> {
    @Select("SELECT a.id AS baseTicketId,a.img,a.play_name AS playName ,a.play_no AS playNo,IF(b.auth_sell=1 OR b.auth_sell IS NULL,'',0)  AS authSell,IF(b.auth_cash=1 OR b.auth_cash IS NULL,'',0) AS authCash FROM base_ticket a\n" +
            "LEFT JOIN   `base_cp_auth_play` b ON\n" +
            "a.id=b.base_ticket_id  AND\n" +
            "b.base_cp_id=#{cpId}  \n" +
            "WHERE  a.del_flag=0 ORDER BY a.create_time DESC")
    List<BaseCpAuthPlayDataDto> listCpAuthPlay(@Param("cpId") String cpId);
}