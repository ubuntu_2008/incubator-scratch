package com.scratch.core.org.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.org.controller.base.BBaseOrgController;

/**
 * 信息管理管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/org")
public class IBaseOrgController extends BBaseOrgController {
}
