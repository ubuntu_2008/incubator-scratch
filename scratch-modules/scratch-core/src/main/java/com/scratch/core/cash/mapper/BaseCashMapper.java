package com.scratch.core.cash.mapper;

import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.api.cash.domain.po.BaseCashPo;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 兑奖管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseCashMapper extends BaseMapper<BaseCashQuery, BaseCashDto, BaseCashPo> {
}