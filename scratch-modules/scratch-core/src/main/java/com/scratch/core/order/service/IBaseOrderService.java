package com.scratch.core.order.service;

import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.common.web.entity.service.IBaseService;

/**
 * 订单管理 服务层
 *
 * @author scratch
 */
public interface IBaseOrderService extends IBaseService<BaseOrderQuery, BaseOrderDto> {
}