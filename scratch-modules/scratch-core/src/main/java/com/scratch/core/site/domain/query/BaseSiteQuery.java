package com.scratch.core.site.domain.query;

import com.scratch.core.api.site.domain.po.BaseSitePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 网点 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseSiteQuery extends BaseSitePo {

    @Serial
    private static final long serialVersionUID = 1L;
}