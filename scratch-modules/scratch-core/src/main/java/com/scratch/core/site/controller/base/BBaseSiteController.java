package com.scratch.core.site.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.core.site.service.IBaseSiteService;

/**
 * 网点管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseSiteController extends BaseController<BaseSiteQuery, BaseSiteDto, IBaseSiteService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "网点" ;
    }
}
