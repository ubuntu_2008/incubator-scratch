package com.scratch.core.order.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.domain.Remote;
import com.scratch.common.web.correlate.service.CorrelateService;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import com.scratch.system.api.organize.feign.RemoteDeptService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.scratch.common.web.correlate.contant.CorrelateConstants.SubOperate.SELECT;

import com.scratch.core.api.order.domain.dto.BaseOrderDto;

/**
 * 订单 关联映射
 */
@Getter
@AllArgsConstructor
public enum BaseOrderCorrelate implements CorrelateService {

    BASE_LIST("默认列表|（归属区域）,(归属渠道)", new ArrayList<>() {{
        add(new Remote<>(SELECT, RemoteDeptService.class, BaseOrderDto::getAreaId, SysDeptDto::getId, BaseOrderDto::getDept));
    }});;

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}