package com.scratch.core.cpAuthPlay.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.core.cpAuthPlay.service.IBaseCpAuthPlayService;

/**
 * 渠道玩法授权管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseCpAuthPlayController extends BaseController<BaseCpAuthPlayQuery, BaseCpAuthPlayDto, IBaseCpAuthPlayService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "渠道玩法授权" ;
    }
}
