package com.scratch.core.terminal.controller.inner;

import com.scratch.common.core.web.result.R;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.terminal.controller.base.BBaseTerminalController;

/**
 * 终端信息管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/terminal")
public class IBaseTerminalController extends BBaseTerminalController {
    /**
     * 刷新终端信息缓存 | 内部调用
     */
    @Override
    @GetMapping("/refresh")
    @Log(title = "终端信息管理", businessType = BusinessType.REFRESH)
    public R<Boolean> refreshCacheInner() {
        return super.refreshCacheInner();
    }
}
