package com.scratch.core.cp.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cp.controller.base.BBaseCpController;
import com.scratch.core.cp.service.IBaseCpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 信息管理管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/cp")
public class ABaseCpController extends BBaseCpController {
    @Autowired
    private IBaseCpService baseCpService;

    /**
     * 获取渠道编号
     */
    @GetMapping(value = "/getCpNo")
    public AjaxResult getCpNo() {
        return success(baseCpService.getCpNo());
    }


    /**
     * 业务授权
     */
    @PutMapping("/authBus")
    @PreAuthorize("@ss.hasAuthority('core:cp:edit')")
    @Log(title = "渠道业务授权", businessType = BusinessType.UPDATE)
    public AjaxResult authBus(@RequestBody BaseCpDto org) {
        return toAjax(baseCpService.authBus(org));
    }

    @PostMapping("/authPlay")
    @PreAuthorize("@ss.hasAuthority('core:cp:edit')")
    @Log(title = "渠道玩法授权", businessType = BusinessType.UPDATE)
    public AjaxResult authPlay(@RequestBody BaseCpAuthPlayDto baseCpAuthPlayDto) {
        return toAjax(baseCpService.authPlay(baseCpAuthPlayDto));
    }


    @GetMapping("/listAuthPlay")
    @PreAuthorize("@ss.hasAuthority('core:cp:list')")
    public AjaxResult listAuthPlay(String cpId) {
        startPage();
        List<BaseCpAuthPlayDataDto> listAuthPlay = baseCpService.listCpAuthPlay(cpId);
        return getDataTable(listAuthPlay);
    }


    /**
     * 查询信息管理列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:cp:list')")
    public AjaxResult list(BaseCpQuery cp) {
        return super.list(cp);
    }

    /**
     * 查询信息管理详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:cp:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 信息管理新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:cp:add')")
    @Log(title = "信息管理管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseCpDto cp) {
        AjaxResult ajaxResult = super.add(cp);
        if (ajaxResult.isSuccess()) {
            baseCpService.addAfter(ajaxResult);
        }
        return ajaxResult;

    }

    /**
     * 信息管理修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:cp:edit')")
    @Log(title = "信息管理管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseCpDto cp) {
        return super.edit(cp);
    }

    /**
     * 信息管理修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:cp:edit', 'core:cp:es')")
    @Log(title = "信息管理管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseCpDto cp) {
        return super.editStatus(cp);
    }

    /**
     * 信息管理批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:cp:delete')")
    @Log(title = "信息管理管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
