package com.scratch.core.orgAuthPlay.domain.query;

import com.scratch.core.api.orgAuthPlay.domain.po.BaseOrgAuthPlayPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 机构玩法授权 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseOrgAuthPlayQuery extends BaseOrgAuthPlayPo {

    @Serial
    private static final long serialVersionUID = 1L;
}