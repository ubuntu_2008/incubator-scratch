package com.scratch.core.area.controller.base;

import com.scratch.common.web.entity.controller.TreeController;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.core.area.service.ISysAreaService;

/**
 * 区域管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BSysAreaController extends TreeController<SysAreaQuery, SysAreaDto, ISysAreaService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "区域" ;
    }
}
