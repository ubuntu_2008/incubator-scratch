package com.scratch.core.cp.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.core.cp.service.IBaseCpService;

/**
 * 信息管理管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseCpController extends BaseController<BaseCpQuery, BaseCpDto, IBaseCpService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "信息管理" ;
    }
}
