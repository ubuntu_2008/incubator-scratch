package com.scratch.core.cash.service;

import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.common.web.entity.service.IBaseService;

/**
 * 兑奖管理 服务层
 *
 * @author scratch
 */
public interface IBaseCashService extends IBaseService<BaseCashQuery, BaseCashDto> {
}