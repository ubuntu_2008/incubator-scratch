package com.scratch.core.cp.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.core.api.cp.domain.po.BaseCpPo;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.core.cp.domain.model.BaseCpConverter;
import com.scratch.core.cp.mapper.BaseCpMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.cp.manager.IBaseCpManager;
import org.springframework.stereotype.Component;

/**
 * 信息管理管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseCpManagerImpl extends BaseManagerImpl<BaseCpQuery, BaseCpDto, BaseCpPo, BaseCpMapper, BaseCpConverter> implements IBaseCpManager {
    @Override
    public int authBus(Long id, String authBus) {
        return baseMapper.update(null,
                Wrappers.<BaseCpPo>update().lambda()
                        .set(BaseCpPo::getAuthBus, authBus)
                        .eq(BaseCpPo::getId, id));
    }
}