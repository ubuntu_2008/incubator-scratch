package com.scratch.core.area.mapper;

import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.po.SysAreaPo;
import com.scratch.common.web.entity.mapper.TreeMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 区域管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysAreaMapper extends TreeMapper<SysAreaQuery, SysAreaDto, SysAreaPo> {
}