package com.scratch.core.area.domain.dto;

import com.scratch.core.area.domain.po.SysAreaPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 区域 数据传输对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysAreaDto extends SysAreaPo {

    @Serial
    private static final long serialVersionUID = 1L;

}
