package com.scratch.core.site.manager;

import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 网点管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseSiteManager extends IBaseManager<BaseSiteQuery, BaseSiteDto> {
    int authBus(Long id, String authBus);
}