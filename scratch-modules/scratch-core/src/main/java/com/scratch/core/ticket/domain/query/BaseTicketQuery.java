package com.scratch.core.ticket.domain.query;

import com.scratch.core.api.ticket.domain.po.BaseTicketPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 票信息管理 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTicketQuery extends BaseTicketPo {

    @Serial
    private static final long serialVersionUID = 1L;
}