package com.scratch.core.site.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.common.security.annotation.AdminAuth;
import com.scratch.core.site.controller.base.BBaseSiteController;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.core.site.service.IBaseSiteService;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 网点管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/site")
public class ABaseSiteController extends BBaseSiteController {

    @Autowired
    private IBaseSiteService baseSiteService;

    /**
     * 获取机构编号
     */
    @GetMapping(value = "/getSiteNo")
    public AjaxResult getSiteNo() {
        return success(baseSiteService.getSiteNo());
    }


    /**
     * 业务授权
     */
    @PutMapping("/authBus")
    @PreAuthorize("@ss.hasAuthority('core:site:edit')")
    @Log(title = "网点业务授权", businessType = BusinessType.UPDATE)
    public AjaxResult authBus(@RequestBody BaseSiteDto site) {
        return toAjax(baseSiteService.authBus(site));
    }

    @PostMapping("/authPlay")
    @PreAuthorize("@ss.hasAuthority('core:site:edit')")
    @Log(title = "网点玩法授权", businessType = BusinessType.UPDATE)
    public AjaxResult authPlay(@RequestBody BaseSiteAuthPlayDto baseSiteAuthPlayDto) {
        return toAjax(baseSiteService.authPlay(baseSiteAuthPlayDto));
    }


    @GetMapping("/listAuthPlay")
    @PreAuthorize("@ss.hasAuthority('core:site:list')")
    public AjaxResult listAuthPlay(String siteId) {
        startPage();
        List<BaseSiteAuthPlayDataDto> listAuthPlay = baseSiteService.listSiteAuthPlay(siteId);
        return getDataTable(listAuthPlay);
    }


    /**
     * 查询网点列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:site:list')")
    public AjaxResult list(BaseSiteQuery site) {

        return super.list(site);
    }

    /**
     * 查询网点详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:site:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 网点导出
     */
    @Override
    @PostMapping("/export")
    @PreAuthorize("@ss.hasAuthority('core:site:export')")
    @Log(title = "网点管理", businessType = BusinessType.EXPORT)
    public void export(HttpServletResponse response, BaseSiteQuery site) {
        super.export(response, site);
    }

    /**
     * 网点新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:site:add')")
    @Log(title = "网点管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseSiteDto site) {
        AjaxResult ajaxResult = super.add(site);
        if (ajaxResult.isSuccess()) {
            baseSiteService.addAfter(ajaxResult);
        }
        return ajaxResult;
    }

    /**
     * 网点修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:site:edit')")
    @Log(title = "网点管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseSiteDto site) {
        return super.edit(site);
    }

    /**
     * 网点修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:site:edit', 'core:site:es')")
    @Log(title = "网点管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseSiteDto site) {
        return super.editStatus(site);
    }

    /**
     * 网点批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:site:delete')")
    @Log(title = "网点管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
