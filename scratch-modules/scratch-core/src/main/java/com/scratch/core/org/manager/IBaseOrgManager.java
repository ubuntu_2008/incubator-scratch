package com.scratch.core.org.manager;

import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 信息管理管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseOrgManager extends IBaseManager<BaseOrgQuery, BaseOrgDto> {
    int authBus(Long id, String authBus);
}