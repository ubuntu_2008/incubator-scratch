package com.scratch.core.terminal.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.domain.Direct;
import com.scratch.common.web.correlate.service.CorrelateService;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.site.service.IBaseSiteService;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.terminalModel.service.IBaseTerminalModelService;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.scratch.common.web.correlate.contant.CorrelateConstants.SubOperate.SELECT;

/**
 * 终端信息 关联映射
 */
@Getter
@AllArgsConstructor
public enum BaseTerminalCorrelate implements CorrelateService {

    BASE_LIST("默认列表|（归属站点）", new ArrayList<>() {{
        add(new Direct<>(SELECT, IBaseSiteService.class, BaseTerminalDto::getSiteId, BaseSiteDto::getId, BaseTerminalDto::getSite));
        add(new Direct<>(SELECT, IBaseTerminalModelService.class, BaseTerminalDto::getModelId, BaseTerminalModelDto::getId, BaseTerminalDto::getModel));
    }});

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}