package com.scratch.core.site.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.OperateConstants;
import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.redis.constant.RedisConstants;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.core.site.domain.correlate.BaseSiteCorrelate;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.core.site.domain.query.BaseSiteQuery;
import com.scratch.core.site.manager.IBaseSiteManager;
import com.scratch.core.site.service.IBaseSiteService;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.core.siteAuthPlay.service.IBaseSiteAuthPlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 网点管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseSiteServiceImpl extends BaseServiceImpl<BaseSiteQuery, BaseSiteDto, BaseSiteCorrelate, IBaseSiteManager> implements IBaseSiteService {

    @Autowired
    private IBaseSiteManager baseSiteManager;

    @Autowired
    private IBaseSiteAuthPlayService baseSiteAuthPlayService;

    private final static String SITE_NO_B = "SITE";
    private final static Long SITE_NO_C = 10001L;

    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.SITE;
    }


    /**
     * 查询网点对象列表 | 数据权限
     *
     * @param site 网点对象
     * @return 网点对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseSiteMapper"})
    public List<BaseSiteDto> selectListScope(BaseSiteQuery site) {
        List<BaseSiteDto> BaseSiteDtoList = baseManager.selectList(site);
        startCorrelates(BaseSiteCorrelate.BASE_LIST);
        return subCorrelates(BaseSiteDtoList);
    }

    @Override
    public BaseSiteDto getSiteNo() {
        String siteNo = redisService.hasKey(RedisConstants.BaseKey.BASE_SITE_NO.getCode()) ?
                SITE_NO_B + (SITE_NO_C + ((Integer) redisService.getCacheObject(RedisConstants.BaseKey.BASE_SITE_NO.getCode())))
                : SITE_NO_B + SITE_NO_C;
        BaseSiteDto baseSiteDto = new BaseSiteDto();
        baseSiteDto.setSiteNo(siteNo);
        return baseSiteDto;
    }

    @Override
    public void addAfter(AjaxResult ajaxResult) {
        if (ajaxResult.isSuccess()) {
            redisService.redisTemplate.opsForValue().increment(RedisConstants.BaseKey.BASE_SITE_NO.getCode(), 1);
        }
    }


    @Override
    public int authBus(BaseSiteDto site) {
        BaseSiteDto originDto = startHandle(OperateConstants.ServiceType.EDIT, site, null);
        int row = baseSiteManager.authBus(site.getId(), site.getAuthBus());
        BaseSiteDto newSite = originDto;
        newSite.setAuthBus(site.getAuthBus());
        endHandle(OperateConstants.ServiceType.EDIT, row, originDto, newSite);
        return row;


    }

    @Override
    public int authPlay(BaseSiteAuthPlayDto baseSiteAuthPlayDto) {
        BaseSiteAuthPlayQuery baseSiteAuthPlayQuery = new BaseSiteAuthPlayQuery();
        baseSiteAuthPlayQuery.setBaseSiteId(baseSiteAuthPlayDto.getBaseSiteId());
        baseSiteAuthPlayQuery.setBaseTicketId(baseSiteAuthPlayDto.getBaseTicketId());
        List<BaseSiteAuthPlayDto> list = baseSiteAuthPlayService.selectList(baseSiteAuthPlayQuery);
        if (list.size() > 0) {
            baseSiteAuthPlayDto.setId(list.get(0).getId());
            return baseSiteAuthPlayService.update(baseSiteAuthPlayDto);
        } else {
            return baseSiteAuthPlayService.insert(baseSiteAuthPlayDto);
        }
    }


    @Override
    public List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(String siteId) {
        return baseSiteAuthPlayService.listSiteAuthPlay(siteId);
    }
}