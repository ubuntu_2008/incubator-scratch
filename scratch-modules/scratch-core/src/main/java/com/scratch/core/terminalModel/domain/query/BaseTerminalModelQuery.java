package com.scratch.core.terminalModel.domain.query;

import com.scratch.core.api.terminalModel.domain.po.BaseTerminalModelPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 终端型号 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTerminalModelQuery extends BaseTerminalModelPo {

    @Serial
    private static final long serialVersionUID = 1L;
}