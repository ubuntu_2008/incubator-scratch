package com.scratch.core.org.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.api.org.domain.po.BaseOrgPo;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 信息管理 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseOrgConverter extends BaseConverter<BaseOrgQuery, BaseOrgDto, BaseOrgPo> {
}
