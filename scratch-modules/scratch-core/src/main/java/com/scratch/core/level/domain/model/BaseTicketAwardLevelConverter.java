package com.scratch.core.level.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.api.level.domain.po.BaseTicketAwardLevelPo;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 奖级 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseTicketAwardLevelConverter extends BaseConverter<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto, BaseTicketAwardLevelPo> {
}
