package com.scratch.core.org.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.common.security.annotation.AdminAuth;
import com.scratch.common.security.utils.SecurityUtils;
import com.scratch.core.org.controller.base.BBaseOrgController;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.core.org.service.IBaseOrgService;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 信息管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/org")
public class ABaseOrgController extends BBaseOrgController {

    @Autowired
    private IBaseOrgService baseOrgService;

    /**
     * 获取机构编号
     */
    @GetMapping(value = "/getOrgNo")
    public AjaxResult getOrgNo() {
        return success(baseOrgService.getOrgNo());
    }

    /**
     * 业务授权
     */
    @PutMapping("/authBus")
    @PreAuthorize("@ss.hasAuthority('core:org:edit')")
    @Log(title = "机构业务授权", businessType = BusinessType.UPDATE)
    public AjaxResult authBus(@RequestBody BaseOrgDto org) {
        return toAjax(baseOrgService.authBus(org));
    }

    @PostMapping("/authPlay")
    @PreAuthorize("@ss.hasAuthority('core:org:edit')")
    @Log(title = "机构玩法授权", businessType = BusinessType.UPDATE)
    public AjaxResult authPlay(@RequestBody BaseOrgAuthPlayDto baseOrgAuthPlayDto) {
        return toAjax(baseOrgService.authPlay(baseOrgAuthPlayDto));
    }


    @GetMapping("/listAuthPlay")
    @PreAuthorize("@ss.hasAuthority('core:org:list')")
    public AjaxResult listAuthPlay(String orgId) {
        startPage();
        List<BaseOrgAuthPlayDataDto> listAuthPlay = baseOrgService.listOrgAuthPlay(orgId);
        return getDataTable(listAuthPlay);
    }


    /**
     * 查询信息管理列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:org:list')")
    public AjaxResult list(BaseOrgQuery org) {
        return super.list(org);
    }

    /**
     * 查询信息管理详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:org:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 信息管理新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:org:add')")
    @Log(title = "信息管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseOrgDto org) {
        org.setEnterpriseId(SecurityUtils.getEnterpriseId());
        org.setCreateBy(SecurityUtils.getUserId());
        AjaxResult ajaxResult = super.add(org);
        if (ajaxResult.isSuccess()) {
            baseOrgService.addAfter(ajaxResult);
        }
        return ajaxResult;
    }

    /**
     * 信息管理修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:org:edit')")
    @Log(title = "信息管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseOrgDto org) {
        org.setEnterpriseId(SecurityUtils.getEnterpriseId());
        org.setCreateBy(SecurityUtils.getUserId());
        return super.edit(org);
    }

    /**
     * 信息管理修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:org:edit', 'core:org:es')")
    @Log(title = "信息管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseOrgDto org) {
        org.setEnterpriseId(SecurityUtils.getEnterpriseId());
        org.setCreateBy(SecurityUtils.getUserId());
        return super.editStatus(org);
    }

    /**
     * 信息管理批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:org:delete')")
    @Log(title = "信息管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
