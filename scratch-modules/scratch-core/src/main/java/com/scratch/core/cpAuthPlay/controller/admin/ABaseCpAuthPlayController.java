package com.scratch.core.cpAuthPlay.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.cpAuthPlay.controller.base.BBaseCpAuthPlayController;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 渠道玩法授权管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/cpAuthPlay")
public class ABaseCpAuthPlayController extends BBaseCpAuthPlayController {

    /**
     * 查询渠道玩法授权列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:cpAuthPlay:list')")
    public AjaxResult list(BaseCpAuthPlayQuery cpAuthPlay) {
        return super.list(cpAuthPlay);
    }

    /**
     * 查询渠道玩法授权详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:cpAuthPlay:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 渠道玩法授权新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:cpAuthPlay:add')")
    @Log(title = "渠道玩法授权管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseCpAuthPlayDto cpAuthPlay) {
        return super.add(cpAuthPlay);
    }

    /**
     * 渠道玩法授权修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:cpAuthPlay:edit')")
    @Log(title = "渠道玩法授权管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseCpAuthPlayDto cpAuthPlay) {
        return super.edit(cpAuthPlay);
    }

    /**
     * 渠道玩法授权修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:cpAuthPlay:edit', 'core:cpAuthPlay:es')")
    @Log(title = "渠道玩法授权管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseCpAuthPlayDto cpAuthPlay) {
        return super.editStatus(cpAuthPlay);
    }

    /**
     * 渠道玩法授权批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:cpAuthPlay:delete')")
    @Log(title = "渠道玩法授权管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
