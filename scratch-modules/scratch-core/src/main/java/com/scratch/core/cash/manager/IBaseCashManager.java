package com.scratch.core.cash.manager;

import com.scratch.core.api.cash.domain.dto.BaseCashDto;
import com.scratch.core.cash.domain.query.BaseCashQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 兑奖管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseCashManager extends IBaseManager<BaseCashQuery, BaseCashDto> {
}