package com.scratch.core.ticket.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.utils.core.*;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.core.level.service.IBaseTicketAwardLevelService;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.core.ticket.domain.correlate.BaseTicketCorrelate;
import com.scratch.core.ticket.service.IBaseTicketService;
import com.scratch.core.ticket.manager.IBaseTicketManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 票信息管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseTicketServiceImpl extends BaseServiceImpl<BaseTicketQuery, BaseTicketDto, BaseTicketCorrelate, IBaseTicketManager> implements IBaseTicketService {

    @Autowired
    private IBaseTicketAwardLevelService levelService;


    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.TICKET;
    }


    /**
     * 查询票信息管理对象列表 | 数据权限
     *
     * @param ticket 票信息管理对象
     * @return 票信息管理对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseTicketMapper"})
    public List<BaseTicketDto> selectListScope(BaseTicketQuery ticket) {
        return super.selectListScope(ticket);
    }


    @Override
    public BaseTicketDto getInfoTicket(Serializable id) {
        BaseTicketDto baseTicketDto = baseManager.selectById(id);
        BaseTicketAwardLevelQuery baseTicketAwardLevelQuery = new BaseTicketAwardLevelQuery();
        baseTicketAwardLevelQuery.setBaseTicketId(baseTicketDto.getId());

        List<BaseTicketAwardLevelDto> levels = levelService.selectList(baseTicketAwardLevelQuery);
        baseTicketDto.setLevels(levels);
        return baseTicketDto;
    }

    @Override
    public Boolean addTicket(BaseTicketDto ticket) {
        Long baseTicketId = IdUtil.getSnowflake(0, 0).nextId();
        ticket.setId(baseTicketId);
        AtomicInteger index=new AtomicInteger(0);
        List<BaseTicketAwardLevelDto> levels = ticket.getLevels().stream().filter(x -> StringUtils.isNotBlank(x.getName())).map(
                x -> {
                    x.setBaseTicketId(baseTicketId);
                    x.setSort(index.getAndIncrement() + 1);
                    return x;
                }
        ).collect(Collectors.toList());
        return levelService.insertBatch(levels) > 0 && this.insert(ticket) > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean editTicket(BaseTicketDto ticket) {
        this.update(ticket);
        BaseTicketAwardLevelQuery baseTicketAwardLevelQuery = new BaseTicketAwardLevelQuery();
        baseTicketAwardLevelQuery.setBaseTicketId(ticket.getId());
        levelService.delete(baseTicketAwardLevelQuery);
        AtomicInteger index=new AtomicInteger(0);

        List<BaseTicketAwardLevelDto> levels = ticket.getLevels().stream().filter(x -> !Objects.isNull(x)&& StrUtil.isNotBlank(x.getName())).map(
                x -> {
                    x.setBaseTicketId(ticket.getId());
                    x.setSort(index.getAndIncrement() + 1);
                    return x;
                }
        ).collect(Collectors.toList());
        return levelService.insertBatch(levels) > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public int batchRemoveTicket(List<Long> idList) {
        idList.forEach(id -> {
            BaseTicketAwardLevelQuery baseTicketAwardLevelQuery = new BaseTicketAwardLevelQuery();
            baseTicketAwardLevelQuery.setBaseTicketId(id);
            levelService.delete(baseTicketAwardLevelQuery);
        });
        return this.deleteByIds(idList);
    }

}