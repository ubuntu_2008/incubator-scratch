package com.scratch.core.area.domain.model;

import com.scratch.common.core.web.entity.model.TreeConverter;
import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.po.SysAreaPo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 区域 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysAreaConverter extends TreeConverter<SysAreaQuery, SysAreaDto, SysAreaPo> {
}
