package com.scratch.core.cp.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.api.cp.domain.po.BaseCpPo;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 信息管理 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseCpConverter extends BaseConverter<BaseCpQuery, BaseCpDto, BaseCpPo> {
}
