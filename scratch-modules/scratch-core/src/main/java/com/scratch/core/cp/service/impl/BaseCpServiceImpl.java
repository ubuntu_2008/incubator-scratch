package com.scratch.core.cp.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.OperateConstants;
import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.redis.constant.RedisConstants;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.core.cp.domain.correlate.BaseCpCorrelate;
import com.scratch.core.cp.service.IBaseCpService;
import com.scratch.core.cp.manager.IBaseCpManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.core.cpAuthPlay.service.IBaseCpAuthPlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 信息管理管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseCpServiceImpl extends BaseServiceImpl<BaseCpQuery, BaseCpDto, BaseCpCorrelate, IBaseCpManager> implements IBaseCpService {
    @Autowired
    private IBaseCpManager baseCpManager;

    @Autowired
    private IBaseCpAuthPlayService baseCpAuthPlayService;


    private final static String CP_NO_B = "CP";
    private final static Long CP_NO_C = 10001L;

    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.CP;
    }


    /**
     * 查询信息管理对象列表 | 数据权限
     *
     * @param cp 信息管理对象
     * @return 信息管理对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseCpMapper"})
    public List<BaseCpDto> selectListScope(BaseCpQuery cp) {
        return super.selectListScope(cp);
    }

    @Override
    public BaseCpDto getCpNo() {
        String cpNo = redisService.hasKey(RedisConstants.BaseKey.BASE_CP_NO.getCode()) ?
                CP_NO_B + (CP_NO_C + ((Integer) redisService.getCacheObject(RedisConstants.BaseKey.BASE_CP_NO.getCode())))
                : CP_NO_B + CP_NO_C;
        BaseCpDto BaseCpDto = new BaseCpDto();
        BaseCpDto.setCpNo(cpNo);
        return BaseCpDto;
    }

    @Override
    public void addAfter(AjaxResult ajaxResult) {
        if (ajaxResult.isSuccess()) {
            redisService.redisTemplate.opsForValue().increment(RedisConstants.BaseKey.BASE_CP_NO.getCode(), 1);
        }
    }


    @Override
    public int authBus(BaseCpDto cp) {
        BaseCpDto originDto = startHandle(OperateConstants.ServiceType.EDIT, cp, null);
        int row = baseCpManager.authBus(cp.getId(), cp.getAuthBus());
        BaseCpDto newCp=originDto;
        newCp.setAuthBus(cp.getAuthBus());
        endHandle(OperateConstants.ServiceType.EDIT, row, originDto, newCp);
        return row;
    }

    @Override
    public int authPlay(BaseCpAuthPlayDto baseCpAuthPlayDto) {
        BaseCpAuthPlayQuery baseCpAuthPlayQuery = new BaseCpAuthPlayQuery();
        baseCpAuthPlayQuery.setBaseCpId(baseCpAuthPlayDto.getBaseCpId());
        baseCpAuthPlayQuery.setBaseTicketId(baseCpAuthPlayDto.getBaseTicketId());
        List<BaseCpAuthPlayDto> list = baseCpAuthPlayService.selectList(baseCpAuthPlayQuery);
        if (list.size() > 0) {
            baseCpAuthPlayDto.setId(list.get(0).getId());
            return baseCpAuthPlayService.update(baseCpAuthPlayDto);
        } else {
            return baseCpAuthPlayService.insert(baseCpAuthPlayDto);
        }
    }


    @Override
    public List<BaseCpAuthPlayDataDto> listCpAuthPlay(String cpId) {
        return baseCpAuthPlayService.listCpAuthPlay(cpId);
    }
}