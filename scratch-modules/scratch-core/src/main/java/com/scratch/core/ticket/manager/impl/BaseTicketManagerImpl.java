package com.scratch.core.ticket.manager.impl;

import com.scratch.core.api.ticket.domain.po.BaseTicketPo;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.core.ticket.domain.model.BaseTicketConverter;
import com.scratch.core.ticket.mapper.BaseTicketMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.ticket.manager.IBaseTicketManager;
import org.springframework.stereotype.Component;

/**
 * 票信息管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseTicketManagerImpl extends BaseManagerImpl<BaseTicketQuery, BaseTicketDto, BaseTicketPo, BaseTicketMapper, BaseTicketConverter> implements IBaseTicketManager {
}