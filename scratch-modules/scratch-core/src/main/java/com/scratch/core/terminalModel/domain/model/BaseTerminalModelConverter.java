package com.scratch.core.terminalModel.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.api.terminalModel.domain.po.BaseTerminalModelPo;
import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 终端型号 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseTerminalModelConverter extends BaseConverter<BaseTerminalModelQuery, BaseTerminalModelDto, BaseTerminalModelPo> {
}
