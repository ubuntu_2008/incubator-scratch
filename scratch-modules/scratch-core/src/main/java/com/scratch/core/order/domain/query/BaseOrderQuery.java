package com.scratch.core.order.domain.query;

import com.scratch.core.api.order.domain.po.BaseOrderPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 订单 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseOrderQuery extends BaseOrderPo {

    @Serial
    private static final long serialVersionUID = 1L;
}