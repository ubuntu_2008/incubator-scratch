package com.scratch.core.terminal.mapper;

import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.api.terminal.domain.po.BaseTerminalPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 终端信息管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseTerminalMapper extends BaseMapper<BaseTerminalQuery, BaseTerminalDto, BaseTerminalPo> {
}