package com.scratch.core.siteAuthPlay.manager;

import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

import java.util.List;

/**
 * 站点玩法授权管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseSiteAuthPlayManager extends IBaseManager<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto> {

    int authPlay(Long baseSiteId,Long baseTicketId, String authSell, String bkgeSell,String authCash,String bkgeCash);

    List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(String siteId);
}