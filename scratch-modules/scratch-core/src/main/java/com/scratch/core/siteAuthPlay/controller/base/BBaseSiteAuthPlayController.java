package com.scratch.core.siteAuthPlay.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.core.siteAuthPlay.service.IBaseSiteAuthPlayService;

/**
 * 站点玩法授权管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseSiteAuthPlayController extends BaseController<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto, IBaseSiteAuthPlayService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "站点玩法授权" ;
    }
}
