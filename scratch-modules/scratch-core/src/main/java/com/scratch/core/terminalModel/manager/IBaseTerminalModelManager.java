package com.scratch.core.terminalModel.manager;

import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 终端型号管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseTerminalModelManager extends IBaseManager<BaseTerminalModelQuery, BaseTerminalModelDto> {
    int hp(Long id, String hp);
}