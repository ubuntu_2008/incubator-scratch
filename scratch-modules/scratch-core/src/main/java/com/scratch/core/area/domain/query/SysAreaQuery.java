package com.scratch.core.area.domain.query;

import com.scratch.core.area.domain.po.SysAreaPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 区域 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysAreaQuery extends SysAreaPo {

    @Serial
    private static final long serialVersionUID = 1L;
}