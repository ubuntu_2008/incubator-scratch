package com.scratch.core.org.domain.query;

import com.scratch.core.api.org.domain.po.BaseOrgPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 信息管理 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseOrgQuery extends BaseOrgPo {

    @Serial
    private static final long serialVersionUID = 1L;
}