package com.scratch.core.ticket.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.core.ticket.service.IBaseTicketService;

/**
 * 票信息管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseTicketController extends BaseController<BaseTicketQuery, BaseTicketDto, IBaseTicketService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "票信息管理" ;
    }
}
