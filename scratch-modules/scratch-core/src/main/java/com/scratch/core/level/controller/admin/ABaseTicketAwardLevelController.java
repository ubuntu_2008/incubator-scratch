package com.scratch.core.level.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.level.controller.base.BBaseTicketAwardLevelController;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 奖级管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/level")
public class ABaseTicketAwardLevelController extends BBaseTicketAwardLevelController {

    /**
     * 查询奖级列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:level:list')")
    public AjaxResult list(BaseTicketAwardLevelQuery ticketAwardLevel) {
        return super.list(ticketAwardLevel);
    }

    /**
     * 查询奖级详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:level:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 奖级新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:level:add')")
    @Log(title = "奖级管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseTicketAwardLevelDto ticketAwardLevel) {
        return super.add(ticketAwardLevel);
    }

    /**
     * 奖级修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:level:edit')")
    @Log(title = "奖级管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseTicketAwardLevelDto ticketAwardLevel) {
        return super.edit(ticketAwardLevel);
    }

    /**
     * 奖级批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:level:delete')")
    @Log(title = "奖级管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
