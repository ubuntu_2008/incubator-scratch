package com.scratch.core.cp.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.service.CorrelateService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 信息管理 关联映射
 */
@Getter
@AllArgsConstructor
public enum BaseCpCorrelate implements CorrelateService {

    ;

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}