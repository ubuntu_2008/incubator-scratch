package com.scratch.core.org.service;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.common.web.entity.service.IBaseService;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;

import java.util.List;

/**
 * 信息管理管理 服务层
 *
 * @author scratch
 */
public interface IBaseOrgService extends IBaseService<BaseOrgQuery, BaseOrgDto> {

    BaseOrgDto getOrgNo();

    void addAfter(AjaxResult ajaxResult);

    int authBus(BaseOrgDto org);

    int authPlay(BaseOrgAuthPlayDto baseOrgAuthPlayDto);

    List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(String orgId);


}