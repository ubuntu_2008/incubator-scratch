package com.scratch.core.terminalModel.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.terminalModel.controller.base.BBaseTerminalModelController;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 终端型号管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/terminalModel")
public class ABaseTerminalModelController extends BBaseTerminalModelController {


    @PutMapping("/hp")
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:edit')")
    @Log(title = "终端型号仓位配置", businessType = BusinessType.UPDATE)
    public AjaxResult authBus(@RequestBody BaseTerminalModelDto terminalModel) {
        return toAjax(baseService.hp(terminalModel));
    }

    /**
     * 查询终端型号列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:list')")
    public AjaxResult list(BaseTerminalModelQuery terminalModel) {
        return super.list(terminalModel);
    }

    /**
     * 查询终端型号详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }





    /**
     * 终端型号新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:add')")
    @Log(title = "终端型号管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseTerminalModelDto terminalModel) {
        return super.add(terminalModel);
    }

    /**
     * 终端型号修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:edit')")
    @Log(title = "终端型号管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseTerminalModelDto terminalModel) {
        return super.edit(terminalModel);
    }

    /**
     * 终端型号修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:terminalModel:edit', 'core:terminalModel:es')")
    @Log(title = "终端型号管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseTerminalModelDto terminalModel) {
        return super.editStatus(terminalModel);
    }

    /**
     * 终端型号批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:delete')")
    @Log(title = "终端型号管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
