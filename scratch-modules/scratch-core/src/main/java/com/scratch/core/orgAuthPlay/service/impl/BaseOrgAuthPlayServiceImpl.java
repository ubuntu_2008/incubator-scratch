package com.scratch.core.orgAuthPlay.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import com.scratch.core.orgAuthPlay.domain.correlate.BaseOrgAuthPlayCorrelate;
import com.scratch.core.orgAuthPlay.service.IBaseOrgAuthPlayService;
import com.scratch.core.orgAuthPlay.manager.IBaseOrgAuthPlayManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 机构玩法授权管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseOrgAuthPlayServiceImpl extends BaseServiceImpl<BaseOrgAuthPlayQuery, BaseOrgAuthPlayDto, BaseOrgAuthPlayCorrelate, IBaseOrgAuthPlayManager> implements IBaseOrgAuthPlayService {

    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.ORG_AUTH_PLAY;
    }

    /**
     * 查询机构玩法授权对象列表 | 数据权限
     *
     * @param orgAuthPlay 机构玩法授权对象
     * @return 机构玩法授权对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseOrgAuthPlayMapper"})
    public List<BaseOrgAuthPlayDto> selectListScope(BaseOrgAuthPlayQuery orgAuthPlay) {
        return super.selectListScope(orgAuthPlay);
    }

    @Override
    public int authPlay(Long baseOrgId, Long baseTicketId, String authBus, String authPlay) {
        return baseManager.authPlay(baseOrgId,baseTicketId,authBus,authPlay);
    }

    @Override
    public List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(String orgId) {
        return baseManager.listOrgAuthPlay(orgId);
    }
}