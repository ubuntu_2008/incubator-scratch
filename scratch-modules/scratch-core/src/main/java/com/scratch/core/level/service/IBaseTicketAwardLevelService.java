package com.scratch.core.level.service;

import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.common.web.entity.service.IBaseService;

/**
 * 奖级管理 服务层
 *
 * @author scratch
 */
public interface IBaseTicketAwardLevelService extends IBaseService<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto> {
}