package com.scratch.core.cpAuthPlay.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.cpAuthPlay.controller.base.BBaseCpAuthPlayController;

/**
 * 渠道玩法授权管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/cpAuthPlay")
public class IBaseCpAuthPlayController extends BBaseCpAuthPlayController {
}
