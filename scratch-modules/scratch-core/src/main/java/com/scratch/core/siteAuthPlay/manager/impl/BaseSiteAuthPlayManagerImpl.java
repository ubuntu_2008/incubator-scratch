package com.scratch.core.siteAuthPlay.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.api.siteAuthPlay.domain.po.BaseSiteAuthPlayPo;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.core.siteAuthPlay.domain.model.BaseSiteAuthPlayConverter;
import com.scratch.core.siteAuthPlay.mapper.BaseSiteAuthPlayMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.siteAuthPlay.manager.IBaseSiteAuthPlayManager;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 站点玩法授权管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseSiteAuthPlayManagerImpl extends BaseManagerImpl<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto, BaseSiteAuthPlayPo, BaseSiteAuthPlayMapper, BaseSiteAuthPlayConverter> implements IBaseSiteAuthPlayManager {
    @Override
    public int authPlay(Long baseSiteId,Long baseTicketId, String authSell, String bkgeSell,String authCash,String bkgeCash) {
        return baseMapper.update(null,
                Wrappers.<BaseSiteAuthPlayPo>update().lambda()
                        .set(BaseSiteAuthPlayPo::getAuthSell, authSell)
                        .set(BaseSiteAuthPlayPo::getBkgeSell, bkgeSell)
                        .set(BaseSiteAuthPlayPo::getAuthCash, authCash)
                        .set(BaseSiteAuthPlayPo::getBkgeCash, bkgeCash)
                        .eq(BaseSiteAuthPlayPo::getBaseSiteId, baseSiteId)
                        .eq(BaseSiteAuthPlayPo::getBaseTicketId,baseTicketId));
    }

    @Override
    public List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(String siteId) {
        return baseMapper.listSiteAuthPlay(siteId);
    }

}