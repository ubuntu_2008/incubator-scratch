package com.scratch.core.terminal.service;

import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.common.web.entity.service.IBaseService;

import java.util.Map;

/**
 * 终端信息管理 服务层
 *
 * @author scratch
 */
public interface IBaseTerminalService extends IBaseService<BaseTerminalQuery, BaseTerminalDto> {
    int allocation(BaseTerminalDto baseTerminalDto);
    Map<String, Object> hpTicket(String devNo);
}