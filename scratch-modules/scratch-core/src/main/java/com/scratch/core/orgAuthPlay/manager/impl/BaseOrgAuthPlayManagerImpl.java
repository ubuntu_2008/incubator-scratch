package com.scratch.core.orgAuthPlay.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.core.orgAuthPlay.domain.model.BaseOrgAuthPlayConverter;
import com.scratch.core.api.orgAuthPlay.domain.po.BaseOrgAuthPlayPo;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import com.scratch.core.orgAuthPlay.manager.IBaseOrgAuthPlayManager;
import com.scratch.core.orgAuthPlay.mapper.BaseOrgAuthPlayMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 机构玩法授权管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseOrgAuthPlayManagerImpl extends BaseManagerImpl<BaseOrgAuthPlayQuery, BaseOrgAuthPlayDto, BaseOrgAuthPlayPo, BaseOrgAuthPlayMapper, BaseOrgAuthPlayConverter> implements IBaseOrgAuthPlayManager {
    @Deprecated
    @Override
    public int authPlay(Long baseOrgId,Long baseTicketId, String authSell, String authCash) {
        return baseMapper.update(null,
                Wrappers.<BaseOrgAuthPlayPo>update().lambda()
                        .set(BaseOrgAuthPlayPo::getAuthSell, authSell)
                        .set(BaseOrgAuthPlayPo::getAuthCash, authCash)
                        .eq(BaseOrgAuthPlayPo::getBaseOrgId, baseOrgId)
                        .eq(BaseOrgAuthPlayPo::getBaseTicketId,baseTicketId));

    }


    @Override
    public List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(String orgId) {
        return baseMapper.listOrgAuthPlay(orgId);
    }


}