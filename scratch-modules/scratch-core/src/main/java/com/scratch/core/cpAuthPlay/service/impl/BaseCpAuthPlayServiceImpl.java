package com.scratch.core.cpAuthPlay.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.core.cpAuthPlay.domain.correlate.BaseCpAuthPlayCorrelate;
import com.scratch.core.cpAuthPlay.service.IBaseCpAuthPlayService;
import com.scratch.core.cpAuthPlay.manager.IBaseCpAuthPlayManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 渠道玩法授权管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseCpAuthPlayServiceImpl extends BaseServiceImpl<BaseCpAuthPlayQuery, BaseCpAuthPlayDto, BaseCpAuthPlayCorrelate, IBaseCpAuthPlayManager> implements IBaseCpAuthPlayService {


    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.CP_AUTH_PLAY;
    }


    /**
     * 查询渠道玩法授权对象列表 | 数据权限
     *
     * @param cpAuthPlay 渠道玩法授权对象
     * @return 渠道玩法授权对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseCpAuthPlayMapper"})
    public List<BaseCpAuthPlayDto> selectListScope(BaseCpAuthPlayQuery cpAuthPlay) {
        return super.selectListScope(cpAuthPlay);
    }

    @Override
    public int authPlay(Long baseCpId, Long baseTicketId, String authBus, String authPlay) {
        return baseManager.authPlay(baseCpId, baseTicketId, authBus, authPlay);
    }

    @Override
    public List<BaseCpAuthPlayDataDto> listCpAuthPlay(String cpId) {
        return baseManager.listCpAuthPlay(cpId);
    }
}