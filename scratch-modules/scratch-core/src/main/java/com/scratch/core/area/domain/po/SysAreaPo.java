package com.scratch.core.area.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.scratch.common.core.web.tenant.base.TTreeEntity;
import com.scratch.core.area.domain.dto.SysAreaDto;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

import static com.baomidou.mybatisplus.annotation.SqlCondition.LIKE;
import static com.scratch.common.core.constant.basic.EntityConstants.REMARK;

/**
 * 区域 持久化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_area", excludeProperty = { REMARK })
public class SysAreaPo extends TTreeEntity<SysAreaDto> {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 区域状态 */
    protected String status;

    /** 名称 */
    @TableField(condition = LIKE)
    @NotBlank(message = "区域名称不能为空")
    protected String name;


}