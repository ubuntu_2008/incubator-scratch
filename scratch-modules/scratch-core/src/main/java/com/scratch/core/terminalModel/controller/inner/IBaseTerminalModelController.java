package com.scratch.core.terminalModel.controller.inner;

import com.scratch.common.security.annotation.InnerAuth;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scratch.core.terminalModel.controller.base.BBaseTerminalModelController;

/**
 * 终端型号管理 | 内部调用 业务处理
 *
 * @author scratch
 */
@InnerAuth
@RestController
@RequestMapping("/inner/terminalModel")
public class IBaseTerminalModelController extends BBaseTerminalModelController {
}
