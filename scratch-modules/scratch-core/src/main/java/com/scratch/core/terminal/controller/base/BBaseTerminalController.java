package com.scratch.core.terminal.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.core.terminal.service.IBaseTerminalService;

/**
 * 终端信息管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseTerminalController extends BaseController<BaseTerminalQuery, BaseTerminalDto, IBaseTerminalService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "终端信息" ;
    }
}
