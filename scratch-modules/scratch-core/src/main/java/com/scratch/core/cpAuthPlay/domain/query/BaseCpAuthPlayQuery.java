package com.scratch.core.cpAuthPlay.domain.query;

import com.scratch.core.api.cpAuthPlay.domain.po.BaseCpAuthPlayPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 渠道玩法授权 数据查询对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseCpAuthPlayQuery extends BaseCpAuthPlayPo {

    @Serial
    private static final long serialVersionUID = 1L;
}