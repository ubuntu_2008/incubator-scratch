package com.scratch.core.level.service.impl;

import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.core.level.domain.correlate.BaseTicketAwardLevelCorrelate;
import com.scratch.core.level.service.IBaseTicketAwardLevelService;
import com.scratch.core.level.manager.IBaseTicketAwardLevelManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 奖级管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseTicketAwardLevelServiceImpl extends BaseServiceImpl<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto, BaseTicketAwardLevelCorrelate, IBaseTicketAwardLevelManager> implements IBaseTicketAwardLevelService {

    /**
     * 查询奖级对象列表 | 数据权限
     *
     * @param ticketAwardLevel 奖级对象
     * @return 奖级对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseTicketAwardLevelMapper"})
    public List<BaseTicketAwardLevelDto> selectListScope(BaseTicketAwardLevelQuery ticketAwardLevel) {
        return super.selectListScope(ticketAwardLevel);
    }

}