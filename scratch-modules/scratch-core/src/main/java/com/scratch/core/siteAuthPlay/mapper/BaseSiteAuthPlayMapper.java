package com.scratch.core.siteAuthPlay.mapper;

import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDataDto;
import com.scratch.core.siteAuthPlay.domain.query.BaseSiteAuthPlayQuery;
import com.scratch.core.api.siteAuthPlay.domain.dto.BaseSiteAuthPlayDto;
import com.scratch.core.api.siteAuthPlay.domain.po.BaseSiteAuthPlayPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 站点玩法授权管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseSiteAuthPlayMapper extends BaseMapper<BaseSiteAuthPlayQuery, BaseSiteAuthPlayDto, BaseSiteAuthPlayPo> {
    @Select("SELECT a.id AS baseTicketId,a.img,a.play_name AS playName ,a.play_no AS playNo,IF(b.auth_sell=1 OR b.auth_sell IS NULL,'',0)  AS authSell,IF(b.bkge_sell IS NULL,0,b.bkge_sell) AS bkgeSell,IF(b.auth_cash=1 OR b.auth_cash IS NULL,'',0) AS authCash ,IF(b.bkge_cash IS NULL,0,b.bkge_cash) AS bkgeCash\t FROM base_ticket a\n" +
            "LEFT JOIN   `base_site_auth_play` b ON\n" +
            "a.id=b.base_ticket_id  AND\n" +
            "b.base_site_id=#{siteId}  \n" +
            "WHERE  a.del_flag=0 ORDER BY a.create_time DESC")
    List<BaseSiteAuthPlayDataDto> listSiteAuthPlay(@Param("siteId") String siteId);
}