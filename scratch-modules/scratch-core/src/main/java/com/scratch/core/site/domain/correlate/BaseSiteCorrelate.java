package com.scratch.core.site.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.domain.Direct;
import com.scratch.common.web.correlate.domain.Remote;
import com.scratch.common.web.correlate.service.CorrelateService;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.cp.service.IBaseCpService;
import com.scratch.core.api.site.domain.dto.BaseSiteDto;
import com.scratch.system.api.organize.feign.RemoteDeptService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import com.scratch.system.api.organize.domain.dto.SysDeptDto;

import static com.scratch.common.web.correlate.contant.CorrelateConstants.SubOperate.SELECT;

/**
 * 网点 关联映射
 */
@Getter
@AllArgsConstructor
public enum BaseSiteCorrelate implements CorrelateService {

    BASE_LIST("默认列表|（归属区域）,(归属渠道)", new ArrayList<>() {{
        add(new Remote<>(SELECT, RemoteDeptService.class, BaseSiteDto::getAreaId, SysDeptDto::getId, BaseSiteDto::getDept));
        add(new Direct<>(SELECT, IBaseCpService.class, BaseSiteDto::getCpId, BaseCpDto::getId, BaseSiteDto::getCp));
    }});

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}