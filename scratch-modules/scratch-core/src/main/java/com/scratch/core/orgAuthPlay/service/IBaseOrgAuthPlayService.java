package com.scratch.core.orgAuthPlay.service;

import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDataDto;
import com.scratch.core.orgAuthPlay.domain.query.BaseOrgAuthPlayQuery;
import com.scratch.core.api.orgAuthPlay.domain.dto.BaseOrgAuthPlayDto;
import com.scratch.common.web.entity.service.IBaseService;

import java.util.List;

/**
 * 机构玩法授权管理 服务层
 *
 * @author scratch
 */
public interface IBaseOrgAuthPlayService extends IBaseService<BaseOrgAuthPlayQuery, BaseOrgAuthPlayDto> {

    int authPlay(Long baseOrgId,Long baseTicketId,String authBus,String authPlay);

    List<BaseOrgAuthPlayDataDto> listOrgAuthPlay(String orgId);

}