package com.scratch.core.level.mapper;

import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.api.level.domain.po.BaseTicketAwardLevelPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 奖级管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseTicketAwardLevelMapper extends BaseMapper<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto, BaseTicketAwardLevelPo> {
}