package com.scratch.core.ticket.controller.admin;

import com.scratch.common.core.constant.basic.BaseConstants;
import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.core.ticket.service.IBaseTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.ticket.controller.base.BBaseTicketController;
import com.scratch.core.api.ticket.domain.dto.BaseTicketDto;
import com.scratch.core.ticket.domain.query.BaseTicketQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 票信息管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/ticket")
public class ABaseTicketController extends BBaseTicketController {

    @Autowired
    private IBaseTicketService baseTicketService;

    /**
     * 查询票信息管理列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:ticket:list')")
    public AjaxResult list(BaseTicketQuery ticket) {
        return super.list(ticket);
    }

    /**
     * 查询票信息管理详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:ticket:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return success(baseTicketService.getInfoTicket(id));
    }

    /**
     * 票信息管理新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:ticket:add')")
    @Log(title = "票信息管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseTicketDto ticket) {
        ticket.initOperate(BaseConstants.Operate.ADD);
        AEHandle(ticket.getOperate(), ticket);
        return toAjax(baseTicketService.addTicket(ticket));
    }

    /**
     * 票信息管理修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:ticket:edit')")
    @Log(title = "票信息管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseTicketDto ticket) {
        return toAjax(baseTicketService.editTicket(ticket));
    }

    /**
     * 票信息管理修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:ticket:edit', 'core:ticket:es')")
    @Log(title = "票信息管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseTicketDto ticket) {
        return super.editStatus(ticket);
    }

    /**
     * 票信息管理批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:ticket:delete')")
    @Log(title = "票信息管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return toAjax(baseTicketService.batchRemoveTicket(idList));
    }
}
