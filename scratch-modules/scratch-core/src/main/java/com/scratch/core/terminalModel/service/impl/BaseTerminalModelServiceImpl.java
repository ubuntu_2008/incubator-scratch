package com.scratch.core.terminalModel.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.OperateConstants;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.redis.constant.RedisConstants;
import com.scratch.common.redis.service.RedisService;
import com.scratch.core.api.terminalModel.domain.dto.BaseTerminalModelDto;
import com.scratch.core.terminalModel.domain.query.BaseTerminalModelQuery;
import com.scratch.core.terminalModel.domain.correlate.BaseTerminalModelCorrelate;
import com.scratch.core.terminalModel.service.IBaseTerminalModelService;
import com.scratch.core.terminalModel.manager.IBaseTerminalModelManager;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 终端型号管理 服务层处理
 *
 * @author scratch
 */
@Service
public class BaseTerminalModelServiceImpl extends BaseServiceImpl<BaseTerminalModelQuery, BaseTerminalModelDto, BaseTerminalModelCorrelate, IBaseTerminalModelManager> implements IBaseTerminalModelService {

    @Autowired
    private RedisService redisService;

    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.TERMINAL_MODEL;
    }

    /**
     * 查询终端型号对象列表 | 数据权限
     *
     * @param terminalModel 终端型号对象
     * @return 终端型号对象集合
     */
    @Override
    //@DataScope(userAlias = "createBy", mapperScope = {"BaseTerminalModelMapper"})
    public List<BaseTerminalModelDto> selectListScope(BaseTerminalModelQuery terminalModel) {
        return super.selectListScope(terminalModel);
    }

    @Override
    public int hp(BaseTerminalModelDto terminalModel) {
        BaseTerminalModelDto originDto = startHandle(OperateConstants.ServiceType.EDIT, terminalModel, null);
        int row = baseManager.hp(terminalModel.getId(), terminalModel.getHp());
        BaseTerminalModelDto newTerminalModel = originDto;
        newTerminalModel.setHp(terminalModel.getHp());
        endHandle(OperateConstants.ServiceType.EDIT, row, originDto, newTerminalModel);
        return row;


    }

}