package com.scratch.core.cp.service;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.common.web.entity.service.IBaseService;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;

import java.util.List;

/**
 * 信息管理管理 服务层
 *
 * @author scratch
 */
public interface IBaseCpService extends IBaseService<BaseCpQuery, BaseCpDto> {
    BaseCpDto getCpNo();

    void addAfter(AjaxResult ajaxResult);

    int authBus(BaseCpDto cp);

    int authPlay(BaseCpAuthPlayDto baseCpAuthPlayDto);

    List<BaseCpAuthPlayDataDto> listCpAuthPlay(String cpId);

}