package com.scratch.core.area.service;

import com.scratch.core.area.domain.dto.SysAreaDto;
import com.scratch.core.area.domain.query.SysAreaQuery;
import com.scratch.common.web.entity.service.ITreeService;

/**
 * 区域管理 服务层
 *
 * @author scratch
 */
public interface ISysAreaService extends ITreeService<SysAreaQuery, SysAreaDto> {
}