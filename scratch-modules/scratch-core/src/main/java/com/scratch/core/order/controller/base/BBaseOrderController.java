package com.scratch.core.order.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import com.scratch.core.order.service.IBaseOrderService;

/**
 * 订单管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BBaseOrderController extends BaseController<BaseOrderQuery, BaseOrderDto, IBaseOrderService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "订单" ;
    }
}
