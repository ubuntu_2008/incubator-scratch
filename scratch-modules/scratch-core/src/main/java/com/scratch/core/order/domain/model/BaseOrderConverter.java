package com.scratch.core.order.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.core.api.order.domain.dto.BaseOrderDto;
import com.scratch.core.api.order.domain.po.BaseOrderPo;
import com.scratch.core.order.domain.query.BaseOrderQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 订单 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BaseOrderConverter extends BaseConverter<BaseOrderQuery, BaseOrderDto, BaseOrderPo> {
}
