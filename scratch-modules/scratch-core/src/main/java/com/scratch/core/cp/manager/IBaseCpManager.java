package com.scratch.core.cp.manager;

import com.scratch.core.api.cp.domain.dto.BaseCpDto;
import com.scratch.core.cp.domain.query.BaseCpQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 信息管理管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseCpManager extends IBaseManager<BaseCpQuery, BaseCpDto> {
    int authBus(Long id, String authBus);
}