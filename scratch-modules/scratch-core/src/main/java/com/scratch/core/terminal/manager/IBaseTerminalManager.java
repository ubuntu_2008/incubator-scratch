package com.scratch.core.terminal.manager;

import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

/**
 * 终端信息管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseTerminalManager extends IBaseManager<BaseTerminalQuery, BaseTerminalDto> {
    int allocation(Long id, Long siteId);
}