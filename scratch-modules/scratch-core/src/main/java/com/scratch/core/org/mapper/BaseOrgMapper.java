package com.scratch.core.org.mapper;

import com.scratch.core.org.domain.query.BaseOrgQuery;
import com.scratch.core.api.org.domain.dto.BaseOrgDto;
import com.scratch.core.api.org.domain.po.BaseOrgPo;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.common.datasource.annotation.Isolate;

/**
 * 信息管理管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface BaseOrgMapper extends BaseMapper<BaseOrgQuery, BaseOrgDto, BaseOrgPo> {
}