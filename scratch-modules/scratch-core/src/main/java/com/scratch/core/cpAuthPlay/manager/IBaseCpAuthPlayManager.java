package com.scratch.core.cpAuthPlay.manager;

import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDataDto;
import com.scratch.core.api.cpAuthPlay.domain.dto.BaseCpAuthPlayDto;
import com.scratch.core.cpAuthPlay.domain.query.BaseCpAuthPlayQuery;
import com.scratch.common.web.entity.manager.IBaseManager;

import java.util.List;

/**
 * 渠道玩法授权管理 数据封装层
 *
 * @author scratch
 */
public interface IBaseCpAuthPlayManager extends IBaseManager<BaseCpAuthPlayQuery, BaseCpAuthPlayDto> {

    int authPlay(Long baseCpId,Long baseTicketId,String authBus,String authPlay);

    List<BaseCpAuthPlayDataDto> listCpAuthPlay(String cpId);
}