package com.scratch.core.terminal.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.core.web.validate.V_A;
import com.scratch.common.core.web.validate.V_E;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.core.terminal.service.IBaseTerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import com.scratch.core.terminal.controller.base.BBaseTerminalController;
import com.scratch.core.api.terminal.domain.dto.BaseTerminalDto;
import com.scratch.core.terminal.domain.query.BaseTerminalQuery;
import com.scratch.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 终端信息管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/terminal")
public class ABaseTerminalController extends BBaseTerminalController {
    @Autowired
    private IBaseTerminalService baseTerminalService;

    /**
     * 终端分配
     */
    @PutMapping("/allocation")
    @PreAuthorize("@ss.hasAuthority('core:terminal:edit')")
    @Log(title = "终端分配", businessType = BusinessType.UPDATE)
    public AjaxResult allocation(@RequestBody BaseTerminalDto terminal) {
        return toAjax(baseTerminalService.allocation(terminal));
    }


    /**
     * 查询终端信息列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority('core:terminal:list')")
    public AjaxResult list(BaseTerminalQuery terminal) {
        return super.list(terminal);
    }

    /**
     * 查询终端信息详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority('core:terminal:single')")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }


    @GetMapping(value = "/hpTicket/{devNo}")
    @PreAuthorize("@ss.hasAuthority('core:terminalModel:single')")
    public AjaxResult getTerminalHpTicketApi(@PathVariable String devNo) {
        return success(baseService.hpTicket(devNo));
    }


    /**
     * 终端信息新增
     */
    @Override
    @PostMapping
    @PreAuthorize("@ss.hasAuthority('core:terminal:add')")
    @Log(title = "终端信息管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody BaseTerminalDto terminal) {
        return super.add(terminal);
    }

    /**
     * 终端信息修改
     */
    @Override
    @PutMapping
    @PreAuthorize("@ss.hasAuthority('core:terminal:edit')")
    @Log(title = "终端信息管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody BaseTerminalDto terminal) {
        return super.edit(terminal);
    }

    /**
     * 终端信息修改状态
     */
    @Override
    @PutMapping("/status")
    @PreAuthorize("@ss.hasAnyAuthority('core:terminal:edit', 'core:terminal:es')")
    @Log(title = "终端信息管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody BaseTerminalDto terminal) {
        return super.editStatus(terminal);
    }

    /**
     * 终端信息批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
    @PreAuthorize("@ss.hasAuthority('core:terminal:delete')")
    @Log(title = "终端信息管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
}
