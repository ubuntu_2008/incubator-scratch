package com.scratch.core.level.manager.impl;

import com.scratch.core.api.level.domain.po.BaseTicketAwardLevelPo;
import com.scratch.core.api.level.domain.dto.BaseTicketAwardLevelDto;
import com.scratch.core.level.domain.query.BaseTicketAwardLevelQuery;
import com.scratch.core.level.domain.model.BaseTicketAwardLevelConverter;
import com.scratch.core.level.mapper.BaseTicketAwardLevelMapper;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.core.level.manager.IBaseTicketAwardLevelManager;
import org.springframework.stereotype.Component;

/**
 * 奖级管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class BaseTicketAwardLevelManagerImpl extends BaseManagerImpl<BaseTicketAwardLevelQuery, BaseTicketAwardLevelDto, BaseTicketAwardLevelPo, BaseTicketAwardLevelMapper, BaseTicketAwardLevelConverter> implements IBaseTicketAwardLevelManager {
}