package com.scratch.job.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.domain.Direct;
import com.scratch.common.web.correlate.service.CorrelateService;
import com.scratch.job.api.domain.dto.SysJobDto;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.service.ISysJobLogService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.scratch.common.web.correlate.contant.CorrelateConstants.SubOperate.DELETE;

/**
 * 调度任务 关联映射
 *
 * @author scratch
 */
@Getter
@AllArgsConstructor
public enum SysJobCorrelate implements CorrelateService {

    BASE_DEL("默认删除|（调度日志）", new ArrayList<>() {{
        // 调度任务 | 调度日志
        add(new Direct<>(DELETE, ISysJobLogService.class, SysJobLogDto::getJobId, SysJobDto::getId));
    }});

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}
