package com.scratch.job.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.query.SysJobLogQuery;

/**
 * 调度任务日志管理 数据封装层
 *
 * @author scratch
 */
public interface ISysJobLogManager extends IBaseManager<SysJobLogQuery, SysJobLogDto> {

    /**
     * 清空任务日志
     */
    void cleanLog();
}
