package com.scratch.job.mapper;

import com.scratch.common.datasource.annotation.Isolate;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.po.SysJobLogPo;
import com.scratch.job.api.domain.query.SysJobLogQuery;


/**
 * 调度日志管理 数据层
 *
 * @author scratch
 */
@Isolate
public interface SysJobLogMapper extends BaseMapper<SysJobLogQuery, SysJobLogDto, SysJobLogPo> {
}
