package com.scratch.job.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.job.api.domain.dto.SysJobDto;
import com.scratch.job.api.domain.po.SysJobPo;
import com.scratch.job.api.domain.query.SysJobQuery;

/**
 * 调度任务管理 数据层
 *
 * @author scratch
 */
@Master
public interface SysJobMapper extends BaseMapper<SysJobQuery, SysJobDto, SysJobPo> {
}
