package com.scratch.job.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.po.SysJobLogPo;
import com.scratch.job.api.domain.query.SysJobLogQuery;
import com.scratch.job.domain.model.SysJobLogConverter;
import com.scratch.job.manager.ISysJobLogManager;
import com.scratch.job.mapper.SysJobLogMapper;
import org.springframework.stereotype.Component;

/**
 * 调度任务日志管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysJobLogManagerImpl extends BaseManagerImpl<SysJobLogQuery, SysJobLogDto, SysJobLogPo, SysJobLogMapper, SysJobLogConverter> implements ISysJobLogManager {

    /**
     * 清空任务日志
     */
    @Override
    public void cleanLog() {
        baseMapper.delete(Wrappers.update());
    }
}
