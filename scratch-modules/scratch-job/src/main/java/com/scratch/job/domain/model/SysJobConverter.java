package com.scratch.job.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.job.api.domain.dto.SysJobDto;
import com.scratch.job.api.domain.po.SysJobPo;
import com.scratch.job.api.domain.query.SysJobQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 调度任务 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysJobConverter extends BaseConverter<SysJobQuery, SysJobDto, SysJobPo> {
}
