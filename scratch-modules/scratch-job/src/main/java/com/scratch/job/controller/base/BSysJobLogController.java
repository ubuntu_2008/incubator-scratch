package com.scratch.job.controller.base;

import com.scratch.common.web.entity.controller.BaseController;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.query.SysJobLogQuery;
import com.scratch.job.service.ISysJobLogService;

/**
 * 定时任务 | 调度日志管理 | 通用 业务处理
 *
 * @author scratch
 */
public class BSysJobLogController extends BaseController<SysJobLogQuery, SysJobLogDto, ISysJobLogService> {

    /** 定义节点名称 */
    @Override
    protected String getNodeName() {
        return "调度日志";
    }
}
