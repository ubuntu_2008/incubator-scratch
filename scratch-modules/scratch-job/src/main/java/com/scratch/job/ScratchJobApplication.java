package com.scratch.job;

import com.scratch.common.security.annotation.EnableCustomConfig;
import com.scratch.common.security.annotation.EnableResourceServer;
import com.scratch.common.security.annotation.EnableRyFeignClients;
import com.scratch.common.swagger.annotation.EnableCustomSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 定时任务
 *
 * @author scratch
 */
@EnableCustomConfig
@EnableCustomSwagger
@EnableResourceServer
@EnableRyFeignClients
@SpringBootApplication
public class ScratchJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScratchJobApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  定时任务模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
