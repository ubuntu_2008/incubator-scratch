package com.scratch.job.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.po.SysJobLogPo;
import com.scratch.job.api.domain.query.SysJobLogQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 调度日志 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SysJobLogConverter extends BaseConverter<SysJobLogQuery, SysJobLogDto, SysJobLogPo> {
}
