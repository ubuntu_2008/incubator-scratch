package com.scratch.job.controller.admin;

import com.scratch.common.core.web.result.AjaxResult;
import com.scratch.common.log.annotation.Log;
import com.scratch.common.log.enums.BusinessType;
import com.scratch.common.security.annotation.AdminAuth;
import com.scratch.job.api.domain.query.SysJobLogQuery;
import com.scratch.job.controller.base.BSysJobLogController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

/**
 * 定时任务 | 调度日志管理 | 管理端 业务处理
 *
 * @author scratch
 */
@AdminAuth
@RestController
@RequestMapping("/admin/job/log")
public class ASysJobLogController extends BSysJobLogController {

    /**
     * 查询调度日志列表
     */
    @Override
    @GetMapping("/list")
    @PreAuthorize("@ss.hasAuthority(@Auth.SCHEDULE_JOB_LIST)")
    public AjaxResult list(SysJobLogQuery jobLog) {
        return super.list(jobLog);
    }

    /**
     * 查询调度日志详细
     */
    @Override
    @GetMapping(value = "/{id}")
    @PreAuthorize("@ss.hasAuthority(@Auth.SCHEDULE_JOB_SINGLE)")
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }

    /**
     * 清空调度日志
     */
    @DeleteMapping("/clean")
    @PreAuthorize("@ss.hasAuthority(@Auth.SCHEDULE_JOB_DEL)")
    @Log(title = "调度日志管理", businessType = BusinessType.CLEAN)
    public AjaxResult clean() {
        baseService.cleanLog();
        return success();
    }
}
