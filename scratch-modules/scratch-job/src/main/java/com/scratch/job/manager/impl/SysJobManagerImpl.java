package com.scratch.job.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.web.annotation.TenantIgnore;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.job.api.domain.dto.SysJobDto;
import com.scratch.job.api.domain.po.SysJobPo;
import com.scratch.job.api.domain.query.SysJobQuery;
import com.scratch.job.domain.model.SysJobConverter;
import com.scratch.job.manager.ISysJobManager;
import com.scratch.job.mapper.SysJobMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 调度任务管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class SysJobManagerImpl extends BaseManagerImpl<SysJobQuery, SysJobDto, SysJobPo, SysJobMapper, SysJobConverter> implements ISysJobManager {

    /**
     * 项目启动时
     */
    @Override
    @TenantIgnore
    public List<SysJobDto> initScheduler() {
        List<SysJobPo> jobList = baseMapper.selectList(Wrappers.query());
        return baseConverter.mapperDto(jobList);
    }
}
