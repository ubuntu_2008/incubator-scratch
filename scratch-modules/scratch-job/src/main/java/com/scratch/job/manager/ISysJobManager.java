package com.scratch.job.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.job.api.domain.dto.SysJobDto;
import com.scratch.job.api.domain.query.SysJobQuery;

import java.util.List;

/**
 * 调度任务管理 数据封装层
 *
 * @author scratch
 */
public interface ISysJobManager extends IBaseManager<SysJobQuery, SysJobDto> {

    /**
     * 项目启动时
     */
    List<SysJobDto> initScheduler();
}
