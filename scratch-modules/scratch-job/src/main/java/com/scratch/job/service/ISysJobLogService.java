package com.scratch.job.service;

import com.scratch.common.web.entity.service.IBaseService;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.query.SysJobLogQuery;

/**
 * 调度日志管理 服务层
 *
 * @author scratch
 */
public interface ISysJobLogService extends IBaseService<SysJobLogQuery, SysJobLogDto> {

    /**
     * 清空任务日志
     */
    public void cleanLog();
}
