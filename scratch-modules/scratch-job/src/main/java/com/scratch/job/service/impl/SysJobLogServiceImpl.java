package com.scratch.job.service.impl;

import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.job.api.domain.dto.SysJobLogDto;
import com.scratch.job.api.domain.query.SysJobLogQuery;
import com.scratch.job.domain.correlate.SysJobLogCorrelate;
import com.scratch.job.manager.impl.SysJobLogManagerImpl;
import com.scratch.job.service.ISysJobLogService;
import org.springframework.stereotype.Service;

/**
 * 调度日志管理 服务层处理
 *
 * @author scratch
 */
@Service
public class SysJobLogServiceImpl extends BaseServiceImpl<SysJobLogQuery, SysJobLogDto, SysJobLogCorrelate, SysJobLogManagerImpl> implements ISysJobLogService {

    /**
     * 清空任务日志
     */
    @Override
    public void cleanLog() {
        baseManager.cleanLog();
    }
}
