package com.scratch.file;

import com.github.tobato.fastdfs.FdfsClientConfig;
import com.scratch.common.security.annotation.EnableResourceServer;
import com.scratch.common.security.annotation.EnableRyFeignClients;
import com.scratch.common.security.config.ApplicationConfig;
import com.scratch.common.security.config.JacksonConfig;
import com.scratch.common.swagger.annotation.EnableCustomSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * 文件服务
 *
 * @author scratch
 */
@EnableCustomSwagger
@EnableResourceServer
@EnableRyFeignClients
@Import({ApplicationConfig.class, JacksonConfig.class, FdfsClientConfig.class})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ScratchFileApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScratchFileApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  文件服务模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
