package com.scratch.tenant.source.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.tenant.api.source.domain.dto.TeSourceDto;
import com.scratch.tenant.api.source.domain.po.TeSourcePo;
import com.scratch.tenant.api.source.domain.query.TeSourceQuery;

/**
 * 租户服务 | 策略模块 | 数据源管理 数据层
 *
 * @author scratch
 */
@Master
public interface TeSourceMapper extends BaseMapper<TeSourceQuery, TeSourceDto, TeSourcePo> {
}