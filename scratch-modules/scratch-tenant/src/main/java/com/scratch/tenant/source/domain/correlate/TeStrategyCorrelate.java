package com.scratch.tenant.source.domain.correlate;

import com.scratch.common.web.correlate.domain.BaseCorrelate;
import com.scratch.common.web.correlate.service.CorrelateService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 数据源策略 关联映射
 *
 * @author scratch
 */
@Getter
@AllArgsConstructor
public enum TeStrategyCorrelate implements CorrelateService {

    ;

    private final String info;
    private final List<? extends BaseCorrelate<?>> correlates;

}
