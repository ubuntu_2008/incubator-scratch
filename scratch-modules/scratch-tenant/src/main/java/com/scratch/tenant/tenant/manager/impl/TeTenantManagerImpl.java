package com.scratch.tenant.tenant.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.scratch.common.core.constant.basic.SqlConstants;
import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.tenant.api.tenant.domain.dto.TeTenantDto;
import com.scratch.tenant.api.tenant.domain.po.TeTenantPo;
import com.scratch.tenant.api.tenant.domain.query.TeTenantQuery;
import com.scratch.tenant.tenant.domain.model.TeTenantConverter;
import com.scratch.tenant.tenant.manager.ITeTenantManager;
import com.scratch.tenant.tenant.mapper.TeTenantMapper;
import org.springframework.stereotype.Component;

/**
 * 租户服务 | 租户模块 | 租户管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class TeTenantManagerImpl extends BaseManagerImpl<TeTenantQuery, TeTenantDto, TeTenantPo, TeTenantMapper, TeTenantConverter> implements ITeTenantManager {

    /**
     * 校验数据源策略是否被使用
     *
     * @param strategyId 数据源策略id
     * @return 结果
     */
    @Override
    public TeTenantDto checkStrategyExist(Long strategyId) {
        TeTenantPo tenant = baseMapper.selectOne(
                Wrappers.<TeTenantPo>query().lambda()
                        .eq(TeTenantPo::getStrategyId, strategyId)
                        .last(SqlConstants.LIMIT_ONE));
        return baseConverter.mapperDto(tenant);
    }
}