package com.scratch.tenant.source.manager;

import com.scratch.common.web.entity.manager.IBaseManager;
import com.scratch.tenant.api.source.domain.dto.TeSourceDto;
import com.scratch.tenant.api.source.domain.query.TeSourceQuery;

/**
 * 租户服务 | 策略模块 | 数据源管理 数据封装层
 *
 * @author scratch
 */
public interface ITeSourceManager extends IBaseManager<TeSourceQuery, TeSourceDto> {
}