package com.scratch.tenant.source.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.tenant.api.source.domain.dto.TeStrategyDto;
import com.scratch.tenant.api.source.domain.po.TeStrategyPo;
import com.scratch.tenant.api.source.domain.query.TeStrategyQuery;

/**
 * 租户服务 | 策略模块 | 源策略管理 数据层
 *
 * @author scratch
 */
@Master
public interface TeStrategyMapper extends BaseMapper<TeStrategyQuery, TeStrategyDto, TeStrategyPo> {
}