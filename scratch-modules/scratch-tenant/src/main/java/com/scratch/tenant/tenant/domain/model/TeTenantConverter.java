package com.scratch.tenant.tenant.domain.model;

import com.scratch.common.core.web.entity.model.BaseConverter;
import com.scratch.tenant.api.tenant.domain.dto.TeTenantDto;
import com.scratch.tenant.api.tenant.domain.po.TeTenantPo;
import com.scratch.tenant.api.tenant.domain.query.TeTenantQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 租户服务 | 租户模块 | 租户 对象映射器
 *
 * @author scratch
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface TeTenantConverter extends BaseConverter<TeTenantQuery, TeTenantDto, TeTenantPo> {
}