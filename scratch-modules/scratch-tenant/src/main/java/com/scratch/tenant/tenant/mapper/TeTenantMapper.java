package com.scratch.tenant.tenant.mapper;

import com.scratch.common.datasource.annotation.Master;
import com.scratch.common.web.entity.mapper.BaseMapper;
import com.scratch.tenant.api.tenant.domain.dto.TeTenantDto;
import com.scratch.tenant.api.tenant.domain.po.TeTenantPo;
import com.scratch.tenant.api.tenant.domain.query.TeTenantQuery;

/**
 * 租户服务 | 租户模块 | 租户管理 数据层
 *
 * @author scratch
 */
@Master
public interface TeTenantMapper extends BaseMapper<TeTenantQuery, TeTenantDto, TeTenantPo> {
}