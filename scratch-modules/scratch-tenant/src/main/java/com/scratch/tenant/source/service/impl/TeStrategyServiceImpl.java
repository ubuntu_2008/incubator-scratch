package com.scratch.tenant.source.service.impl;

import com.scratch.common.cache.constant.CacheConstants;
import com.scratch.common.core.constant.basic.DictConstants;
import com.scratch.common.core.utils.core.ObjectUtil;
import com.scratch.common.core.utils.core.StrUtil;
import com.scratch.common.web.entity.service.impl.BaseServiceImpl;
import com.scratch.tenant.api.source.domain.dto.TeStrategyDto;
import com.scratch.tenant.api.source.domain.query.TeStrategyQuery;
import com.scratch.tenant.source.domain.correlate.TeStrategyCorrelate;
import com.scratch.tenant.source.manager.ITeStrategyManager;
import com.scratch.tenant.source.service.ITeStrategyService;
import org.springframework.stereotype.Service;

/**
 * 租户服务 | 策略模块 | 源策略管理 服务层处理
 *
 * @author scratch
 */
@Service
public class TeStrategyServiceImpl extends BaseServiceImpl<TeStrategyQuery, TeStrategyDto, TeStrategyCorrelate, ITeStrategyManager> implements ITeStrategyService {

    /**
     * 缓存主键命名定义
     */
    @Override
    public CacheConstants.CacheType getCacheKey() {
        return CacheConstants.CacheType.TE_STRATEGY_KEY;
    }

    /**
     * 校验数据源是否被使用
     *
     * @param sourceId 数据源id
     * @return 结果 | true/false 存在/不存在
     */
    @Override
    public boolean checkSourceExist(Long sourceId) {
        return ObjectUtil.isNotNull(baseManager.checkSourceExist(sourceId));
    }

    /**
     * 校验源策略是否为默认源策略
     *
     * @param id 源策略id
     * @return 结果 | true/false 是/不是
     */
    @Override
    public boolean checkIsDefault(Long id) {
        TeStrategyDto strategy = baseManager.selectById(id);
        return ObjectUtil.isNotNull(strategy) && StrUtil.equals(strategy.getIsDefault(), DictConstants.DicYesNo.YES.getCode());
    }

}