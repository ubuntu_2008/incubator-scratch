package com.scratch.tenant.tenant.domain.dto;

import com.scratch.common.core.web.entity.base.BasisEntity;
import com.scratch.system.api.organize.domain.dto.SysDeptDto;
import com.scratch.system.api.organize.domain.dto.SysPostDto;
import com.scratch.system.api.organize.domain.dto.SysUserDto;
import com.scratch.tenant.api.tenant.domain.dto.TeTenantDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 租户初始化对象
 *
 * @author scratch
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TeTenantRegister extends BasisEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /** 租户信息 */
    private TeTenantDto tenant;

    /** 部门信息 */
    private SysDeptDto dept;

    /** 岗位信息 */
    private SysPostDto post;

    /** 用户信息 */
    private SysUserDto user;

}
