package com.scratch.tenant.source.manager.impl;

import com.scratch.common.web.entity.manager.impl.BaseManagerImpl;
import com.scratch.tenant.api.source.domain.dto.TeSourceDto;
import com.scratch.tenant.api.source.domain.po.TeSourcePo;
import com.scratch.tenant.api.source.domain.query.TeSourceQuery;
import com.scratch.tenant.source.domain.model.TeSourceConverter;
import com.scratch.tenant.source.manager.ITeSourceManager;
import com.scratch.tenant.source.mapper.TeSourceMapper;
import org.springframework.stereotype.Component;

/**
 * 租户服务 | 策略模块 | 数据源管理 数据封装层处理
 *
 * @author scratch
 */
@Component
public class TeSourceManagerImpl extends BaseManagerImpl<TeSourceQuery, TeSourceDto, TeSourcePo, TeSourceMapper, TeSourceConverter> implements ITeSourceManager {
}