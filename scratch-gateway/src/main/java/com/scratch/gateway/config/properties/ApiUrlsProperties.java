package com.scratch.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 需要验签的url
 *
 * @author scratch
 */
@Data
@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "sign.api")
public class ApiUrlsProperties {

    private String appId;

    private String appSecret;

    private Integer delay;

    /**
     * 网关需要验签的urls
     */
    private String[] urls = {};

}
