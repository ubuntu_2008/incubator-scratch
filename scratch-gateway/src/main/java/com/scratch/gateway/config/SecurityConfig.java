package com.scratch.gateway.config;


import com.scratch.common.redis.service.RedisService;
import com.scratch.gateway.config.properties.ApiUrlsProperties;
import com.scratch.gateway.config.properties.IgnoreWhiteProperties;
import com.scratch.gateway.filter.AuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * 安全认证配置
 *
 * @author scratch
 */
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    @Autowired
    private IgnoreWhiteProperties ignoreWhite;

    @Autowired
    private ApiUrlsProperties apiUrls;

    @Autowired
    private RedisService redisService;

    @Bean
    public SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http) {
        http.authorizeExchange((authorize) -> authorize.anyExchange().permitAll())
                // 网关鉴权
                .addFilterAfter(new AuthFilter(redisService, ignoreWhite,apiUrls), SecurityWebFiltersOrder.FIRST)
                .cors(ServerHttpSecurity.CorsSpec::disable)
                // CSRF禁用，因为不使用session
                .csrf(ServerHttpSecurity.CsrfSpec::disable);
        return http.build();
    }
}
