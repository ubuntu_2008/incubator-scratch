package com.scratch.auth.service;

import com.scratch.auth.form.RegisterBody;

/**
 * 登录校验 服务层
 *
 * @author scratch
 */
public interface ISysLoginService {

    /**
     * 注册
     */
    void register(RegisterBody registerBody);

}
