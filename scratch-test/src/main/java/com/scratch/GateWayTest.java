package com.scratch;


import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.scratch.common.service.db.domain.BaseTicketLoadEntity;
import com.scratch.core.api.service.domain.*;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

public class GateWayTest {
    public static void main(String args[]) {
        try {
//            getPlayList();
//            loadTickets();
//            showList();
//            order();
//            cashQrCode();
            cash();
        } catch (Exception e) {
            e.getMessage();
        }
    }


    private static void getPlayList() throws Exception {
        ReqPlayList reqPlayList = new ReqPlayList();
        reqPlayList.setDevNo("U061810130018");
        request("/v1/api/terminal/getPlayList", reqPlayList);
    }

    private static void loadTickets() throws Exception {
        ReqTicketLoad reqTicketLoad = new ReqTicketLoad();
        reqTicketLoad.setDevNo("U061810130018");
        reqTicketLoad.setHpId(2);
        reqTicketLoad.setTicketId("1719926480303030272");
        reqTicketLoad.setTotal(100);
        request("/v1/api/terminal/loadTickets", reqTicketLoad);
    }

    private static void showList() throws Exception {
        ReqShowList reqShowList = new ReqShowList();
        reqShowList.setDevNo("U061810130018");
        request("/v1/api/terminal/showList", reqShowList);
    }

    private static void order() throws Exception {
        ReqOrder reqOrder = new ReqOrder();
        reqOrder.setDevNo("U061810130018");
        reqOrder.setHpId(2);
        reqOrder.setMoney(20);
        reqOrder.setNumber(1);
        reqOrder.setTicketId("1719926480303030272");
        request("/v1/api/terminal/order", reqOrder);
    }


    private static void cashQrCode() throws Exception {
        ReqCashQrCode reqCashQrCode = new ReqCashQrCode();
        reqCashQrCode.setDevNo("U061810130018");
        request("/v1/api/terminal/cashQrCode", reqCashQrCode);
    }

    private static void cash() throws Exception {
        ReqCash reqCash = new ReqCash();
        reqCash.setDevNo("U061810130018");
        reqCash.setUserId("u17018491575896264142");
        reqCash.setDataArea("J082923190036544611815943882234833615959");
        request("/v1/api/terminal/cash", reqCash);
    }




    private static Boolean request(String path, Object obj) throws Exception {
        String url = "http://192.168.31.62:8080";
        long begin = System.currentTimeMillis();
        Map<String, String> headMap = Maps.newHashMapWithExpectedSize(5);
//        timestamp为毫秒数的字符串形式 String.valueOf(LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli())
        headMap.put("timestamp", String.valueOf(LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()));  //值应该为毫秒数的字符串形式
        headMap.put("path", path);
        headMap.put("version", "v1");
//        第二步：进行 Key 的自然排序，然后 Key，Value值拼接最后再拼接分配给你的 SK。
        List<String> storedKeys = Arrays.stream(
                        headMap.keySet()
                                .toArray(new String[]{}))
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
        final String sign = storedKeys.stream()
                .map(key -> String.join("", key, headMap.get(key)))
                .collect(Collectors.joining()).trim()
                .concat("7F9D594A77394E17AB911C43D15601FF");
        headMap.put("sign", DigestUtils.md5DigestAsHex(sign.getBytes()).toUpperCase());
        headMap.put("appKey", "D18864BA295A4A49AB2146B82ACFC34C");
        headMap.put("Content-Type", "application/json");
        String result = HttpRequest.post(url + path)
                .addHeaders(headMap)
                .body(JSON.toJSONString(obj))
                .execute().body();
        System.out.println("响应:" + result);
        long end = System.currentTimeMillis();
        System.out.println("结束:耗时" + (end - begin));
        return true;
    }


}
